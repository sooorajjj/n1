#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)
aurora-framework-res-source-path := APPS/aurora-framework-res_intermediates/src
# the library
# ============================================================

include $(CLEAR_VARS)

#LOCAL_DEX_PREOPT := false

LOCAL_INTERMEDIATE_SOURCES := \
			$(aurora-framework-res-source-path)/com/aurora/internal/R.java \
			$(aurora-framework-res-source-path)/com/aurora/R.java \
			$(aurora-framework-res-source-path)/com/aurora/Manifest.java 

GN_WIDGET_BASE_SUBDIRS := \
	$(addsuffix /java, \
	    core \
	 )

GN_WIDGET_BASE_JAVA_SRC_DIRS := \
	$(addprefix ./,$(GN_WIDGET_BASE_SUBDIRS))

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under,$(GN_WIDGET_BASE_JAVA_SRC_DIRS))
#LOCAL_STATIC_JAVA_LIBRARIES := \
#  android-support-v4
###$(info $(LOCAL_SRC_FILES))
LOCAL_MODULE := aurora-framework-static


include $(BUILD_STATIC_JAVA_LIBRARY)

include $(call first-makefiles-under,$(LOCAL_PATH))

$(info $(LOCAL_INSTALLED_MODULE))
$(LOCAL_INSTALLED_MODULE): | $(PRODUCT_OUT)/system/framework/framework-res.apk
$(info logtest)
$(info $(PRODUCT_OUT))
