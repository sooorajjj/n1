package aurora.widget;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.content.Context;
import android.content.ClipboardManager;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.aurora.R;

class AuroraTextViewEditToolbar extends AuroraTextViewToolbar {
    static final String LOG_TAG = "GN_FW_GNTextViewEditToolbar";

    private static final int ID_SELECT_ALL = android.R.id.selectAll;
    private static final int ID_START_SELECTING_TEXT = android.R.id.startSelectingText;
    private static final int ID_CUT = android.R.id.cut;
    private static final int ID_COPY = android.R.id.copy;
    private static final int ID_SWITCH_INPUT_METHOD = android.R.id.switchInputMethod;

    private static final int ID_SELECT_ALL_STR = R.string.aurora_selectAll;
    private static final int ID_START_SELECTING_TEXT_STR = R.string.aurora_select;
    private static final int ID_CUT_STR = R.string.aurora_cut;
    private static final int ID_COPY_STR = R.string.aurora_copy;
    private static final int ID_SWITCH_INPUT_METHOD_STR = R.string.aurora_inputMethod;

    private TextView mItemSelectAll;
    private TextView mItemStartSelect;
    private TextView mItemCut;
    private TextView mItemCopy;
    private TextView mItemInputMethod;
    private HashMap<Integer,TextView> mCustomItems = new HashMap<Integer,TextView>();

    private OnClickListener mOnClickListener = new OnClickListener() {
        public void onClick(View v) {
            if (isShowing()) {
                /*if (mEditText instanceof GnExtractEditText) {
                    mEditText.onTextContextMenuItem(v.getId());
                }*/
                onItemAction(v.getId());
                switch (v.getId()) {
                case ID_SELECT_ALL:
                case ID_START_SELECTING_TEXT:
                    hide();
                    show();
                    break;
                default:
                    hide();
                    break;
                }
            }
        }
    };
    
    public void addItem(int id ,int textResId){
    	TextView newItem = initToolbarItem(id,textResId);
    	newItem.setOnClickListener(getOnClickListener());
    	if(mCustomItems.containsKey(id)){
    		return;
    	}
    	mCustomItems.put(id, newItem);
    }
    

    AuroraTextViewEditToolbar(AuroraEditText hostView) {
        super(hostView);
        initToolbarItem();
    }

    protected void initToolbarItem() {
        super.initToolbarItem();
        mItemSelectAll = initToolbarItem(ID_SELECT_ALL, ID_SELECT_ALL_STR);
        mItemStartSelect = initToolbarItem(ID_START_SELECTING_TEXT, ID_START_SELECTING_TEXT_STR);
        mItemCopy = initToolbarItem(ID_COPY, ID_COPY_STR);
        mItemCut = initToolbarItem(ID_CUT, ID_CUT_STR);
        mItemInputMethod = initToolbarItem(ID_SWITCH_INPUT_METHOD, ID_SWITCH_INPUT_METHOD_STR);
    }

    protected OnClickListener getOnClickListener() {
        return mOnClickListener;
    }

    protected void updateToolbarItemsEx() {/*
        mToolbarGroup.removeAllViews();
        // construct toolbar.
        if (mEditText.isInTextSelectionMode()) {
            Log.d("LOG_TAG", "updateToolbarItems()----mEditText.isInTextSelectionMode()");
            if (mEditText.canCut()) {
                Log.d("LOG_TAG", "updateToolbarItems()----mEditText.canCut()");
                mToolbarGroup.addView(mItemCut);
            }
            if (mEditText.canCopy()) {
                Log.d("LOG_TAG", "updateToolbarItems()----mEditText.canCopy()");
                mToolbarGroup.addView(mItemCopy);
            }
            if (mEditText.canPaste()) {
                Log.d("LOG_TAG", "updateToolbarItems()----mEditText.canPaste()");
                mToolbarGroup.addView(mItemPaste);
            }
        } else {
            if (mEditText.canSelectText()) {
                Log.d("LOG_TAG", "updateToolbarItems()----mEditText.canSelectText()");
                if (!mEditText.hasPasswordTransformationMethod()) {
                    mToolbarGroup.addView(mItemStartSelect);
                }
                mToolbarGroup.addView(mItemSelectAll);
            }
            if (mEditText.canPaste()) {
                Log.d("LOG_TAG", "updateToolbarItems()----mEditText.canPaste()");
                mToolbarGroup.addView(mItemPaste);
            }
            if (mEditText.isInputMethodTarget()) {
                Log.d("LOG_TAG", "updateToolbarItems()----mEditText.isInputMethodTarget()");
                mToolbarGroup.addView(mItemInputMethod);
            }
        }
    */}
    
    protected void updateToolbarItems() {
        mToolbarGroup.removeAllViews();
        
        boolean passwordTransformed = mEditText.getTransformationMethod() instanceof PasswordTransformationMethod;
        CharSequence text = mEditText.getText();

        boolean hasClip = ((ClipboardManager) mEditText.getContext()
                           .getSystemService(Context.CLIPBOARD_SERVICE)).hasPrimaryClip();
	
        if (mEditText.hasSelection()) {
            if (!passwordTransformed && (text.length() > 0) &&
                    text instanceof Editable &&
                    (mEditText.getKeyListener() != null)) {
                mToolbarGroup.addView(mItemCut);
            }

            if (!passwordTransformed && (text.length() > 0)) {
                mToolbarGroup.addView(mItemCopy);
            }

            if (text instanceof Editable &&
                    (mEditText.getKeyListener() != null) &&
                    (mEditText.getSelectionStart() >= 0) &&
                    (mEditText.getSelectionEnd() >= 0) && hasClip) {
                mToolbarGroup.addView(mItemPaste);
            }
        } else {
            if (text.length() > 0) {
                if (mEditText.isSelectionToolEnabled()) {
                    if (!passwordTransformed) {
                        mToolbarGroup.addView(mItemStartSelect);
                    }

                    mToolbarGroup.addView(mItemSelectAll);
                }
            }

            if (text instanceof Editable &&
                    (mEditText.getKeyListener() != null) &&
                    (mEditText.getSelectionStart() >= 0) &&
                    (mEditText.getSelectionEnd() >= 0) && hasClip) {
                mToolbarGroup.addView(mItemPaste);
          
            }

            if (mEditText.isImSwitcherEnabled() &&
                    mEditText.isInputMethodTarget()) {
                    //TODO Alan.Xu
                    //!(mEditText instanceof OppoExtractEditText)) {
                mToolbarGroup.addView(mItemInputMethod);
            }
			if (hasClip) {
					if (mCustomItems.size() > 0) {
						Iterator<Integer> iter = mCustomItems.keySet()
								.iterator();
						while (iter.hasNext()) {
							Integer key = (Integer) iter.next();
							TextView item = mCustomItems.get(key);
							mToolbarGroup.addView(item);
						}
					}
			}
          	
        }
    }

    private boolean onItemAction(int id) {
        CharSequence text = mEditText.getText();
        CharSequence transformed = text;//mEditText.getTransformed();

        int min = 0;
        int max = text.length();
        if (mEditText.isFocused()) {
            final int selStart = mEditText.getSelectionStart();
            final int selEnd = mEditText.getSelectionEnd();
            min = Math.max(0, Math.min(selStart, selEnd));
            max = Math.max(0, Math.max(selStart, selEnd));
        }

        ClipboardManager clip = (ClipboardManager) mEditText.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        
        switch (id) {
        case ID_SELECT_ALL:
            Selection.setSelection((Spannable) text, 0, text.length());
            mEditText.mStart=0;
            mEditText.mEnd=text.length();
            mEditText.mIsSelectedAll = true;
//            mEditText.onContextItemClicked(ID_SELECT_ALL);
            mEditText.startTextSelectionMode();
            mEditText.onContextItemClicked(ID_SELECT_ALL);
            return true;
        case ID_START_SELECTING_TEXT:
            mEditText.startTextSelectionMode();
            return true;
        case ID_CUT:
            int end = mEditText.getSelectionStart();
            clip.setText(transformed.subSequence(min, max));
            // if (!(mEditText instanceof GnExtractEditText)) {
                ((Editable) text).delete(min, max);
            // }
            mEditText.stopTextSelectionMode();
            // if (mEditText instanceof GnExtractEditText) {
                Selection.setSelection((Spannable) mEditText.getText(), end);
            // }
//                mEditText.onContextItemClicked(ID_CUT);
                mEditText.onContextItemClicked(ID_CUT);
            return true;
        case ID_COPY:
            clip.setText(transformed.subSequence(min, max));
            mEditText.stopTextSelectionMode();
//            mEditText.onContextItemClicked(ID_COPY);
            mEditText.onContextItemClicked(ID_COPY);
            return true;
        case ID_PASTE:
            CharSequence paste = clip.getText();
            if (paste != null && paste.length() > 0) {
                Selection.setSelection((Spannable) text, max);
                // if (!(mEditText instanceof GnExtractEditText)) {
                    ((Editable) text).replace(min, max, paste);
                //}
                mEditText.stopTextSelectionMode();
                mEditText.onContextItemClicked(ID_PASTE);
            }
            
            return true;
        case ID_SWITCH_INPUT_METHOD:
            if (!(mEditText instanceof AuroraExtractEditText)) {
                InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.showInputMethodPicker();
                }
            }
            return true;
        }
//       mEditText.onContextItemClicked(id);
        if(mCustomItems.containsKey(id)){
        	mEditText.onContextItemClicked(id);
        }
        return false;
    }

}

