package com.android.internal.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.android.internal.app.AuroraResolverActivity.DisplayResolveInfo;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ResolveListAdapter extends BaseAdapter {
    private final Intent[] mInitialIntents;
    private final Intent mIntent;
    private final List<ResolveInfo> mBaseResolveList;
    
    private final int mLaunchedFromUid;
    private final LayoutInflater mInflater;

    private List<DisplayResolveInfo> mList;
    
    private List<DisplayResolveInfo> mDatas;

    private PackageManager mPm;
    
    /** ViewPager页码 */
	private int mIndex;
	/** 根据屏幕大小计算得到的每页item个数 */
	private int mPageItemCount = 1;
	/** 传进来的List的总长度 */
	private int mTotalSize;
	
	private int mPosition;
	
	
	
	
	private AuroraResolverActivity mActivity;
	
    
    public ResolveListAdapter(Context context, Intent intent,
            Intent[] initialIntents, List<ResolveInfo> rList, int launchedFromUid,int index, int pageItemCount,
             PackageManager pm,AuroraResolverActivity activity) {
    	mPm = pm;
    	mActivity = activity;
        mIntent = new Intent(intent);
        mIntent.setComponent(null);
        mInitialIntents = initialIntents;
        mBaseResolveList = rList;
        mLaunchedFromUid = launchedFromUid;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mList = new ArrayList<DisplayResolveInfo>();
        mDatas = new ArrayList<DisplayResolveInfo>();
        mList = mActivity.mList;
        mTotalSize = mList.size();
        this.mIndex = index;
		this.mPageItemCount = pageItemCount;
    	
		// itemRealNum=list.size()-index*pageItemCount;
		int list_index = index * pageItemCount;
		for (int i = list_index; i < mList.size(); i++) {
			mDatas.add((DisplayResolveInfo) mList.get(i));
		}
        setDrawableCache();
    }
    
    public void initPageDatas( ){
    	
    }
    
    
    List<DisplayResolveInfo> getDatasList(){
    	return mList;
    }

    public void handlePackagesChanged() {
        final int oldItemCount = getCount();
        mActivity.rebuildList();
        notifyDataSetChanged();
        final int newItemCount = getCount();
        if (newItemCount == 0) {
            // We no longer have any items...  just finish the activity.
        	mActivity.finish();
        } 
        
    }


   

    public ResolveInfo resolveInfoForPosition(int position) {
        return mList.get(position).ri;
    }

    public Intent intentForPosition(int position) {
        DisplayResolveInfo dri = mList.get(position);
        
        Intent intent = new Intent(dri.origIntent != null
                ? dri.origIntent : mIntent);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT
                |Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        ActivityInfo ai = dri.ri.activityInfo;
        intent.setComponent(new ComponentName(
                ai.applicationInfo.packageName, ai.name));
        return intent;
    }

    public int getCount() {
    	int size = mTotalSize / mPageItemCount;
		if (mIndex == size){
			return mTotalSize - mPageItemCount * mIndex;
		}else{
			return mPageItemCount;
		}
    }

    public Object getItem(int position) {
        return mDatas.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
       return getViewInner(position,convertView,parent);
    }
	
	private View getViewInner(int position, View convertView, ViewGroup parent) {
        View view;
        mPosition = position;
        if (convertView == null) {
            view = mInflater.inflate(
                   com.aurora.R.layout.aurora_resolve_list_item, parent, false);
                   
			ImageView icon = (ImageView)view.findViewById(com.aurora.R.id.aurora_icon);
			
			
        } else {
            view = convertView;
        }
        bindView(view, mDatas.get(position));
        return view;
    }
    
    private final void bindView(View view, DisplayResolveInfo info) {
		bindViewInner(view, info); 
    }
    
    private final void bindViewInner(View view, DisplayResolveInfo info) {
        TextView text = (TextView)view.findViewById(com.aurora.R.id.aurora_text1);
        
        TextView text2 = (TextView)view.findViewById(com.aurora.R.id.aurora_text2);
        
        ImageView icon = (ImageView)view.findViewById(com.aurora.R.id.aurora_icon);
        
        text.setText(info.displayLabel);
        
        text2.setVisibility(View.GONE);
    
        if(mPosition == 0 && icon.getDrawable() != null){
			return;
		}
		
		/*if(mPosition == 0 && icon.getDrawable() == null){
			info.displayIcon = 	mActivity.getImageDrawable(mActivity.loadIconForResolveInfo(info.ri));
		}*/
		
        if (info.displayIcon == null) {
            info.displayIcon = mActivity.loadIconForResolveInfo(info.ri);
        }
      
		icon.setImageDrawable(info.displayIcon);
	
    }
     
    private void setDrawableCache(){
		if(mList != null){
			for(int i = 0 ; i < mList.size(); i++){
				DisplayResolveInfo info = mList.get(i);
				
				if(info.displayIcon == null){
					info.displayIcon = mActivity.loadIconForResolveInfo(info.ri);
				}
				info.displayIcon = mActivity.getImageDrawable(info.displayIcon);
			}
		}
	}
}
