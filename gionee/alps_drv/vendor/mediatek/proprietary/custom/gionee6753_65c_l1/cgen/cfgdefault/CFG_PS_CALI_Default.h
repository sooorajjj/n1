/*********************************************
  This file is added for psensor calibration 
  *********************************************/

/* Gionee chengx 20140403 add for CR01167528 begin */
/***********************************************
  To give GN_CFG_PS_CALI_Default config data
  *********************************************/
#ifndef _CFG_PS_CALI_D_H
#define _CFG_PS_CALI_D_H
PS_CALI stPS_CALIConfigDefault =
{
   0x0,0x0,0x0
};
#endif

/* Gionee chengx 20140403 add for CR01167528 end */
