/* KXTIK1013 motion sensor driver
 *
 *
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/miscdevice.h>
#include <asm/uaccess.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/kobject.h>
#include <linux/earlysuspend.h>
#include <linux/platform_device.h>
#include <asm/atomic.h>
#include <cust_acc_kxtik1013.h>
//#include <cust_acc.h>
#include <linux/hwmsensor.h>
#include <linux/hwmsen_dev.h>
#include <linux/sensors_io.h>
#include "gn_kionix_kxtik1013.h"
#include <linux/hwmsen_helper.h>

//#include <mach/mt_devs.h>
#include <mach/mt_typedefs.h>
#include <mach/mt_gpio.h>
#include <mach/mt_pm_ldo.h>

/* Gionee BSP1 chengx 20140826 modify for CR01371160 begin */
#ifdef GN_MTK_BSP_DEVICECHECK
#include <linux/gn_device_check.h>
extern int gn_set_device_info(struct gn_device_info gn_dev_info);
#endif
/* Gionee BSP1 chengx 20140826 modify for CR01371160 end */

#define POWER_NONE_MACRO MT65XX_POWER_NONE

/*----------------------------------------------------------------------------*/
#define GSE_TAG                  "[Gsensor] "
#define GSE_FUN(f)               printk(KERN_INFO GSE_TAG"%s\n", __FUNCTION__)
#define GSE_ERR(fmt, args...)    printk(KERN_ERR GSE_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#define GSE_INFO(fmt, args...)    printk(KERN_INFO GSE_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#if defined(KXTIK1013_DEBUG)
#define GSE_LOG(fmt, args...)    printk(KERN_INFO GSE_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#else
#define GSE_LOG(fmt, args...)
#endif

/*----------------------------------------------------------------------------*/
#define I2C_DRIVERID_KXTIK1013 150
/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#include <mach/mt_gpio.h>
#include <cust_eint.h>
#include <mach/mt_typedefs.h>
#define KXTIK1013_MOTION_SENSOR_FUNCTION
extern void mt_eint_unmask(unsigned int line);
extern void mt_eint_mask(unsigned int line);
extern void mt_eint_registration(unsigned int eint_num, unsigned int flow, void (EINT_FUNC_PTR)(void), unsigned int is_auto_umask);
static int kxtik1013_set_motion_enable(struct i2c_client *client, bool enable);
/* Gionee BSP1 chengx 20140828 modify for xxxx end */
/*----------------------------------------------------------------------------*/
//#define CONFIG_KXTIK1013_LOWPASS   /*apply low pass filter on output*/       
#define SW_CALIBRATION

#define SHOW_DATA_RATE      30
/*----------------------------------------------------------------------------*/
#define KXTIK1013_AXIS_X          0
#define KXTIK1013_AXIS_Y          1
#define KXTIK1013_AXIS_Z          2
#define KXTIK1013_AXES_NUM        3
#define KXTIK1013_DATA_LEN        6
#define KXTIK1013_DEV_NAME        "KXTIK1013"
static DEFINE_MUTEX(kx1013_mutex);

/*----------------------------------------------------------------------------*/
static const struct i2c_device_id kxtik1013_i2c_id[] = {{KXTIK1013_DEV_NAME,0},{}};
static struct i2c_board_info __initdata i2c_kxtik1013={ I2C_BOARD_INFO(KXTIK1013_DEV_NAME, (0x1e>>1))};

/*----------------------------------------------------------------------------*/
static int kxtik1013_remove(void);
#ifdef MTK_AUTO_DETECT_ACCELEROMETER 
extern int hwmsen_gsensor_del(struct sensor_init_info* obj);
static int kxtik1013_local_init(void);
static struct sensor_init_info kxtik1013_init_info = {
   .name = "gsensor_kxtik",
   .init = kxtik1013_local_init,
   .uninit = kxtik1013_remove,
};
#else
//Gionee yang_yang CR01324878 begin 
#ifdef CONFIG_OF
//#if 0
static const struct of_device_id kxtik1013_of_match[] = {
	{ .compatible = "mediatek,gsensor", },
	{},
};
#endif
//Gionee yang_yang CR01324878 end
static int kxtik1013_probe(struct platform_device *pdev);
static struct platform_driver kxtik1013_gsensor_driver = {
	.probe      = kxtik1013_probe,
	.remove     = kxtik1013_remove,    
	.driver     = {
		.name  = "gsensor",
		//Gionee yang_yang CR01324878 begin 
		#ifdef CONFIG_OF
		.of_match_table = kxtik1013_of_match,
		#endif
		//Gionee yang_yang CR01324878 end
		.owner = THIS_MODULE,
	}
};
#endif

/*----------------------------------------------------------------------------*/
static int kxtik1013_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id); 
static int kxtik1013_i2c_remove(struct i2c_client *client);
static int kxtik1013_i2c_detect(struct i2c_client *client, struct i2c_board_info *info);
static struct i2c_driver kxtik1013_i2c_driver = {
    .driver = {
//        .owner          = THIS_MODULE,
        .name           = KXTIK1013_DEV_NAME,
    },
	.probe      		= kxtik1013_i2c_probe,
	.remove    			= kxtik1013_i2c_remove,
	.detect				= kxtik1013_i2c_detect,
#if !defined(CONFIG_HAS_EARLYSUSPEND)    
    .suspend            = kxtik1013_suspend,
    .resume             = kxtik1013_resume,
#endif
	.id_table = kxtik1013_i2c_id,
//	.address_data = &kxtik1013_addr_data,
};

/*----------------------------------------------------------------------------*/
typedef enum {
    ADX_TRC_FILTER  = 0x01,
    ADX_TRC_RAWDATA = 0x02,
    ADX_TRC_IOCTL   = 0x04,
    ADX_TRC_CALI	= 0X08,
    ADX_TRC_INFO	= 0X10,
} ADX_TRC;

/*----------------------------------------------------------------------------*/
struct scale_factor{
    u8  whole;
    u8  fraction;
};

/*----------------------------------------------------------------------------*/
struct data_resolution {
    struct scale_factor scalefactor;
    int                 sensitivity;
};

/*----------------------------------------------------------------------------*/
#define C_MAX_FIR_LENGTH (32)
struct data_filter {
    s16 raw[C_MAX_FIR_LENGTH][KXTIK1013_AXES_NUM];
    int sum[KXTIK1013_AXES_NUM];
    int num;
    int idx;
};

/*----------------------------------------------------------------------------*/
struct kxtik1013_i2c_data {
    struct i2c_client *client;
    struct acc_hw *hw;
    struct hwmsen_convert   cvt;
    
/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
    struct work_struct      kxtik1013_eint_work;
    bool                    motion_detect_enable;
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

    /*misc*/
    struct data_resolution *reso;
    atomic_t                trace;
    atomic_t                suspend;
    atomic_t                selftest;
	atomic_t				filter;
    s16                     cali_sw[KXTIK1013_AXES_NUM+1];

    /*data*/
    s8                      offset[KXTIK1013_AXES_NUM+1];  /*+1: for 4-byte alignment*/
    s16                     data[KXTIK1013_AXES_NUM+1];

#if defined(CONFIG_KXTIK1013_LOWPASS)
    atomic_t                firlen;
    atomic_t                fir_en;
    struct data_filter      fir;
#endif 

    /*early suspend*/
#if defined(CONFIG_HAS_EARLYSUSPEND)
    struct early_suspend    early_drv;
#endif     
};

/*----------------------------------------------------------------------------*/
static struct i2c_client *kxtik1013_i2c_client = NULL;
static struct kxtik1013_i2c_data *obj_i2c_data = NULL;
static bool sensor_power = true;
static GSENSOR_VECTOR3D gsensor_gain;
static char selftestRes[8]= {0}; 
static int kxtik1013_init_flag = 0;    //0<==>OK,-1<==>fail

/*----------------------------------------------------------------------------*/
static struct data_resolution kxtik1013_data_resolution[1] = {
 /* combination by {FULL_RES,RANGE}*/
    {{ 0, 9}, 1024}, // dataformat +/-2g  in 12-bit resolution;  { 3, 9} = 3.9 = (2*2*1000)/(2^12);  256 = (2^12)/(2*2)          
};
static struct data_resolution kxtik1013_offset_resolution = {{15, 6}, 64};

/*----------------------------------------------------------------------------*/
static int KXTIK1013_SetPowerMode(struct i2c_client *client, bool enable);

/*--------------------KXTIK1013 power control function----------------------------------*/
static void KXTIK1013_power(struct acc_hw *hw, unsigned int on) 
{
	static unsigned int power_on = 0;

	if(hw->power_id != POWER_NONE_MACRO)		// have externel LDO
	{        
		GSE_LOG("power %s\n", on ? "on" : "off");
		if(power_on == on)	// power status not change
		{
			GSE_LOG("ignore power control: %d\n", on);
		}
		else if(on)	// power on
		{
			if(!hwPowerOn(hw->power_id, hw->power_vol, "KXTIK1013"))
			{
				GSE_ERR("power on fails!!\n");
			}
		}
		else	// power off
		{
			if (!hwPowerDown(hw->power_id, "KXTIK1013"))
			{
				GSE_ERR("power off fail!!\n");
			}			  
		}
	}
	power_on = on;    
}

/*----------------------------------------------------------------------------*/
static int KXTIK1013_SetDataResolution(struct kxtik1013_i2c_data *obj)
{
	int err;
	u8  databuf[2];

	KXTIK1013_SetPowerMode(obj->client, false);

	if(hwmsen_read_block(obj->client, KXTIK1013_REG_DATA_RESOLUTION, databuf, 0x01))
	{
		printk("kxtik1013 read Dataformat failt \n");
		return KXTIK1013_ERR_I2C;
	}

	databuf[0] &= ~KXTIK1013_RANGE_DATA_RESOLUTION_MASK;
	databuf[0] |= KXTIK1013_RANGE_DATA_RESOLUTION_MASK;//12bit
	databuf[1] = databuf[0];
	databuf[0] = KXTIK1013_REG_DATA_RESOLUTION;

	err = i2c_master_send(obj->client, databuf, 0x2);

	if(err <= 0)
	{
		return KXTIK1013_ERR_I2C;
	}

	KXTIK1013_SetPowerMode(obj->client, true);

	//kxtik1013_data_resolution[0] has been set when initialize: +/-2g  in 8-bit resolution:  15.6 mg/LSB*/   
	obj->reso = &kxtik1013_data_resolution[0];

	return 0;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ReadData(struct i2c_client *client, s16 data[KXTIK1013_AXES_NUM])
{
	struct kxtik1013_i2c_data *priv = i2c_get_clientdata(client);        
	u8 addr = KXTIK1013_REG_DATAX0;
	u8 buf[KXTIK1013_DATA_LEN] = {0};
	int err = 0;
	int i;
	//int tmp=0;
	//u8 ofs[3];
    static int show_data_count = 0;

	if(NULL == client)
	{
		err = -EINVAL;
	}
	else if((err = hwmsen_read_block(client, addr, buf, 0x06)))
	{
		GSE_ERR("error: %d\n", err);
	}
	else
	{
		data[KXTIK1013_AXIS_X] = (s16)((buf[KXTIK1013_AXIS_X*2] >> 4) |
		         (buf[KXTIK1013_AXIS_X*2+1] << 4));
		data[KXTIK1013_AXIS_Y] = (s16)((buf[KXTIK1013_AXIS_Y*2] >> 4) |
		         (buf[KXTIK1013_AXIS_Y*2+1] << 4));
		data[KXTIK1013_AXIS_Z] = (s16)((buf[KXTIK1013_AXIS_Z*2] >> 4) |
		         (buf[KXTIK1013_AXIS_Z*2+1] << 4));

		for(i=0;i<3;i++)				
		{								//because the data is store in binary complement number formation in computer system
			if ( data[i] == 0x0800 )	//so we want to calculate actual number here
				data[i]= -2048;			//10bit resolution, 512= 2^(12-1)
			else if ( data[i] & 0x0800 )//transfor format
			{							//printk("data 0 step %x \n",data[i]);
				data[i] -= 0x1; 		//printk("data 1 step %x \n",data[i]);
				data[i] = ~data[i]; 	//printk("data 2 step %x \n",data[i]);
				data[i] &= 0x07ff;		//printk("data 3 step %x \n\n",data[i]);
				data[i] = -data[i]; 	
			}
		}	

        show_data_count ++;
		if((atomic_read(&priv->trace) & ADX_TRC_RAWDATA) || (show_data_count % SHOW_DATA_RATE == 0))
		{
			GSE_INFO("[%08X %08X %08X] => [%5d %5d %5d]\n", data[KXTIK1013_AXIS_X], data[KXTIK1013_AXIS_Y], data[KXTIK1013_AXIS_Z],
		                               data[KXTIK1013_AXIS_X], data[KXTIK1013_AXIS_Y], data[KXTIK1013_AXIS_Z]);
		}
#ifdef CONFIG_KXTIK1013_LOWPASS
		if(atomic_read(&priv->filter))
		{
			if(atomic_read(&priv->fir_en) && !atomic_read(&priv->suspend))
			{
				int idx, firlen = atomic_read(&priv->firlen);   
				if(priv->fir.num < firlen)
				{                
					priv->fir.raw[priv->fir.num][KXTIK1013_AXIS_X] = data[KXTIK1013_AXIS_X];
					priv->fir.raw[priv->fir.num][KXTIK1013_AXIS_Y] = data[KXTIK1013_AXIS_Y];
					priv->fir.raw[priv->fir.num][KXTIK1013_AXIS_Z] = data[KXTIK1013_AXIS_Z];
					priv->fir.sum[KXTIK1013_AXIS_X] += data[KXTIK1013_AXIS_X];
					priv->fir.sum[KXTIK1013_AXIS_Y] += data[KXTIK1013IK_AXIS_Y];
					priv->fir.sum[KXTIK1013_AXIS_Z] += data[KXTIK1013_AXIS_Z];
					if(atomic_read(&priv->trace) & ADX_TRC_FILTER)
					{
						GSE_LOG("add [%2d] [%5d %5d %5d] => [%5d %5d %5d]\n", priv->fir.num,
							priv->fir.raw[priv->fir.num][KXTIK1013_AXIS_X], priv->fir.raw[priv->fir.num][KXTIK1013_AXIS_Y], priv->fir.raw[priv->fir.num][KXTIK1013_AXIS_Z],
							priv->fir.sum[KXTIK1013_AXIS_X], priv->fir.sum[KXTIK1013_AXIS_Y], priv->fir.sum[KXTIK1013_AXIS_Z]);
					}
					priv->fir.num++;
					priv->fir.idx++;
				}
				else
				{
					idx = priv->fir.idx % firlen;
					priv->fir.sum[KXTIK1013_AXIS_X] -= priv->fir.raw[idx][KXTIK1013_AXIS_X];
					priv->fir.sum[KXTIK1013_AXIS_Y] -= priv->fir.raw[idx][KXTIK1013_AXIS_Y];
					priv->fir.sum[KXTIK1013_AXIS_Z] -= priv->fir.raw[idx][KXTIK1013_AXIS_Z];
					priv->fir.raw[idx][KXTIK1013_AXIS_X] = data[KXTIK1013_AXIS_X];
					priv->fir.raw[idx][KXTIK1013_AXIS_Y] = data[KXTIK1013_AXIS_Y];
					priv->fir.raw[idx][KXTIK1013_AXIS_Z] = data[KXTIK1013_AXIS_Z];
					priv->fir.sum[KXTIK1013_AXIS_X] += data[KXTIK1013_AXIS_X];
					priv->fir.sum[KXTIK1013_AXIS_Y] += data[KXTIK1013_AXIS_Y];
					priv->fir.sum[KXTIK1013_AXIS_Z] += data[KXTIK1013_AXIS_Z];
					priv->fir.idx++;
					data[KXTIK1013_AXIS_X] = priv->fir.sum[KXTIK1013_AXIS_X]/firlen;
					data[KXTIK1013_AXIS_Y] = priv->fir.sum[KXTIK1013_AXIS_Y]/firlen;
					data[KXTIK1013_AXIS_Z] = priv->fir.sum[KXTIK1013_AXIS_Z]/firlen;
					if(atomic_read(&priv->trace) & ADX_TRC_FILTER)
					{
						GSE_LOG("add [%2d] [%5d %5d %5d] => [%5d %5d %5d] : [%5d %5d %5d]\n", idx,
						priv->fir.raw[idx][KXTIK1013_AXIS_X], priv->fir.raw[idx][KXTIK1013_AXIS_Y], priv->fir.raw[idx][KXTIK1013_AXIS_Z],
						priv->fir.sum[KXTIK1013_AXIS_X], priv->fir.sum[KXTIK1013_AXIS_Y], priv->fir.sum[KXTIK1013_AXIS_Z],
						data[KXTIK1013_AXIS_X], data[KXTIK1013_AXIS_Y], data[KXTIK1013_AXIS_Z]);
					}
				}
			}
		}	
#endif         
	}
	return err;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ReadOffset(struct i2c_client *client, s8 ofs[KXTIK1013_AXES_NUM])
{    
	int err = 0;

	ofs[1]=ofs[2]=ofs[0]=0x00;

	GSE_LOG("offesx=%x, y=%x, z=%x",ofs[0],ofs[1],ofs[2]);
	
	return err;    
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ResetCalibration(struct i2c_client *client)
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	//u8 ofs[4]={0,0,0,0};
	int err = 0;

	memset(obj->cali_sw, 0x00, sizeof(obj->cali_sw));
	memset(obj->offset, 0x00, sizeof(obj->offset));
	return err;    
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ReadCalibration(struct i2c_client *client, int dat[KXTIK1013_AXES_NUM])
{
    struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
    //int err;
    int mul;

	#ifdef SW_CALIBRATION
		mul = 0;//only SW Calibration, disable HW Calibration
	#else
	    if ((err = KXTIK1013_ReadOffset(client, obj->offset))) {
        GSE_ERR("read offset fail, %d\n", err);
        return err;
    	}    
    	mul = obj->reso->sensitivity/kxtik1013_offset_resolution.sensitivity;
	#endif

    dat[obj->cvt.map[KXTIK1013_AXIS_X]] = obj->cvt.sign[KXTIK1013_AXIS_X]*(obj->offset[KXTIK1013_AXIS_X]*mul + obj->cali_sw[KXTIK1013_AXIS_X]);
    dat[obj->cvt.map[KXTIK1013_AXIS_Y]] = obj->cvt.sign[KXTIK1013_AXIS_Y]*(obj->offset[KXTIK1013_AXIS_Y]*mul + obj->cali_sw[KXTIK1013_AXIS_Y]);
    dat[obj->cvt.map[KXTIK1013_AXIS_Z]] = obj->cvt.sign[KXTIK1013_AXIS_Z]*(obj->offset[KXTIK1013_AXIS_Z]*mul + obj->cali_sw[KXTIK1013_AXIS_Z]);                        
                                       
    return 0;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ReadCalibrationEx(struct i2c_client *client, int act[KXTIK1013_AXES_NUM], int raw[KXTIK1013_AXES_NUM])
{  
	/*raw: the raw calibration data; act: the actual calibration data*/
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	//int err;
	int mul;

	#ifdef SW_CALIBRATION
		mul = 0;//only SW Calibration, disable HW Calibration
	#else
		if(err = KXTIK1013_ReadOffset(client, obj->offset))
		{
			GSE_ERR("read offset fail, %d\n", err);
			return err;
		}   
		mul = obj->reso->sensitivity/kxtik1013_offset_resolution.sensitivity;
	#endif
	
	raw[KXTIK1013_AXIS_X] = obj->offset[KXTIK1013_AXIS_X]*mul + obj->cali_sw[KXTIK1013_AXIS_X];
	raw[KXTIK1013_AXIS_Y] = obj->offset[KXTIK1013_AXIS_Y]*mul + obj->cali_sw[KXTIK1013_AXIS_Y];
	raw[KXTIK1013_AXIS_Z] = obj->offset[KXTIK1013_AXIS_Z]*mul + obj->cali_sw[KXTIK1013_AXIS_Z];

	act[obj->cvt.map[KXTIK1013_AXIS_X]] = obj->cvt.sign[KXTIK1013_AXIS_X]*raw[KXTIK1013_AXIS_X];
	act[obj->cvt.map[KXTIK1013_AXIS_Y]] = obj->cvt.sign[KXTIK1013_AXIS_Y]*raw[KXTIK1013_AXIS_Y];
	act[obj->cvt.map[KXTIK1013_AXIS_Z]] = obj->cvt.sign[KXTIK1013_AXIS_Z]*raw[KXTIK1013_AXIS_Z];                        
	                       
	return 0;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_WriteCalibration(struct i2c_client *client, int dat[KXTIK1013_AXES_NUM])
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	int err;
	int cali[KXTIK1013_AXES_NUM], raw[KXTIK1013_AXES_NUM];
	//int lsb = kxtik1013_offset_resolution.sensitivity;
	//int divisor = obj->reso->sensitivity/lsb;

	if((err = KXTIK1013_ReadCalibrationEx(client, cali, raw)))	/*offset will be updated in obj->offset*/
	{ 
		GSE_ERR("read offset fail, %d\n", err);
		return err;
	}

	GSE_LOG("OLDOFF: (%+3d %+3d %+3d): (%+3d %+3d %+3d) / (%+3d %+3d %+3d)\n", 
		raw[KXTIK1013_AXIS_X], raw[KXTIK1013_AXIS_Y], raw[KXTIK1013_AXIS_Z],
		obj->offset[KXTIK1013_AXIS_X], obj->offset[KXTIK1013_AXIS_Y], obj->offset[KXTIK1013_AXIS_Z],
		obj->cali_sw[KXTIK1013_AXIS_X], obj->cali_sw[KXTIK1013_AXIS_Y], obj->cali_sw[KXTIK1013_AXIS_Z]);

	/*calculate the real offset expected by caller*/
	cali[KXTIK1013_AXIS_X] += dat[KXTIK1013_AXIS_X];
	cali[KXTIK1013_AXIS_Y] += dat[KXTIK1013_AXIS_Y];
	cali[KXTIK1013_AXIS_Z] += dat[KXTIK1013_AXIS_Z];

	GSE_LOG("UPDATE: (%+3d %+3d %+3d)\n", 
		dat[KXTIK1013_AXIS_X], dat[KXTIK1013_AXIS_Y], dat[KXTIK1013_AXIS_Z]);

#ifdef SW_CALIBRATION
	obj->cali_sw[KXTIK1013_AXIS_X] = obj->cvt.sign[KXTIK1013_AXIS_X]*(cali[obj->cvt.map[KXTIK1013_AXIS_X]]);
	obj->cali_sw[KXTIK1013_AXIS_Y] = obj->cvt.sign[KXTIK1013_AXIS_Y]*(cali[obj->cvt.map[KXTIK1013_AXIS_Y]]);
	obj->cali_sw[KXTIK1013_AXIS_Z] = obj->cvt.sign[KXTIK1013_AXIS_Z]*(cali[obj->cvt.map[KXTIK1013_AXIS_Z]]);	
#else
	obj->offset[KXTIK1013_AXIS_X] = (s8)(obj->cvt.sign[KXTIK1013_AXIS_X]*(cali[obj->cvt.map[KXTIK1013_AXIS_X]])/(divisor));
	obj->offset[KXTIK1013_AXIS_Y] = (s8)(obj->cvt.sign[KXTIK1013_AXIS_Y]*(cali[obj->cvt.map[KXTIK1013_AXIS_Y]])/(divisor));
	obj->offset[KXTIK1013_AXIS_Z] = (s8)(obj->cvt.sign[KXTIK1013_AXIS_Z]*(cali[obj->cvt.map[KXTIK1013_AXIS_Z]])/(divisor));

	/*convert software calibration using standard calibration*/
	obj->cali_sw[KXTIK1013_AXIS_X] = obj->cvt.sign[KXTIK1013_AXIS_X]*(cali[obj->cvt.map[KXTIK1013_AXIS_X]])%(divisor);
	obj->cali_sw[KXTIK1013_AXIS_Y] = obj->cvt.sign[KXTIK1013_AXIS_Y]*(cali[obj->cvt.map[KXTIK1013_AXIS_Y]])%(divisor);
	obj->cali_sw[KXTIK1013_AXIS_Z] = obj->cvt.sign[KXTIK1013_AXIS_Z]*(cali[obj->cvt.map[KXTIK1013_AXIS_Z]])%(divisor);

	GSE_LOG("NEWOFF: (%+3d %+3d %+3d): (%+3d %+3d %+3d) / (%+3d %+3d %+3d)\n", 
		obj->offset[KXTIK1013_AXIS_X]*divisor + obj->cali_sw[KXTIK1013_AXIS_X], 
		obj->offset[KXTIK1013_AXIS_Y]*divisor + obj->cali_sw[KXTIK1013_AXIS_Y], 
		obj->offset[KXTIK1013_AXIS_Z]*divisor + obj->cali_sw[KXTIK1013_AXIS_Z], 
		obj->offset[KXTIK1013_AXIS_X], obj->offset[KXTIK1013_AXIS_Y], obj->offset[KXTIK1013_AXIS_Z],
		obj->cali_sw[KXTIK1013_AXIS_X], obj->cali_sw[KXTIK1013_AXIS_Y], obj->cali_sw[KXTIK1013_AXIS_Z]);

	if(err = hwmsen_write_block(obj->client, KXTIK1013_REG_OFSX, obj->offset, KXTIK1013_AXES_NUM))
	{
		GSE_ERR("write offset fail: %d\n", err);
		return err;
	}
#endif

	return err;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_CheckDeviceID(struct i2c_client *client)
{
	u8 databuf[10];    
	int res = 0;

	memset(databuf, 0, sizeof(u8)*10);    
	databuf[0] = KXTIK1013_REG_DEVID;   
	res = i2c_master_send(client, databuf, 0x1);
	if (res <= 0)
	{
		GSE_ERR("Gsensor : first i2c_master_send failed!\n");
		mdelay(100);
	}

	res = i2c_master_send(client, databuf, 0x1);
	if(res <= 0)
	{
		GSE_ERR("Gsensor : i2c_master_send failed.\n");
		goto exit_KXTIK1013_CheckDeviceID;
	}
	
	udelay(500);

	databuf[0] = 0x0;        
	res = i2c_master_recv(client, databuf, 0x01);
	if(res <= 0)
	{
		GSE_ERR("Gsensor : i2c_master_recv failed.\n");
		goto exit_KXTIK1013_CheckDeviceID;
	}
	
	GSE_LOG("KXTIK1013_CheckDeviceID 0x%x pass!\n ", databuf[0]);
	return KXTIK1013_SUCCESS;

exit_KXTIK1013_CheckDeviceID:
	return KXTIK1013_ERR_I2C;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_SetPowerMode(struct i2c_client *client, bool enable)
{
	u8 databuf[2];    
	int res = 0;
	u8 addr = KXTIK1013_REG_POWER_CTL;
	//struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	
/*
	if(enable == sensor_power)
	{
		GSE_INFO("Sensor power status is newest!\n");
		return KXTIK1013_SUCCESS;
	}
*/

	if(hwmsen_read_block(client, addr, databuf, 0x01))
	{
		GSE_ERR("read power ctl register err!\n");
		return KXTIK1013_ERR_I2C;
	}

	
	if(enable == TRUE)
	{
		databuf[0] |= KXTIK1013_MEASURE_MODE;
	}
	else
	{
		databuf[0] &= ~KXTIK1013_MEASURE_MODE;
	}
	databuf[1] = databuf[0];
	databuf[0] = KXTIK1013_REG_POWER_CTL;
	

	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
		return KXTIK1013_ERR_I2C;
	}

	GSE_INFO("KXTIK1013_SetPowerMode = %d  enable register value = %x \n ", enable, databuf[1]);

	sensor_power = enable;
	mdelay(5);
	
	return KXTIK1013_SUCCESS;    
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_SetDataFormat(struct i2c_client *client, u8 dataformat)
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	u8 databuf[10];    
	int res = 0;

	memset(databuf, 0, sizeof(u8)*10);  

	KXTIK1013_SetPowerMode(client, false);

	if(hwmsen_read_block(client, KXTIK1013_REG_DATA_FORMAT, databuf, 0x01))
	{
		printk("kxtik1013 read Dataformat failt \n");
		return KXTIK1013_ERR_I2C;
	}

	databuf[0] &= ~KXTIK1013_RANGE_MASK;
	databuf[0] |= dataformat;
	databuf[1] = databuf[0];
	databuf[0] = KXTIK1013_REG_DATA_FORMAT;


	res = i2c_master_send(client, databuf, 0x2);

	if(res <= 0)
	{
		return KXTIK1013_ERR_I2C;
	}

	KXTIK1013_SetPowerMode(client, true);
	
	GSE_INFO("KXTIK1013_SetDataFormat OK! \n");
	

	return KXTIK1013_SetDataResolution(obj);    
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_SetBWRate(struct i2c_client *client, u8 bwrate)
{
	u8 databuf[10];    
	int res = 0;

	memset(databuf, 0, sizeof(u8)*10);    

	if(hwmsen_read_block(client, KXTIK1013_REG_BW_RATE, databuf, 0x01))
	{
		printk("kxtik1013 read rate failt \n");
		return KXTIK1013_ERR_I2C;
	}

	databuf[0] &= 0xf8;
	databuf[0] |= bwrate;
	databuf[1] = databuf[0];
	databuf[0] = KXTIK1013_REG_BW_RATE;


	res = i2c_master_send(client, databuf, 0x2);

	if(res <= 0)
	{
		return KXTIK1013_ERR_I2C;
	}
	
	printk("KXTIK1013_SetBWRate OK! \n");
	
	return KXTIK1013_SUCCESS;    
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_SetIntEnable(struct i2c_client *client, u8 intenable)
{
	u8 databuf[10];    
	int res = 0;

	memset(databuf, 0, sizeof(u8)*10);    
	databuf[0] = KXTIK1013_REG_INT_ENABLE;    
	databuf[1] = 0x00;

	res = i2c_master_send(client, databuf, 0x2);

	if(res <= 0)
	{
		return KXTIK1013_ERR_I2C;
	}
	
	return KXTIK1013_SUCCESS;    
}

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
static int KXTIK1013_SetMotion(struct i2c_client *client, u8 dataformat)
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	u8 databuf[10];    
	int res = 0;
	memset(databuf, 0, sizeof(u8)*10);  
	KXTIK1013_SetPowerMode(client, false);

	databuf[0] = KXTIK1013_REG_CTL_REG3;        //0x1D
	databuf[1] = 0x06;
	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
        printk("kxtik1013 set motion register 0x1D failed");
		return KXTIK1013_ERR_I2C;

	}

	databuf[0] = KXTIK1013_WAKEUP_TIMER;
	databuf[1] = 0x03;
	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
        printk("kxtik1013 set motion wakeup timer failed");
		return KXTIK1013_ERR_I2C;
	}

	databuf[0] = KXTIK1013_WAKEUP_THRESHOLD;
	databuf[1] = 0x02;
	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
        printk("kxtik1013 set motion wakeup THRESHOLD failed");
		return KXTIK1013_ERR_I2C;
	}
	databuf[0] = KXTIK1013_REG_INT_CTRL2;        //0x1F
	databuf[1] = 0x03;
	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
        printk("kxtik1013 set motion reg int enable 0x1e failed");
		return KXTIK1013_ERR_I2C;
	}

	databuf[0] = KXTIK1013_REG_INT_ENABLE;        //0x1E
	databuf[1] = 0x20;
	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
        printk("kxtik1013 set motion reg int enable 0x1e failed");
		return KXTIK1013_ERR_I2C;
	}

	if(hwmsen_read_block(client, KXTIK1013_MOTION_REG, databuf, 0x01))
	{
		printk("kxtik1013 read Dataformat failt \n");
		return KXTIK1013_ERR_I2C;
	}
    if (obj->motion_detect_enable) {
        databuf[0] |= dataformat;
        databuf[1] = databuf[0];
        databuf[0] = KXTIK1013_MOTION_REG;
    } else {
        databuf[0] &= ~dataformat;
        databuf[1] = databuf[0];
        databuf[0] = KXTIK1013_MOTION_REG;
    }

	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
        printk("kxtik1013 set motion enable failed");
		return KXTIK1013_ERR_I2C;
	}

	KXTIK1013_SetPowerMode(client, true);
	GSE_INFO("KXTIK1013_SetMotion  OK! \n");
	return KXTIK1013_SetDataResolution(obj);    
}

static void kxtik1013_eint_work_func(struct work_struct *work)
{

    u8 data = 0;
    hwm_sensor_data sensor_data;
    int err;
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
    u8 data_status_reg = 0;
    u8 data_source_reg1 = 0; 
    u8 data_source_reg2 = 0; 
    GSE_FUN();
    if (obj == NULL) {
        GSE_ERR("%s obj is null", __func__);
    }

	err = hwmsen_read_byte(obj->client, KXTIK1013_STATUS_REG, &data_status_reg);
	GSE_INFO("read the resister 0x18 = %x\n", data_status_reg);
    if (err != KXTIK1013_SUCCESS || data_status_reg != 0x10) {
	    GSE_ERR("read the resister 0x18 error\n");
        return;
    }
	err = hwmsen_read_byte(obj->client, KXTIK1013_SOURCE1_REG, &data_source_reg1);
	GSE_INFO("read the resister 0x16 = %x\n", data_source_reg1);
    if (err != KXTIK1013_SUCCESS || ((data_source_reg1 & 0x02) != 0x02)) {
	    GSE_ERR("read the resister 0x16 error\n");
        return;
    }
	err = hwmsen_read_byte(obj->client, KXTIK1013_SOURCE2_REG, &data_source_reg2);
	GSE_INFO("read the resister 0x17 = %x\n", data_source_reg2);
    sensor_data.values[0] = 1;
    sensor_data.value_divide = 1;
    sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
    if (err = hwmsen_get_interrupt_data(ID_PICK_UP_GESTURE, &sensor_data)) {
        GSE_ERR("call hwmsen_get_interrupt_data fail = %d\n", err);
        return;
    }

    //mt_eint_unmask(CUST_EINT_GSE_1_NUM);
	GSE_INFO("gsensor eint occored\n");
    obj->motion_detect_enable = false;    
    kxtik1013_set_motion_enable(obj->client, obj->motion_detect_enable);
}

/*----------------------------------------------------------------------------*/
static void kxtik1013_eint_func(void) 
{
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
    if (obj == NULL) {
        GSE_ERR("%s obj is null", __func__);
    }
    schedule_work(&obj->kxtik1013_eint_work);
    
}
/*----------------------------------------------------------------------------*/
static int kxtik1013_setup_eint(struct i2c_client *client)
{
	GSE_FUN();
    mt_set_gpio_dir(GPIO_GSE_1_EINT_PIN, GPIO_DIR_IN);
    mt_set_gpio_mode(GPIO_GSE_1_EINT_PIN, GPIO_GSE_1_EINT_PIN_M_EINT);
    mt_set_gpio_pull_enable(GPIO_GSE_1_EINT_PIN, GPIO_PULL_ENABLE);
    mt_set_gpio_pull_select(GPIO_GSE_1_EINT_PIN, GPIO_PULL_UP);

    mt_eint_registration(CUST_EINT_GSE_1_NUM, CUST_EINT_GSE_1_TYPE, kxtik1013_eint_func, 0);
    mt_eint_unmask(CUST_EINT_GSE_1_NUM);
}
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

/*----------------------------------------------------------------------------*/
static int kxtik1013_init_client(struct i2c_client *client, int reset_cali)
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	int res = 0;

	res = KXTIK1013_CheckDeviceID(client); 
	if(res != KXTIK1013_SUCCESS)
	{
		GSE_ERR("Gsensor : check device ID failed.\n");
		return res;
	}	

	res = KXTIK1013_SetPowerMode(client, false);
	if(res != KXTIK1013_SUCCESS)
	{	
		GSE_ERR("Gsensor : set power mode failed.\n");
		return res;
	}
	

	res = KXTIK1013_SetBWRate(client, KXTIK1013_BW_100HZ);
	if(res != KXTIK1013_SUCCESS ) //0x2C->BW=100Hz
	{	
		GSE_ERR("Gsensor : set BW rate failed.\n");
		return res;
	}

	res = KXTIK1013_SetDataFormat(client, KXTIK1013_RANGE_2G);
	if(res != KXTIK1013_SUCCESS) //0x2C->BW=100Hz
	{
		GSE_ERR("Gsensor : set data format failed.\n");
		return res;
	}

	gsensor_gain.x = gsensor_gain.y = gsensor_gain.z = obj->reso->sensitivity;

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if !defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
	res = KXTIK1013_SetIntEnable(client, 0x00);        
	if(res != KXTIK1013_SUCCESS)//0x2E->0x80
	{
		GSE_ERR("Gsensor : set Int Enable failed.\n");
		return res;
	}
#else
/*
	res = KXTIK1013_SetMotion(client, KXTIK1013_MOTION);
	if(res != KXTIK1013_SUCCESS) //0x2C->BW=100Hz
	{
		GSE_ERR("Gsensor : set data format failed.\n");
		return res;
	}
*/
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

	if(0 != reset_cali)
	{ 
		/*reset calibration only in power on*/
		res = KXTIK1013_ResetCalibration(client);
		if(res != KXTIK1013_SUCCESS)
		{
			GSE_ERR("Gsensor : reset calibration failed.\n");
			return res;
		}
	}

	GSE_INFO("kxtik1013_init_client OK!\n");
#ifdef CONFIG_KXTIK1013_LOWPASS
	memset(&obj->fir, 0x00, sizeof(obj->fir));  
#endif

	return KXTIK1013_SUCCESS;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ReadChipInfo(struct i2c_client *client, char *buf, int bufsize)
{
	u8 databuf[10];    

	memset(databuf, 0, sizeof(u8)*10);

	if((NULL == buf)||(bufsize<=30))
	{
		return -1;
	}
	
	if(NULL == client)
	{
		*buf = 0;
		return -2;
	}

	sprintf(buf, "KXTIK1013 Chip");
	return 0;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ReadSensorData(struct i2c_client *client, char *buf, int bufsize)
{
	struct kxtik1013_i2c_data *obj = (struct kxtik1013_i2c_data*)i2c_get_clientdata(client);
	u8 databuf[20];
	int acc[KXTIK1013_AXES_NUM];
	int res = 0;
	memset(databuf, 0, sizeof(u8)*10);

	if(NULL == buf)
	{
		return -1;
	}
	if(NULL == client)
	{
		*buf = 0;
		return -2;
	}

	if(sensor_power == FALSE)
	{
		res = KXTIK1013_SetPowerMode(client, true);
		if(res)
		{
			GSE_ERR("Power on kxtik1013 error %d!\n", res);
		}
	}

	if((res = KXTIK1013_ReadData(client, obj->data)))
	{        
		GSE_ERR("I2C error: ret value=%d", res);
		return -3;
	}
	else
	{
		//printk("raw data x=%d, y=%d, z=%d \n",obj->data[KXTIK1013_AXIS_X],obj->data[KXTIK1013_AXIS_Y],obj->data[KXTIK1013_AXIS_Z]);
		obj->data[KXTIK1013_AXIS_X] += obj->cali_sw[KXTIK1013_AXIS_X];
		obj->data[KXTIK1013_AXIS_Y] += obj->cali_sw[KXTIK1013_AXIS_Y];
		obj->data[KXTIK1013_AXIS_Z] += obj->cali_sw[KXTIK1013_AXIS_Z];
		
		//printk("cali_sw x=%d, y=%d, z=%d \n",obj->cali_sw[KXTIK1013_AXIS_X],obj->cali_sw[KXTIK1013_AXIS_Y],obj->cali_sw[KXTIK1013_AXIS_Z]);
		
		/*remap coordinate*/
		acc[obj->cvt.map[KXTIK1013_AXIS_X]] = obj->cvt.sign[KXTIK1013_AXIS_X]*obj->data[KXTIK1013_AXIS_X];
		acc[obj->cvt.map[KXTIK1013_AXIS_Y]] = obj->cvt.sign[KXTIK1013_AXIS_Y]*obj->data[KXTIK1013_AXIS_Y];
		acc[obj->cvt.map[KXTIK1013_AXIS_Z]] = obj->cvt.sign[KXTIK1013_AXIS_Z]*obj->data[KXTIK1013_AXIS_Z];
		//printk("cvt x=%d, y=%d, z=%d \n",obj->cvt.sign[KXTIK1013_AXIS_X],obj->cvt.sign[KXTIK1013_AXIS_Y],obj->cvt.sign[KXTIK1013_AXIS_Z]);


		//GSE_LOG("Mapped gsensor data: %d, %d, %d!\n", acc[KXTIK1013_AXIS_X], acc[KXTIK1013_AXIS_Y], acc[KXTIK1013_AXIS_Z]);

		//Out put the mg
		//printk("mg acc=%d, GRAVITY=%d, sensityvity=%d \n",acc[KXTIK1013_AXIS_X],GRAVITY_EARTH_1000,obj->reso->sensitivity);
		acc[KXTIK1013_AXIS_X] = acc[KXTIK1013_AXIS_X] * GRAVITY_EARTH_1000 / obj->reso->sensitivity;
		acc[KXTIK1013_AXIS_Y] = acc[KXTIK1013_AXIS_Y] * GRAVITY_EARTH_1000 / obj->reso->sensitivity;
		acc[KXTIK1013_AXIS_Z] = acc[KXTIK1013_AXIS_Z] * GRAVITY_EARTH_1000 / obj->reso->sensitivity;		
		
	

		sprintf(buf, "%04x %04x %04x", acc[KXTIK1013_AXIS_X], acc[KXTIK1013_AXIS_Y], acc[KXTIK1013_AXIS_Z]);
		if(atomic_read(&obj->trace) & ADX_TRC_IOCTL)
		{
			GSE_LOG("gsensor data: %s!\n", buf);
		}
	}
	
	return 0;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_ReadRawData(struct i2c_client *client, char *buf)
{
	struct kxtik1013_i2c_data *obj = (struct kxtik1013_i2c_data*)i2c_get_clientdata(client);
	int res = 0;

	if (!buf || !client)
	{
		return EINVAL;
	}
	
	if((res = KXTIK1013_ReadData(client, obj->data)))
	{        
		GSE_ERR("I2C error: ret value=%d", res);
		return EIO;
	}
	else
	{
		sprintf(buf, "KXTIK1013_ReadRawData %04x %04x %04x", obj->data[KXTIK1013_AXIS_X], 
			obj->data[KXTIK1013_AXIS_Y], obj->data[KXTIK1013_AXIS_Z]);
	
	}
	
	return 0;
}
/*----------------------------------------------------------------------------*/
static int KXTIK1013_InitSelfTest(struct i2c_client *client)
{
	int res = 0;
	u8  data,result;
	
	res = hwmsen_read_byte(client, KXTIK1013_REG_CTL_REG3, &data);
	if(res != KXTIK1013_SUCCESS)
	{
		return res;
	}
//enable selftest bit
	res = hwmsen_write_byte(client, KXTIK1013_REG_CTL_REG3,  KXTIK1013_SELF_TEST|data);
	if(res != KXTIK1013_SUCCESS) //0x2C->BW=100Hz
	{
		return res;
	}
//step 1
	res = hwmsen_read_byte(client, KXTIK1013_DCST_RESP, &result);
	if(res != KXTIK1013_SUCCESS)
	{
		return res;
	}
	printk("step1: result = %x",result);
	if(result != 0xaa)
		return -EINVAL;

//step 2
	res = hwmsen_write_byte(client, KXTIK1013_REG_CTL_REG3,  KXTIK1013_SELF_TEST|data);
	if(res != KXTIK1013_SUCCESS) //0x2C->BW=100Hz
	{
		return res;
	}
//step 3
	res = hwmsen_read_byte(client, KXTIK1013_DCST_RESP, &result);
	if(res != KXTIK1013_SUCCESS)
	{
		return res;
	}
	printk("step3: result = %x",result);
	if(result != 0xAA)
		return -EINVAL;
		
//step 4
	res = hwmsen_read_byte(client, KXTIK1013_DCST_RESP, &result);
	if(res != KXTIK1013_SUCCESS)
	{
		return res;
	}
	printk("step4: result = %x",result);
	if(result != 0x55)
		return -EINVAL;
	else
		return KXTIK1013_SUCCESS;
}
/*----------------------------------------------------------------------------*/
/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
static int kxtik1013_set_motion_enable(struct i2c_client *client, bool enable)
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	u8 databuf[2] = {0,0};    
    u8 data;
	int res = 0;
    int retry = 3;

    mutex_lock(&kx1013_mutex);
	KXTIK1013_SetPowerMode(client, false);

	if(hwmsen_read_block(client, KXTIK1013_MOTION_REG, databuf, 0x01))
	{
		printk("[Gsensor]: kxtik1013 read Dataformat failt \n");
		return KXTIK1013_ERR_I2C;
	}
	printk("[Gsensor]: %s enable = %d \n", __func__, enable);
    if (enable) {
        databuf[0] |= KXTIK1013_MOTION;
        databuf[1] = databuf[0];
        databuf[0] = KXTIK1013_MOTION_REG;
    } else {
        databuf[0] &= ~KXTIK1013_MOTION;
        databuf[1] = databuf[0];
        databuf[0] = KXTIK1013_MOTION_REG;
    }

	res = i2c_master_send(client, databuf, 0x2);
	if(res <= 0)
	{
        printk("[Gsensor]: kxtik1013 set motion enable failed");
		return KXTIK1013_ERR_I2C;
	}

    hwmsen_read_block(client, KXTIK1013_MOTION_REG, &data, 0x01);
    while((retry > 0) && data != databuf[1]) {
        printk("[Gsensor]: @@@@@@@set the motion again databuf[0] = %x, databuf[1] = %x@@@@@@@@\n", databuf[0], databuf[1]);
        mdelay(5);
	    i2c_master_send(client, databuf, 0x2);
        retry--;
        hwmsen_read_block(client, KXTIK1013_MOTION_REG, &data, 0x01);
    }
	KXTIK1013_SetPowerMode(client, true);
    //hwmsen_read_byte(client, KXTIK1013_INT_REL, &databuf);
	printk("[Gsensor]: %s OK \n", __func__);
    mutex_unlock(&kx1013_mutex);

	return res;    
}
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

/*----------------------------------------------------------------------------*/
static ssize_t show_chipinfo_value(struct device_driver *ddri, char *buf)
{
	struct i2c_client *client = kxtik1013_i2c_client;
	char strbuf[KXTIK1013_BUFSIZE];
	if(NULL == client)
	{
		GSE_ERR("i2c client is null!!\n");
		return 0;
	}
	
	KXTIK1013_ReadChipInfo(client, strbuf, KXTIK1013_BUFSIZE);
	return snprintf(buf, PAGE_SIZE, "%s\n", strbuf);        
}
/*
static ssize_t gsensor_init(struct device_driver *ddri, char *buf, size_t count)
{
		struct i2c_client *client = kxtik1013_i2c_client;
		char strbuf[KXTIK1013_BUFSIZE];
		
		if(NULL == client)
		{
			GSE_ERR("i2c client is null!!\n");
			return 0;
		}
		kxtik1013_init_client(client, 1);
		return snprintf(buf, PAGE_SIZE, "%s\n", strbuf);			
}
*/


/*----------------------------------------------------------------------------*/
static ssize_t show_sensordata_value(struct device_driver *ddri, char *buf)
{
	struct i2c_client *client = kxtik1013_i2c_client;
	char strbuf[KXTIK1013_BUFSIZE];
	
	if(NULL == client)
	{
		GSE_ERR("i2c client is null!!\n");
		return 0;
	}
	KXTIK1013_ReadSensorData(client, strbuf, KXTIK1013_BUFSIZE);
	//KXTIK1013_ReadRawData(client, strbuf);
	return snprintf(buf, PAGE_SIZE, "%s\n", strbuf);            
}
/*
static ssize_t show_sensorrawdata_value(struct device_driver *ddri, char *buf, size_t count)
{
		struct i2c_client *client = kxtik1013_i2c_client;
		char strbuf[KXTIK1013_BUFSIZE];
		
		if(NULL == client)
		{
			GSE_ERR("i2c client is null!!\n");
			return 0;
		}
		//KXTIK1013_ReadSensorData(client, strbuf, KXTIK1013_BUFSIZE);
		KXTIK1013_ReadRawData(client, strbuf);
		return snprintf(buf, PAGE_SIZE, "%s\n", strbuf);			
}
*/
/*----------------------------------------------------------------------------*/
static ssize_t show_cali_value(struct device_driver *ddri, char *buf)
{
	struct i2c_client *client = kxtik1013_i2c_client;
	struct kxtik1013_i2c_data *obj;
	int err, len = 0, mul;
	int tmp[KXTIK1013_AXES_NUM];

	if(NULL == client)
	{
		GSE_ERR("i2c client is null!!\n");
		return 0;
	}

	obj = i2c_get_clientdata(client);

	if((err = KXTIK1013_ReadOffset(client, obj->offset)))
	{
		return -EINVAL;
	}
	else if((err = KXTIK1013_ReadCalibration(client, tmp)))
	{
		return -EINVAL;
	}
	else
	{    
		mul = obj->reso->sensitivity/kxtik1013_offset_resolution.sensitivity;
		len += snprintf(buf+len, PAGE_SIZE-len, "[HW ][%d] (%+3d, %+3d, %+3d) : (0x%02X, 0x%02X, 0x%02X)\n", mul,                        
			obj->offset[KXTIK1013_AXIS_X], obj->offset[KXTIK1013_AXIS_Y], obj->offset[KXTIK1013_AXIS_Z],
			obj->offset[KXTIK1013_AXIS_X], obj->offset[KXTIK1013_AXIS_Y], obj->offset[KXTIK1013_AXIS_Z]);
		len += snprintf(buf+len, PAGE_SIZE-len, "[SW ][%d] (%+3d, %+3d, %+3d)\n", 1, 
			obj->cali_sw[KXTIK1013_AXIS_X], obj->cali_sw[KXTIK1013_AXIS_Y], obj->cali_sw[KXTIK1013_AXIS_Z]);

		len += snprintf(buf+len, PAGE_SIZE-len, "[ALL]    (%+3d, %+3d, %+3d) : (%+3d, %+3d, %+3d)\n", 
			obj->offset[KXTIK1013_AXIS_X]*mul + obj->cali_sw[KXTIK1013_AXIS_X],
			obj->offset[KXTIK1013_AXIS_Y]*mul + obj->cali_sw[KXTIK1013_AXIS_Y],
			obj->offset[KXTIK1013_AXIS_Z]*mul + obj->cali_sw[KXTIK1013_AXIS_Z],
			tmp[KXTIK1013_AXIS_X], tmp[KXTIK1013_AXIS_Y], tmp[KXTIK1013_AXIS_Z]);
		
		return len;
    }
}
/*----------------------------------------------------------------------------*/
static ssize_t store_cali_value(struct device_driver *ddri, const char *buf, size_t count)
{
	struct i2c_client *client = kxtik1013_i2c_client;  
	int err, x, y, z;
	int dat[KXTIK1013_AXES_NUM];

	if(!strncmp(buf, "rst", 3))
	{
		if((err = KXTIK1013_ResetCalibration(client)))
		{
			GSE_ERR("reset offset err = %d\n", err);
		}	
	}
	else if(3 == sscanf(buf, "0x%02X 0x%02X 0x%02X", &x, &y, &z))
	{
		dat[KXTIK1013_AXIS_X] = x;
		dat[KXTIK1013_AXIS_Y] = y;
		dat[KXTIK1013_AXIS_Z] = z;
		if((err = KXTIK1013_WriteCalibration(client, dat)))
		{
			GSE_ERR("write calibration err = %d\n", err);
		}		
	}
	else
	{
		GSE_ERR("invalid format\n");
	}
	
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t show_self_value(struct device_driver *ddri, char *buf)
{
	struct i2c_client *client = kxtik1013_i2c_client;
	//struct kxtik1013_i2c_data *obj;

	if(NULL == client)
	{
		GSE_ERR("i2c client is null!!\n");
		return 0;
	}

	//obj = i2c_get_clientdata(client);
	
    return snprintf(buf, 8, "%s\n", selftestRes);
}
/*----------------------------------------------------------------------------*/
static ssize_t store_self_value(struct device_driver *ddri, const char *buf, size_t count)
{   /*write anything to this register will trigger the process*/
	struct item{
	s16 raw[KXTIK1013_AXES_NUM];
	};
	
	struct i2c_client *client = kxtik1013_i2c_client;  
	int res, num;
	struct item *prv = NULL, *nxt = NULL;
	//s32 avg_prv[KXTIK1013_AXES_NUM] = {0, 0, 0};
	//s32 avg_nxt[KXTIK1013_AXES_NUM] = {0, 0, 0};
	u8 data;


	if(1 != sscanf(buf, "%d", &num))
	{
		GSE_ERR("parse number fail\n");
		return count;
	}
	else if(num == 0)
	{
		GSE_ERR("invalid data count\n");
		return count;
	}

	prv = kzalloc(sizeof(*prv) * num, GFP_KERNEL);
	nxt = kzalloc(sizeof(*nxt) * num, GFP_KERNEL);
	if (!prv || !nxt)
	{
		goto exit;
	}


	GSE_LOG("NORMAL:\n");
	KXTIK1013_SetPowerMode(client,true); 

	/*initial setting for self test*/
	if(!KXTIK1013_InitSelfTest(client))
	{
		GSE_LOG("SELFTEST : PASS\n");
		strcpy(selftestRes,"y");
	}	
	else
	{
		GSE_LOG("SELFTEST : FAIL\n");		
		strcpy(selftestRes,"n");
	}

	res = hwmsen_read_byte(client, KXTIK1013_REG_CTL_REG3, &data);
	if(res != KXTIK1013_SUCCESS)
	{
		return res;
	}

	res = hwmsen_write_byte(client, KXTIK1013_REG_CTL_REG3,  ~KXTIK1013_SELF_TEST&data);
	if(res != KXTIK1013_SUCCESS) //0x2C->BW=100Hz
	{
		return res;
	}
	
exit:
	/*restore the setting*/    
	kxtik1013_init_client(client, 0);
	kfree(prv);
	kfree(nxt);
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t show_selftest_value(struct device_driver *ddri, char *buf)
{
	struct i2c_client *client = kxtik1013_i2c_client;
	struct kxtik1013_i2c_data *obj;

	if(NULL == client)
	{
		GSE_ERR("i2c client is null!!\n");
		return 0;
	}

	obj = i2c_get_clientdata(client);
	return snprintf(buf, PAGE_SIZE, "%d\n", atomic_read(&obj->selftest));
}
/*----------------------------------------------------------------------------*/
static ssize_t store_selftest_value(struct device_driver *ddri, const char *buf, size_t count)
{
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
	int tmp;

	if(NULL == obj)
	{
		GSE_ERR("i2c data obj is null!!\n");
		return 0;
	}
	
	
	if(1 == sscanf(buf, "%d", &tmp))
	{        
		if(atomic_read(&obj->selftest) && !tmp)
		{
			/*enable -> disable*/
			kxtik1013_init_client(obj->client, 0);
		}
		else if(!atomic_read(&obj->selftest) && tmp)
		{
			/*disable -> enable*/
			KXTIK1013_InitSelfTest(obj->client);            
		}
		
		GSE_LOG("selftest: %d => %d\n", atomic_read(&obj->selftest), tmp);
		atomic_set(&obj->selftest, tmp); 
	}
	else
	{ 
		GSE_ERR("invalid content: '%s', length = %zd\n", buf, count);   
	}
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t show_firlen_value(struct device_driver *ddri, char *buf)
{
#ifdef CONFIG_KXTIK1013_LOWPASS
	struct i2c_client *client = kxtik1013_i2c_client;
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	if(atomic_read(&obj->firlen))
	{
		int idx, len = atomic_read(&obj->firlen);
		GSE_LOG("len = %2d, idx = %2d\n", obj->fir.num, obj->fir.idx);

		for(idx = 0; idx < len; idx++)
		{
			GSE_LOG("[%5d %5d %5d]\n", obj->fir.raw[idx][KXTIK1013_AXIS_X], obj->fir.raw[idx][KXTIK1013_AXIS_Y], obj->fir.raw[idx][KXTIK1013_AXIS_Z]);
		}
		
		GSE_LOG("sum = [%5d %5d %5d]\n", obj->fir.sum[KXTIK1013_AXIS_X], obj->fir.sum[KXTIK1013_AXIS_Y], obj->fir.sum[KXTIK1013_AXIS_Z]);
		GSE_LOG("avg = [%5d %5d %5d]\n", obj->fir.sum[KXTIK1013_AXIS_X]/len, obj->fir.sum[KXTIK1013_AXIS_Y]/len, obj->fir.sum[KXTIK1013_AXIS_Z]/len);
	}
	return snprintf(buf, PAGE_SIZE, "%d\n", atomic_read(&obj->firlen));
#else
	return snprintf(buf, PAGE_SIZE, "not support\n");
#endif
}
/*----------------------------------------------------------------------------*/
static ssize_t store_firlen_value(struct device_driver *ddri, const char *buf, size_t count)
{
#ifdef CONFIG_KXTIK1013_LOWPASS
	struct i2c_client *client = kxtik1013_i2c_client;  
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);
	int firlen;

	if(1 != sscanf(buf, "%d", &firlen))
	{
		GSE_ERR("invallid format\n");
	}
	else if(firlen > C_MAX_FIR_LENGTH)
	{
		GSE_ERR("exceeds maximum filter length\n");
	}
	else
	{ 
		atomic_set(&obj->firlen, firlen);
		if(NULL == firlen)
		{
			atomic_set(&obj->fir_en, 0);
		}
		else
		{
			memset(&obj->fir, 0x00, sizeof(obj->fir));
			atomic_set(&obj->fir_en, 1);
		}
	}
#endif    
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t show_trace_value(struct device_driver *ddri, char *buf)
{
	ssize_t res;
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
	if (obj == NULL)
	{
		GSE_ERR("i2c_data obj is null!!\n");
		return 0;
	}
	
	res = snprintf(buf, PAGE_SIZE, "0x%04X\n", atomic_read(&obj->trace));     
	return res;    
}
/*----------------------------------------------------------------------------*/
static ssize_t store_trace_value(struct device_driver *ddri, const char *buf, size_t count)
{
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
	int trace;
	if (obj == NULL)
	{
		GSE_ERR("i2c_data obj is null!!\n");
		return 0;
	}
	
	if(1 == sscanf(buf, "0x%x", &trace))
	{
		atomic_set(&obj->trace, trace);
	}	
	else
	{
		GSE_ERR("invalid content: '%s', length = %zd\n", buf, count);
	}
	
	return count;    
}
/*----------------------------------------------------------------------------*/
static ssize_t show_status_value(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;    
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
	if (obj == NULL)
	{
		GSE_ERR("i2c_data obj is null!!\n");
		return 0;
	}	
	
	if(obj->hw)
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "CUST: %d %d (%d %d)\n", 
	            obj->hw->i2c_num, obj->hw->direction, obj->hw->power_id, obj->hw->power_vol);   
	}
	else
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "CUST: NULL\n");
	}
	return len;    
}
/*----------------------------------------------------------------------------*/
static ssize_t show_power_status_value(struct device_driver *ddri, char *buf)
{
	if(sensor_power)
		GSE_LOG("G sensor is in work mode, sensor_power = %d\n", sensor_power);
	else
		GSE_LOG("G sensor is in standby mode, sensor_power = %d\n", sensor_power);

	return 0;
}

/*----------------------------------------------------------------------------*/
static ssize_t show_kx1013_reg_value(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;    
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
    char *tmp_str;
    u8 reg_buf;
    int i;
	if (obj == NULL)
	{
		GSE_ERR("i2c_data obj is null!!\n");
		return 0;
	}	
    for (i=0; i <= 0x21; i++)
    {
        tmp_str = buf + len;
        hwmsen_read_byte(obj->client,i,&reg_buf);
        len += sprintf(tmp_str, "[reg 0x%x = 0x%x]\n", i,reg_buf);
    }

    tmp_str = buf + len;
    hwmsen_read_byte(obj->client, 0x29, &reg_buf);          // timer threshold
    len += sprintf(tmp_str, "[reg 0x29 = 0x%x]\n", reg_buf);

    tmp_str = buf + len;
    hwmsen_read_byte(obj->client, 0x6A, &reg_buf);          //amplitude threshold
    len += sprintf(tmp_str, "[reg 0x6A = 0x%x]\n", reg_buf);	
	return len;    
}

/*----------------------------------------------------------------------------*/
#define IS_SPACE(CH)   (((CH) == ' ') || ((CH) == '\n'))
static int read_int_from_buff(const char* buf, size_t count, u8 data[], int len)
{
    int idx = 0;
    char *cur = (char*)buf;
    char *end = (char*)(buf+count);
    int ret = 0;
    
    while(idx < len) {

        while((cur < end) && IS_SPACE(*cur))
        {
            cur++;
        }
        ret = sscanf(cur, "%hhd", &data[idx]);
        if (ret != 1) {
            GSE_ERR("sscanf failed");
        }
        idx ++;
        while((cur < end) && !IS_SPACE(*cur)) {
            cur ++;
        }

    }
    return idx;
}


static ssize_t store_kx1013_reg_value(struct device_driver *ddri, const char *buf, size_t count)
{
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
	u8 databuf[2];  
	unsigned long input_value;
	int res;
	memset(databuf, 0, sizeof(databuf)/sizeof(databuf[0]));    

    GSE_FUN();
    res = read_int_from_buff(buf, count, databuf, 2);
    if (2 != res) {
        GSE_ERR("%s read int from buff failed", __func__);
    }

	res = i2c_master_send(obj->client, databuf, 0x2);
    if (res <= 0) {

        GSE_ERR("%s i2c master send failed", __func__);
    }
	return count;
	
}
/*----------------------------------------------------------------------------*/
static DRIVER_ATTR(chipinfo,   S_IWUSR | S_IRUGO, show_chipinfo_value,      NULL);
static DRIVER_ATTR(sensordata, S_IWUSR | S_IRUGO, show_sensordata_value,    NULL);
static DRIVER_ATTR(cali,       S_IWUSR | S_IRUGO, show_cali_value,          store_cali_value);
static DRIVER_ATTR(selftest, S_IWUSR | S_IRUGO, show_self_value,  store_self_value);
static DRIVER_ATTR(self,   S_IWUSR | S_IRUGO, show_selftest_value,      store_selftest_value);
static DRIVER_ATTR(firlen,     S_IWUSR | S_IRUGO, show_firlen_value,        store_firlen_value);
static DRIVER_ATTR(trace,      S_IWUSR | S_IRUGO, show_trace_value,         store_trace_value);
static DRIVER_ATTR(status,               S_IRUGO, show_status_value,        NULL);
static DRIVER_ATTR(powerstatus,               S_IRUGO, show_power_status_value,        NULL);

static DRIVER_ATTR(regvalue,              S_IWUSR | S_IRUGO, show_kx1013_reg_value,        store_kx1013_reg_value);

/*----------------------------------------------------------------------------*/
static u8 i2c_dev_reg =0 ;

static ssize_t show_register(struct device_driver *pdri, char *buf)
{
	//int input_value;
		
	GSE_LOG("i2c_dev_reg is 0x%2x \n", i2c_dev_reg);

	return 0;
}

static ssize_t store_register(struct device_driver *ddri, const char *buf, size_t count)
{
	//unsigned long input_value;

	i2c_dev_reg = simple_strtoul(buf, NULL, 16);
	GSE_LOG("set i2c_dev_reg = 0x%2x \n", i2c_dev_reg);

	return 0;
}
static ssize_t store_register_value(struct device_driver *ddri, const char *buf, size_t count)
{
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
	u8 databuf[2];  
	unsigned long input_value;
	int res;
	
	memset(databuf, 0, sizeof(u8)*2);    

	input_value = simple_strtoul(buf, NULL, 16);
	GSE_LOG("input_value = 0x%2lx \n", input_value);

	if(NULL == obj)
	{
		GSE_ERR("i2c data obj is null!!\n");
		return 0;
	}

	databuf[0] = i2c_dev_reg;
	databuf[1] = input_value;
	GSE_LOG("databuf[0]=0x%2x  databuf[1]=0x%2x \n", databuf[0],databuf[1]);

	res = i2c_master_send(obj->client, databuf, 0x2);

	if(res <= 0)
	{
		return KXTIK1013_ERR_I2C;
	}
	return 0;
	
}

static ssize_t show_register_value(struct device_driver *ddri, char *buf)
{
	struct kxtik1013_i2c_data *obj = obj_i2c_data;
	u8 databuf[1];	
	
	memset(databuf, 0, sizeof(u8)*1);	 

	if(NULL == obj)
	{
		GSE_ERR("i2c data obj is null!!\n");
		return 0;
	}
	
	if(hwmsen_read_block(obj->client, i2c_dev_reg, databuf, 0x01))
	{
		GSE_ERR("read power ctl register err!\n");
		return KXTIK1013_ERR_I2C;
	}

	GSE_LOG("i2c_dev_reg=0x%2x  data=0x%2x \n", i2c_dev_reg,databuf[0]);

	return 0;
		
}


static DRIVER_ATTR(i2c,      S_IWUSR | S_IRUGO, show_register_value,         store_register_value);
static DRIVER_ATTR(register,      S_IWUSR | S_IRUGO, show_register,         store_register);


/*----------------------------------------------------------------------------*/
static struct driver_attribute *kxtik1013_attr_list[] = {
	&driver_attr_chipinfo,     /*chip information*/
	&driver_attr_sensordata,   /*dump sensor data*/
	&driver_attr_cali,         /*show calibration data*/
	&driver_attr_self,         /*self test demo*/
	&driver_attr_selftest,     /*self control: 0: disable, 1: enable*/
	&driver_attr_firlen,       /*filter length: 0: disable, others: enable*/
	&driver_attr_trace,        /*trace log*/
	&driver_attr_status,
	&driver_attr_powerstatus,
	&driver_attr_register,
	&driver_attr_i2c,
	&driver_attr_regvalue,
};
/*----------------------------------------------------------------------------*/
static int kxtik1013_create_attr(struct device_driver *driver) 
{
	int idx, err = 0;
	int num = (int)(sizeof(kxtik1013_attr_list)/sizeof(kxtik1013_attr_list[0]));
	if (driver == NULL)
	{
		return -EINVAL;
	}

	for(idx = 0; idx < num; idx++)
	{
		if((err = driver_create_file(driver, kxtik1013_attr_list[idx])))
		{            
			GSE_ERR("driver_create_file (%s) = %d\n", kxtik1013_attr_list[idx]->attr.name, err);
			break;
		}
	}    
	return err;
}
/*----------------------------------------------------------------------------*/
static int kxtik1013_delete_attr(struct device_driver *driver)
{
	int idx ,err = 0;
	int num = (int)(sizeof(kxtik1013_attr_list)/sizeof(kxtik1013_attr_list[0]));

	if(driver == NULL)
	{
		return -EINVAL;
	}
	

	for(idx = 0; idx < num; idx++)
	{
		driver_remove_file(driver, kxtik1013_attr_list[idx]);
	}
	

	return err;
}

/*----------------------------------------------------------------------------*/
int gsensor_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
	int err = 0;
	int value, sample_delay;	
	struct kxtik1013_i2c_data *priv = (struct kxtik1013_i2c_data*)self;
	hwm_sensor_data* gsensor_data;
	char buff[KXTIK1013_BUFSIZE];
	
	GSE_FUN();
	switch (command)
	{
		case SENSOR_DELAY:
			GSE_INFO("SENSOR_DELAY\n");
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				GSE_ERR("Set delay parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				value = *(int *)buff_in;
				if(value <= 5)
				{
					sample_delay = KXTIK1013_BW_200HZ;
				}
				else if(value <= 10)
				{
					sample_delay = KXTIK1013_BW_100HZ;
				}
				else
				{
					sample_delay = KXTIK1013_BW_50HZ;
				}
				
				err = KXTIK1013_SetBWRate(priv->client, sample_delay);
				if(err != KXTIK1013_SUCCESS ) //0x2C->BW=100Hz
				{
					GSE_ERR("Set delay parameter error!\n");
				}

				if(value >= 50)
				{
					atomic_set(&priv->filter, 0);
				}
				else
				{	
				#if defined(CONFIG_KXTIK1013_LOWPASS)
					priv->fir.num = 0;
					priv->fir.idx = 0;
					priv->fir.sum[KXTIK1013_AXIS_X] = 0;
					priv->fir.sum[KXTIK1013_AXIS_Y] = 0;
					priv->fir.sum[KXTIK1013_AXIS_Z] = 0;
					atomic_set(&priv->filter, 1);
				#endif
				}
			}
			break;

		case SENSOR_ENABLE:
			GSE_INFO("SENSOR_ENABLE\n");
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				GSE_ERR("Enable sensor parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				value = *(int *)buff_in;
				if(((value == 0) && (sensor_power == false)) ||((value == 1) && (sensor_power == true)))
				{
					GSE_LOG("Gsensor device have updated!\n");
				}
				else
				{
                /* Gionee BSP1 chengx 20140828 modify for xxxx begin */
                #if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
                    if (!priv->motion_detect_enable) {
                        mutex_lock(&kx1013_mutex);
					    err = KXTIK1013_SetPowerMode( priv->client, !sensor_power);
                        mutex_unlock(&kx1013_mutex);
                    }
                #else
					err = KXTIK1013_SetPowerMode( priv->client, !sensor_power);
                #endif
                /* Gionee BSP1 chengx 20140828 modify for xxxx end */
				}
			}
			break;

		case SENSOR_GET_DATA:
			GSE_INFO("SENSOR_GET_DATA\n");
			if((buff_out == NULL) || (size_out< sizeof(hwm_sensor_data)))
			{
				GSE_ERR("get sensor data parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				gsensor_data = (hwm_sensor_data *)buff_out;
				KXTIK1013_ReadSensorData(priv->client, buff, KXTIK1013_BUFSIZE);
				sscanf(buff, "%x %x %x", &gsensor_data->values[0], 
					&gsensor_data->values[1], &gsensor_data->values[2]);				
				gsensor_data->status = SENSOR_STATUS_ACCURACY_MEDIUM;				
				gsensor_data->value_divide = 1000;
			}
			break;
		default:
			GSE_ERR("gsensor operate function no this parameter %d!\n", command);
			err = -1;
			break;
	}
	
	return err;
}

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
int motion_sensor_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
	int err = 0;
	int value, sample_delay;	
	struct kxtik1013_i2c_data *priv = (struct kxtik1013_i2c_data*)self;
	hwm_sensor_data* motion_sensor_data;
	char buff[KXTIK1013_BUFSIZE];
    char data;
	GSE_FUN();

    hwm_sensor_data sensor_data;

	switch (command)
	{
		case SENSOR_DELAY:
			break;
		case SENSOR_ENABLE:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				GSE_ERR("Enable sensor parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				value = *(int *)buff_in;
                if (((value ==0) && (priv->motion_detect_enable == false)) || ((value == 1) && (priv->motion_detect_enable == true))) {
					GSE_ERR("motion sensor device have updated!\n");
                } else {
                    if (value == 1) {
                        priv->motion_detect_enable = true;    
                    } else {
                        priv->motion_detect_enable = false;    
                    }
                    
					hwmsen_read_byte(priv->client, KXTIK1013_INT_REL, &data);
                    kxtik1013_set_motion_enable(priv->client, priv->motion_detect_enable);
                    mt_eint_unmask(CUST_EINT_GSE_1_NUM);

                    
                    sensor_data.values[0] = 0;
                    sensor_data.value_divide = 1;
                    sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;

	                GSE_INFO("enable the motione sensor value = %d\n", value);
                    if (err = hwmsen_get_interrupt_data(ID_PICK_UP_GESTURE, &sensor_data)) {
                        GSE_ERR("%s call hwmsen_get_interrupt_data fail = %d\n", __func__, err);
                        return err;
                    }
                }
			}
			break;
		case SENSOR_GET_DATA:
			break;
		default:
			GSE_ERR("gsensor operate function no this parameter %d!\n", command);
			err = -1;
			break;
	}
	
	return err;
}
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

/****************************************************************************** 
 * Function Configuration
******************************************************************************/
static int kxtik1013_open(struct inode *inode, struct file *file)
{
	file->private_data = kxtik1013_i2c_client;

	if(file->private_data == NULL)
	{
		GSE_ERR("null pointer!!\n");
		return -EINVAL;
	}
	return nonseekable_open(inode, file);
}
/*----------------------------------------------------------------------------*/
static int kxtik1013_release(struct inode *inode, struct file *file)
{
	file->private_data = NULL;
	return 0;
}
/*----------------------------------------------------------------------------*/

#ifdef CONFIG_COMPAT
static long kxtik1013_compat_ioctl(struct file *file, unsigned int cmd,
       unsigned long arg)
{
    long err = 0;

	void __user *arg32 = compat_ptr(arg);
	
	if (!file->f_op || !file->f_op->unlocked_ioctl)
		return -ENOTTY;
	
    switch (cmd)
    {
        case COMPAT_GSENSOR_IOCTL_READ_SENSORDATA:
            if (arg32 == NULL)
            {
                err = -EINVAL;
                break;    
            }
		
		    err = file->f_op->unlocked_ioctl(file, GSENSOR_IOCTL_READ_SENSORDATA, (unsigned long)arg32);
		    if (err){
		        GSE_ERR("GSENSOR_IOCTL_READ_SENSORDATA unlocked_ioctl failed.");
		        return err;
		    }
        break;
        case COMPAT_GSENSOR_IOCTL_SET_CALI:
            if (arg32 == NULL)
            {
                err = -EINVAL;
                break;    
            }
		
		    err = file->f_op->unlocked_ioctl(file, GSENSOR_IOCTL_SET_CALI, (unsigned long)arg32);
		    if (err){
		        GSE_ERR("GSENSOR_IOCTL_SET_CALI unlocked_ioctl failed.");
		        return err;
		    }
        break;
        case COMPAT_GSENSOR_IOCTL_GET_CALI:
            if (arg32 == NULL)
            {
                err = -EINVAL;
                break;    
            }
		
		    err = file->f_op->unlocked_ioctl(file, GSENSOR_IOCTL_GET_CALI, (unsigned long)arg32);
		    if (err){
		        GSE_ERR("GSENSOR_IOCTL_GET_CALI unlocked_ioctl failed.");
		        return err;
		    }
        break;
        case COMPAT_GSENSOR_IOCTL_CLR_CALI:
            if (arg32 == NULL)
            {
                err = -EINVAL;
                break;    
            }
		
		    err = file->f_op->unlocked_ioctl(file, GSENSOR_IOCTL_CLR_CALI, (unsigned long)arg32);
		    if (err){
		        GSE_ERR("GSENSOR_IOCTL_CLR_CALI unlocked_ioctl failed.");
		        return err;
		    }
        break;

        default:
            GSE_ERR("unknown IOCTL: 0x%08x\n", cmd);
            err = -ENOIOCTLCMD;
        break;

    }

    return err;
}
#endif

static long kxtik1013_unlocked_ioctl(struct file *file, unsigned int cmd,unsigned long arg)
{
	struct i2c_client *client = (struct i2c_client*)file->private_data;
	struct kxtik1013_i2c_data *obj = (struct kxtik1013_i2c_data*)i2c_get_clientdata(client);	
	char strbuf[KXTIK1013_BUFSIZE];
	void __user *data;
	SENSOR_DATA sensor_data;
	long err = 0;
	int cali[3];

	GSE_FUN();
	if(_IOC_DIR(cmd) & _IOC_READ)
	{
		err = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
	}
	else if(_IOC_DIR(cmd) & _IOC_WRITE)
	{
		err = !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));
	}

	if(err)
	{
		GSE_ERR("access error: %08X, (%2d, %2d)\n", cmd, _IOC_DIR(cmd), _IOC_SIZE(cmd));
		return -EFAULT;
	}

	switch(cmd)
	{
		case GSENSOR_IOCTL_INIT:
			kxtik1013_init_client(client, 0);			
			break;

		case GSENSOR_IOCTL_READ_CHIPINFO:
			data = (void __user *) arg;
			if(data == NULL)
			{
				err = -EINVAL;
				break;	  
			}
			
			KXTIK1013_ReadChipInfo(client, strbuf, KXTIK1013_BUFSIZE);
			if(copy_to_user(data, strbuf, strlen(strbuf)+1))
			{
				err = -EFAULT;
				break;
			}				 
			break;	  

		case GSENSOR_IOCTL_READ_SENSORDATA:
			data = (void __user *) arg;
			if(data == NULL)
			{
				err = -EINVAL;
				break;	  
			}
			
			KXTIK1013_ReadSensorData(client, strbuf, KXTIK1013_BUFSIZE);
			if(copy_to_user(data, strbuf, strlen(strbuf)+1))
			{
				err = -EFAULT;
				break;	  
			}				 
			break;

		case GSENSOR_IOCTL_READ_GAIN:
			data = (void __user *) arg;
			if(data == NULL)
			{
				err = -EINVAL;
				break;	  
			}			
			
			if(copy_to_user(data, &gsensor_gain, sizeof(GSENSOR_VECTOR3D)))
			{
				err = -EFAULT;
				break;
			}				 
			break;

		case GSENSOR_IOCTL_READ_RAW_DATA:
			data = (void __user *) arg;
			if(data == NULL)
			{
				err = -EINVAL;
				break;	  
			}
			KXTIK1013_ReadRawData(client, strbuf);
			if(copy_to_user(data, &strbuf, strlen(strbuf)+1))
			{
				err = -EFAULT;
				break;	  
			}
			break;	  

		case GSENSOR_IOCTL_SET_CALI:
			data = (void __user*)arg;
			if(data == NULL)
			{
				err = -EINVAL;
				break;	  
			}
			if(copy_from_user(&sensor_data, data, sizeof(sensor_data)))
			{
				err = -EFAULT;
				break;	  
			}
			if(atomic_read(&obj->suspend))
			{
				GSE_ERR("Perform calibration in suspend state!!\n");
				err = -EINVAL;
			}
			else
			{
				cali[KXTIK1013_AXIS_X] = sensor_data.x * obj->reso->sensitivity / GRAVITY_EARTH_1000;
				cali[KXTIK1013_AXIS_Y] = sensor_data.y * obj->reso->sensitivity / GRAVITY_EARTH_1000;
				cali[KXTIK1013_AXIS_Z] = sensor_data.z * obj->reso->sensitivity / GRAVITY_EARTH_1000;			  
				err = KXTIK1013_WriteCalibration(client, cali);			 
			}
			break;

		case GSENSOR_IOCTL_CLR_CALI:
			err = KXTIK1013_ResetCalibration(client);
			break;

		case GSENSOR_IOCTL_GET_CALI:
			data = (void __user*)arg;
			if(data == NULL)
			{
				err = -EINVAL;
				break;	  
			}
			if((err = KXTIK1013_ReadCalibration(client, cali)))
			{
				break;
			}
			
			sensor_data.x = cali[KXTIK1013_AXIS_X] * GRAVITY_EARTH_1000 / obj->reso->sensitivity;
			sensor_data.y = cali[KXTIK1013_AXIS_Y] * GRAVITY_EARTH_1000 / obj->reso->sensitivity;
			sensor_data.z = cali[KXTIK1013_AXIS_Z] * GRAVITY_EARTH_1000 / obj->reso->sensitivity;
			if(copy_to_user(data, &sensor_data, sizeof(sensor_data)))
			{
				err = -EFAULT;
				break;
			}		
			break;
		

		default:
			GSE_ERR("unknown IOCTL: 0x%08x\n", cmd);
			err = -ENOIOCTLCMD;
			break;
			
	}

	return err;
}


/*----------------------------------------------------------------------------*/
static struct file_operations kxtik1013_fops = {
	.owner = THIS_MODULE,
	.open = kxtik1013_open,
	.release = kxtik1013_release,
	.unlocked_ioctl = kxtik1013_unlocked_ioctl,
	#ifdef CONFIG_COMPAT
	.compat_ioctl = kxtik1013_compat_ioctl,
	#endif
	//.ioctl = kxtik1013_ioctl,
};
/*----------------------------------------------------------------------------*/
static struct miscdevice kxtik1013_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "gsensor",
	.fops = &kxtik1013_fops,
};
/*----------------------------------------------------------------------------*/
#ifndef CONFIG_HAS_EARLYSUSPEND
static int kxtik1013_suspend(struct i2c_client *client, pm_message_t msg) 
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);    
	int err = 0;
	GSE_FUN();    

	if(msg.event == PM_EVENT_SUSPEND)
	{   
		if(obj == NULL)
		{
			GSE_ERR("null pointer!!\n");
			return -EINVAL;
		}
		atomic_set(&obj->suspend, 1);
		if(err = KXTIK1013_SetPowerMode(obj->client, false))
		{
			GSE_ERR("write power control fail!!\n");
			return;
		}

		sensor_power = false;      
		KXTIK1013_power(obj->hw, 0);
	}
	return err;
}

static int kxtik1013_resume(struct i2c_client *client)
{
	struct kxtik1013_i2c_data *obj = i2c_get_clientdata(client);        
	int err;
	GSE_FUN();

	if(obj == NULL)
	{
		GSE_ERR("null pointer!!\n");
		return -EINVAL;
	}

	KXTIK1013_power(obj->hw, 1);
	if(err = kxtik1013_init_client(client, 0))
	{
		GSE_ERR("initialize client fail!!\n");
		return err;        
	}
	atomic_set(&obj->suspend, 0);

	return 0;
}

#else /*CONFIG_HAS_EARLY_SUSPEND is defined*/

static void kxtik1013_early_suspend(struct early_suspend *h) 
{
	struct kxtik1013_i2c_data *obj = container_of(h, struct kxtik1013_i2c_data, early_drv);   
	int err;
    u8 data;
	GSE_FUN();    

	if(obj == NULL)
	{
		GSE_ERR("null pointer!!\n");
		return;
	}
	atomic_set(&obj->suspend, 1);

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
	hwmsen_read_byte(obj->client, KXTIK1013_INT_REL, &data);
    if (obj->motion_detect_enable)
    {
        kxtik1013_set_motion_enable(obj->client, obj->motion_detect_enable);
        mt_eint_unmask(CUST_EINT_GSE_1_NUM);
	    GSE_INFO("enable the motion detect func\n");
    }
    else
    {
        kxtik1013_set_motion_enable(obj->client, obj->motion_detect_enable);

        if(err = KXTIK1013_SetPowerMode(obj->client, false)) {
            GSE_ERR("write power control fail!!\n");
            return;
        }
        sensor_power = false;
        KXTIK1013_power(obj->hw, 0);
    }

#else
	if((err = KXTIK1013_SetPowerMode(obj->client, false)))
	{
		GSE_ERR("write power control fail!!\n");
		return;
	}

	sensor_power = false;
	
	KXTIK1013_power(obj->hw, 0);
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */
}

static void kxtik1013_late_resume(struct early_suspend *h)
{
	struct kxtik1013_i2c_data *obj = container_of(h, struct kxtik1013_i2c_data, early_drv);         
	int err;
	GSE_FUN();

	if(obj == NULL)
	{
		GSE_ERR("null pointer!!\n");
		return;
	}

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
   mt_eint_mask(CUST_EINT_GSE_1_NUM);
   kxtik1013_set_motion_enable(obj->client, false);
#endif

	KXTIK1013_power(obj->hw, 1);
	if((err = kxtik1013_init_client(obj->client, 0)))
	{
		GSE_ERR("initialize client fail!!\n");
		return;        
	}

/* Gionee BSP1 chengx 20140828 modify for xxxx end */

	atomic_set(&obj->suspend, 0);    
}
#endif /*CONFIG_HAS_EARLYSUSPEND*/

/*----------------------------------------------------------------------------*/
static int kxtik1013_i2c_detect(struct i2c_client *client, struct i2c_board_info *info) 
{    
	strcpy(info->type, KXTIK1013_DEV_NAME);
	return 0;
}

/*----------------------------------------------------------------------------*/
static int kxtik1013_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct i2c_client *new_client;
	struct kxtik1013_i2c_data *obj;
	struct hwmsen_object sobj;
/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
    struct hwmsen_object motionobj;
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */
	int err = 0;
	GSE_FUN();

/* Gionee BSP1 chengx 20140826 modify for CR01371160 begin */
#ifdef GN_MTK_BSP_DEVICECHECK
	struct gn_device_info gn_dev_info_gsensor = {0};
	gn_dev_info_gsensor.gn_dev_type = GN_DEVICE_TYPE_ACCELEROMETER;
	strcpy(gn_dev_info_gsensor.name, KXTIK1013_DEV_NAME);
	gn_set_device_info(gn_dev_info_gsensor);
#endif
/* Gionee BSP1 chengx 20140826 modify for CR01371160 end */

	if(!(obj = kzalloc(sizeof(*obj), GFP_KERNEL))) {
		GSE_ERR("Gsensor : kzalloc failed.\n");
		err = -ENOMEM;
		goto exit;
	}

	obj->hw = get_cust_acc_hw_kxtik();
	if(obj->hw) {
		GSE_LOG("Gsensor : get_cust_acc_hw_kxtik() OK.\n");
    }
	else {
		GSE_ERR("Gsensor : get_cust_acc_hw_kxtik() failed.\n");
    }
	if((err = hwmsen_get_convert(obj->hw->direction, &obj->cvt))) {
		GSE_ERR("invalid direction: %d\n", obj->hw->direction);
		goto exit;
	}

	obj_i2c_data = obj;
	obj->client = client;
	new_client = obj->client;
	i2c_set_clientdata(new_client,obj);
	atomic_set(&obj->trace, 0);
	atomic_set(&obj->suspend, 0);
	
#ifdef CONFIG_KXTIK1013_LOWPASS
	if(obj->hw->firlen > C_MAX_FIR_LENGTH) {
		atomic_set(&obj->firlen, C_MAX_FIR_LENGTH);
	}	
	else {
		atomic_set(&obj->firlen, obj->hw->firlen);
	}
	
	if(atomic_read(&obj->firlen) > 0) {
		atomic_set(&obj->fir_en, 1);
	}
	
#endif

	kxtik1013_i2c_client = new_client;

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
    INIT_WORK(&obj->kxtik1013_eint_work, kxtik1013_eint_work_func);
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

	if(err = kxtik1013_init_client(new_client, 1)) {
		GSE_ERR("Gsensor : init_clientdata failed.\n");
		goto exit_init_failed;
	}

	if(err = misc_register(&kxtik1013_device)) {
		GSE_ERR("kxtik1013_device register failed\n");
		goto exit_misc_device_register_failed;
	}

#ifdef MTK_AUTO_DETECT_ACCELEROMETER
	if(err = kxtik1013_create_attr(&(kxtik1013_init_info.platform_diver_addr->driver))) {
		GSE_ERR("create attribute err = %d\n", err);
		goto exit_create_attr_failed;
	}
#else
	if(err = kxtik1013_create_attr(&kxtik1013_gsensor_driver.driver))
	{
		GSE_ERR("create attribute err = %d\n", err);
		goto exit_create_attr_failed;
	}
#endif

	sobj.self = obj;
    sobj.polling = 1;
    sobj.sensor_operate = gsensor_operate;
	if(err = hwmsen_attach(ID_ACCELEROMETER, &sobj))
	{
		GSE_ERR("attach fail = %d\n", err);
		goto exit_attach_failed;
	}

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
	motionobj.self = obj;
    motionobj.polling = 0;
    motionobj.sensor_operate = motion_sensor_operate;
	if(err = hwmsen_attach(ID_PICK_UP_GESTURE, &motionobj))
	{
		GSE_ERR("attach fail = %d\n", err);
		goto exit_attach_failed;
	}
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

#ifdef CONFIG_HAS_EARLYSUSPEND
	obj->early_drv.level    = EARLY_SUSPEND_LEVEL_DISABLE_FB - 1,
	obj->early_drv.suspend  = kxtik1013_early_suspend,
	obj->early_drv.resume   = kxtik1013_late_resume,    
	register_early_suspend(&obj->early_drv);
#endif

/* Gionee BSP1 chengx 20140828 modify for xxxx begin */
#if defined(KXTIK1013_MOTION_SENSOR_FUNCTION)
    kxtik1013_setup_eint(client);

	err = KXTIK1013_SetMotion(client, KXTIK1013_MOTION);
	if(err != KXTIK1013_SUCCESS) //0x2C->BW=100Hz
	{
		GSE_ERR("Gsensor : set data format failed.\n");
		return err;
	}

    obj->motion_detect_enable = false;
#endif
/* Gionee BSP1 chengx 20140828 modify for xxxx end */

	GSE_INFO("%s: OK\n", __func__);   
	kxtik1013_init_flag = 0; 
	return 0;

exit_attach_failed:
#ifdef MTK_AUTO_DETECT_ACCELEROMETER
	kxtik1013_delete_attr(&(kxtik1013_init_info.platform_diver_addr->driver));
#else
	kxtik1013_delete_attr(&kxtik1013_gsensor_driver.driver);
#endif
exit_create_attr_failed:
	misc_deregister(&kxtik1013_device);
exit_misc_device_register_failed:
exit_init_failed:
	//i2c_detach_client(new_client);
exit_kfree:
	kfree(obj);
exit:
	GSE_ERR("%s: err = %d\n", __func__, err);     
	kxtik1013_init_flag = -1;   
	return err;
}

/*----------------------------------------------------------------------------*/
static int kxtik1013_i2c_remove(struct i2c_client *client)
{
	int err = 0;	
	
#ifdef MTK_AUTO_DETECT_ACCELEROMETER
	if (err = kxtik1013_delete_attr(&(kxtik1013_init_info.platform_diver_addr->driver))) {
		GSE_ERR("kxtik_delete_attr fail: %d\n", err);
	}
#else
	if(err = kxtik1013_delete_attr(&kxtik1013_gsensor_driver.driver))
	{
		GSE_ERR("kxtik1013_delete_attr fail: %d\n", err);
	}
#endif
	
	if(err = misc_deregister(&kxtik1013_device))
	{
		GSE_ERR("misc_deregister fail: %d\n", err);
	}
	    
	kxtik1013_i2c_client = NULL;
	i2c_unregister_device(client);
	kfree(i2c_get_clientdata(client));
	return 0;
}

/*----------------------------------------------------------------------------*/
#ifdef MTK_AUTO_DETECT_ACCELEROMETER 
static int kxtik1013_local_init(void)
{
	struct acc_hw *hw = get_cust_acc_hw_kxtik();

	GSE_FUN();
	KXTIK1013_power(hw, 1);
	if (i2c_add_driver(&kxtik1013_i2c_driver)) {
		GSE_ERR("add driver error\n");
		return -1;
	}
	if (-1 == kxtik1013_init_flag) {
		return -1;
	}
	return 0;
}
#else
static int kxtik1013_probe(struct platform_device *pdev) 
{
	struct acc_hw *hw = get_cust_acc_hw_kxtik();
	GSE_FUN();

	KXTIK1013_power(hw, 1);
	if(i2c_add_driver(&kxtik1013_i2c_driver))
	{
		GSE_ERR("add driver error\n");
		return -1;
	}
	return 0;
}
#endif

/*----------------------------------------------------------------------------*/
static int kxtik1013_remove(void)
{
    struct acc_hw *hw = get_cust_acc_hw_kxtik();

    GSE_FUN();    
    KXTIK1013_power(hw, 0);    
    i2c_del_driver(&kxtik1013_i2c_driver);
    return 0;
}

/*----------------------------------------------------------------------------*/
static int __init kxtik1013_init(void)
{
	GSE_FUN();	
    //Gionee yang_yang CR01324878 begin
	i2c_register_board_info(2, &i2c_kxtik1013, 1);
    //Gionee yang_yang CR01324878 end
#ifdef MTK_AUTO_DETECT_ACCELEROMETER
    if (hwmsen_gsensor_add(&kxtik1013_init_info))
	{
		GSE_ERR("failed to add gsensor!");
		return -ENODEV;
	}
#else
	if(platform_driver_register(&kxtik1013_gsensor_driver))
	{
		GSE_ERR("failed to register driver");
		return -ENODEV;
	}
#endif
	return 0;    
}
/*----------------------------------------------------------------------------*/
static void __exit kxtik1013_exit(void)
{
	GSE_FUN();
#ifdef MTK_AUTO_DETECT_ACCELEROMETER
    hwmsen_gsensor_del(&kxtik1013_init_info);
#else
	platform_driver_unregister(&kxtik1013_gsensor_driver);
#endif
}
/*----------------------------------------------------------------------------*/
module_init(kxtik1013_init);
module_exit(kxtik1013_exit);
/*----------------------------------------------------------------------------*/
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("KXTIK1013 I2C driver");
MODULE_AUTHOR("Dexiang.Liu@mediatek.com");
