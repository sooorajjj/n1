#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <utils/Log.h>
#include <stdbool.h>
#include <jni.h>
#include <JNIHelp.h>
#include "libhwm.h"
#define LOG_TAG "gsensor"
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

JNIEXPORT jint
JNICALL Java_com_qualcomm_sensors_sensortest_SensorUserCal_performUserCal
  ( JNIEnv *env, jclass class, jbyte sensor_type, jbyte data_type )
{
	LOGE( "%s: sensor_type = %d, data_type = %d", __FUNCTION__, sensor_type,  data_type);

	LOGE( "%s: sensor_type = %d, data_type = %d", __FUNCTION__, sensor_type,  data_type);
	int gs_fd = -1;
	int err = 0;
	HwmData cali;
	
	err = gsensor_open(&gs_fd);
	if(err < 0)
	{
		LOGE("gsensor_open fail in do_gsensor_calibration");
		return -1;
	}

	if((err = gsensor_get_cali(gs_fd, &cali)) != 0)
	{	 
		LOGE("get calibration fail: (%s) %d\n", strerror(errno), err);
		return -1;
	}


	err = gsensor_close(&gs_fd);
	if(err < 0)
	{
		LOGE("gsensor_close fail in do_gsensor_calibration");
		return -1;
	}
	return 0;

		
}


/*
 * Array of methods.
 *
 * Each entry has three fields: the name of the method, the method
 * signature, and a pointer to the native implementation.
 */
static const JNINativeMethod gMethods[] = {
  { "performUserCal",
    "(BB)I",
    (void*)Java_com_qualcomm_sensors_sensortest_SensorUserCal_performUserCal
  }
};


/*
 * Explicitly register all methods for our class.
 *
 * @return 0 on success.
 */
static int
registerMethods(JNIEnv* env)
{
  static const char* const kClassName =
      "gn/com/android/mmitest/item/SensorUserCal";
  jclass class;

  /* look up the class */
  class = (*env)->FindClass( env, kClassName );
  if(class == NULL)
  {
    LOGE( "%s: Can't find class %s", __FUNCTION__, kClassName );
    return -1;
  }

  /* register all the methods */
  if( (*env)->RegisterNatives( env, class, gMethods,
      sizeof(gMethods) / sizeof(gMethods[0]) ) != JNI_OK )
  {
    LOGE( "%s: Failed registering methods for %s",
          __FUNCTION__, kClassName );
    return -1;
  }
  LOGE( "%s: success", __FUNCTION__ );

  return 0;
}


/*
 * This is called by the VM when the shared library is first loaded.
 */
jint
JNI_OnLoad( JavaVM* vm, void* reserved )
{
   JNIEnv* env = NULL;
   if( (*vm)->GetEnv( vm, (void**) &env, JNI_VERSION_1_4 ) != JNI_OK )
   {
      LOGE( "%s: GetEnv failed", __FUNCTION__ );
      return -1;
   }

   if( registerMethods( env ) != 0 )
   {
      LOGE( "%s: native registration failed", __FUNCTION__ );
      return -1;
   }
   return JNI_VERSION_1_4;
}

