
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_PRELINK_MODULE := false
//LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_SHARED_LIBRARIES := libutils liblog libhwm
LOCAL_SRC_FILES := jni/sensor_user_cal_gn.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/jni/inc\
		    $(JNI_H_INCLUDE)

LOCAL_MODULE    := libsensor_user_cal_gn
LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)

