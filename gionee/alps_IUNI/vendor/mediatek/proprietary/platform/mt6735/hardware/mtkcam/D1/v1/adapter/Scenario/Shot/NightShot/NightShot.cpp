/*
  * Description:
  *     Create shot mode for night shot.
  * 
  * Author : Wutangzhi
  * Date    : 2015/05/04
  * Email   : wutz@gionee.com
  * CR       : [CR01474543] �Ż�ҹ����������
  */

#define LOG_TAG "MtkCam/NightShot"
//
#include <mtkcam/Log.h>
#include <mtkcam/common.h>
//
#include <mtkcam/hwutils/CameraProfile.h>
using namespace CPTool;
//
#include <mtkcam/featureio/IHal3A.h>
using namespace NS3A;
//
#include <mtkcam/hal/IHalSensor.h>
//
#include <mtkcam/camshot/ICamShot.h>
#include <mtkcam/camshot/IMultiShot.h>
//
#include <mtkcam/exif/IBaseCamExif.h>
//
#include <Shot/IShot.h>
//
#include "ImpShot.h"
#include "NightShot.h"
//
using namespace android;
using namespace NSShot;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


#define MAX_JPEG_QUALITY  (92)
#define NIGHT_SHOT_CAP_CNT (6)

/******************************************************************************
 *
 ******************************************************************************/
extern "C"
sp<IShot>
createInstance_NightShot(
    char const*const    pszShotName, 
    uint32_t const      u4ShotMode, 
    int32_t const       i4OpenId
)
{
    sp<IShot>       pShot = NULL;
    sp<NightShot>  pImpShot = NULL;
    //
    //  (1.1) new Implementator.
    pImpShot = new NightShot(pszShotName, u4ShotMode, i4OpenId);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new ContinuousShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (1.2) initialize Implementator if needed.
    if  ( ! pImpShot->onCreate() ) {
        CAM_LOGE("[%s] onCreate()", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot->onDestroy();
        pImpShot = NULL;
    }
    //
    return  pShot;
}


/******************************************************************************
 *  This function is invoked when this object is firstly created.
 *  All resources can be allocated here.
 ******************************************************************************/
bool
NightShot::
onCreate()
{
#warning "[TODO] NightShot::onCreate()"
    bool ret = true;
    return ret;
}


/******************************************************************************
 *  This function is invoked when this object is ready to destryoed in the
 *  destructor. All resources must be released before this returns.
 ******************************************************************************/
void
NightShot::
onDestroy()
{
#warning "[TODO] NightShot::onDestroy()"
}


/******************************************************************************
 *
 ******************************************************************************/
NightShot::
NightShot(
    char const*const pszShotName, 
    uint32_t const u4ShotMode, 
    int32_t const i4OpenId
)
    : ImpShot(pszShotName, u4ShotMode, i4OpenId)
    , mpMultiShot(NULL) // [CS]+
    , mu4ShotConut(0)
    , mbLastImage(false)
    , mbShotStoped(false)
    , mShotStopMtx()
    , semMShotEnd()
    , mu4GroupId(0)
    , mbCbShutterMsg(true)
    , mpuExifHeaderBuf(NULL)
{
}


/******************************************************************************
 *
 ******************************************************************************/
NightShot::
~NightShot()
{
    if ( mpuExifHeaderBuf != NULL )
        delete [] mpuExifHeaderBuf;
    //if ( mpCaptureBufMgr != NULL )
    //    mpCaptureBufMgr = NULL;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
NightShot::
sendCommand(
    uint32_t const  cmd, 
    MUINTPTR const  arg1, 
    uint32_t const  arg2,
    uint32_t const  arg3
)
{
    AutoCPTLog cptlog(Event_CShot_sendCmd, cmd, arg1);

    bool ret = true;
    //
    switch  (cmd)
    {
    //  This command is to reset this class. After captures and then reset, 
    //  performing a new capture should work well, no matter whether previous 
    //  captures failed or not.
    //
    //  Arguments:
    //          N/A
    case eCmd_reset:
        ret = onCmd_reset();
        break;

    //  This command is to perform capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_capture:
        ret = onCmd_capture();
        break;

    //  This command is to perform cancel capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_cancel:
        onCmd_cancel();
        break;
    //  This command is to perform set continuous shot speed.
    //
    //  Arguments:
    //          N/A
    case eCmd_setCShotSpeed:
        
        break;
    //
    default:
        ret = ImpShot::sendCommand(cmd, arg1, arg2, arg3);
    }
    //
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
NightShot::
onCmd_reset()
{
#warning "[TODO] ContinuousShot::onCmd_reset()"
    bool ret = true;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
NightShot::
onCmd_capture()
{ 
    
    bool ret = true;
	
    mbCbShutterMsg = true;
	mpIMemDrv =  IMemDrv::createInstance();
    if (mpIMemDrv == NULL)
    {
        MY_LOGE("g_pIMemDrv is NULL \n");
        return 0;
    }

    mpIImageBufAllocator =  IImageBufferAllocator::getInstance();
    if (mpIImageBufAllocator == NULL)
    {
        MY_LOGE("mpIImageBufAllocator is NULL \n");
        return 0;
    }
	
	requestBufs();

    AutoCPTLog cptlog(Event_CShot_capture);
    MY_LOGD("+ "); 

    {
        Mutex::Autolock lock(mShotStopMtx);

        if(mbShotStoped)
        {
            return ret;
        }
        //
        mpMultiShot = NSCamShot::IMultiShot::createInstance(static_cast<EShotMode>(mu4ShotMode), "NightShot");
        //
        MUINT32 nrtype = 0;
        {
            IHal3A* p3AHal = IHal3A::createInstance(IHal3A::E_Camera_1,
                    getOpenId(),
                    LOG_TAG);
            if( p3AHal ) {
                nrtype = queryCapNRType( p3AHal->isNeedFiringFlash() ? getCaptureIso() : getPreviewIso() );
                p3AHal->destroyInstance(LOG_TAG); 
            }
        }
        // 
        mpMultiShot->init(); 
        // 
        mpMultiShot->enableNotifyMsg(NSCamShot::ECamShot_NOTIFY_MSG_EOF      |
                                     NSCamShot::ECamShot_NOTIFY_MSG_SHOTS_END );
        //
        EImageFormat ePostViewFmt = static_cast<EImageFormat>(mShotParam.miPostviewDisplayFormat);
        //
		MUINT32 datamsg;
		datamsg =  NSCamShot::ECamShot_DATA_MSG_YUV;
        mpMultiShot->enableDataMsg(datamsg); 

		if(getOpenId() == 1 && mShotParam.mu4IsFlip == true){
		  switch(mShotParam.mu4Transform){
		   case eTransform_ROT_90:
			mShotParam.mu4Transform = eTransform_FLIP_V | eTransform_ROT_90; 
			break;
			
		   case eTransform_ROT_270:
			mShotParam.mu4Transform = eTransform_FLIP_H | eTransform_ROT_90; 
			break; 
		   case eTransform_ROT_180:
			mShotParam.mu4Transform = eTransform_FLIP_V; 
			break;
			
		   default:
			mShotParam.mu4Transform = eTransform_FLIP_H; 
			break;
		  }
		}

        // shot param 
        NSCamShot::ShotParam rShotParam(
                eImgFmt_YUY2,                    //yuv format 
                mShotParam.mi4PictureWidth,      //picutre width 
                mShotParam.mi4PictureHeight,     //picture height
                mShotParam.mu4Transform,         //picture transform 
                ePostViewFmt,                    //postview format 
                mShotParam.mi4PostviewWidth,     //postview width 
                mShotParam.mi4PostviewHeight,    //postview height 
                0,                               //postview transform
                mShotParam.mu4ZoomRatio          //zoom   
                );                                  

        if( mJpegParam.mu4JpegQuality > MAX_JPEG_QUALITY ) {
                MY_LOGW("limit jpeg quality to %d", MAX_JPEG_QUALITY);
                mJpegParam.mu4JpegQuality = MAX_JPEG_QUALITY;
        }

        // jpeg param 
        NSCamShot::JpegParam rJpegParam(
                NSCamShot::ThumbnailParam(mJpegParam.mi4JpegThumbWidth,
                    mJpegParam.mi4JpegThumbHeight, 
                    mJpegParam.mu4JpegThumbQuality,
                    MTRUE),
                mJpegParam.mu4JpegQuality,       //Quality 
                MFALSE                           //isSOI 
                ); 
     
                                                                         
        // sensor param 
        NSCamShot::SensorParam rSensorParam(
            getOpenId(),                             //sensor idx
            SENSOR_SCENARIO_ID_NORMAL_CAPTURE,       //Scenaio 
            10,                                      //bit depth 
            MFALSE,                                  //bypass delay 
            MFALSE                                   //bypass scenario 
            );  
        //
        mpMultiShot->setCallbacks(fgCamShotNotifyCb, fgCamShotDataCb, this); 
        //     
        ret = mpMultiShot->setShotParam(rShotParam); 
        //
        ret = mpMultiShot->setJpegParam(rJpegParam); 
        //
        ret = mpMultiShot->setPrvBufHdl((MVOID*)mpPrvBufHandler);

        if( mpCapBufMgr != NULL ) {
            ret = mpMultiShot->setCapBufMgr((MVOID*)mpCapBufMgr);
        } else {
            MY_LOGE("mpCapBufMgr == NULL");
            ret = MFALSE;
        }
        //
        ret = mpMultiShot->sendCommand( NSCamShot::ECamShot_CMD_SET_NRTYPE, nrtype, 0, 0 );
        //
        mu4ShotConut = 0;
        ::sem_init(&semMShotEnd, 0, 0);
        // 
        ret = ret & mpMultiShot->start(rSensorParam, NIGHT_SHOT_CAP_CNT); 
    }

    ::sem_wait(&semMShotEnd); 

    {
        Mutex::Autolock lock(mShotStopMtx);
        if(!mbShotStoped)
        {
            mpMultiShot->stop();
            mbShotStoped = true;
        }
        //
        ret = mpMultiShot->uninit(); 
        //
        mpMultiShot->destroyInstance(); 
        //
        mpMultiShot = NULL;
    }

    //handle yuv callback, do 3rd algo 
    //handleYuvDataCallback(mpYuvSource,mpYuvSource->getBufSizeInBytes(0));
	//release   buffers
    releaseBufs();

    MY_LOGD("- "); 
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
void
NightShot::
onCmd_cancel()
{
    AutoCPTLog cptlog(Event_CShot_cancel);
    MY_LOGD("onCmd_cancel +)");
    
    Mutex::Autolock lock(mShotStopMtx);
    if(!mbShotStoped)
    {
        if(mpMultiShot != NULL)
        {
            MY_LOGD("real need stop MultiShot");
            mpMultiShot->stop();
        }
        else
        {
            MY_LOGD("MultiShot not created, only set mbShotStoped = true");
        }
        
        mbShotStoped = true;
    }

	::sem_post(&semMShotEnd);
    
    MY_LOGD("onCmd_cancel -)");
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL 
NightShot::
fgCamShotNotifyCb(MVOID* user, NSCamShot::CamShotNotifyInfo const msg)
{
    NightShot *pNightShot = reinterpret_cast <NightShot *>(user); 
    if (NULL != pNightShot) {
        if (NSCamShot::ECamShot_NOTIFY_MSG_EOF == msg.msgType) {
			pNightShot->mpShotCallback->onCB_Shutter(true, 0); 
		}
    }

    return MTRUE; 
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
NightShot::
fgCamShotDataCb(MVOID* user, NSCamShot::CamShotDataInfo const msg)
{
    NightShot *pNightShot = reinterpret_cast<NightShot *>(user); 
    if (NULL != pNightShot) {
        if (NSCamShot::ECamShot_DATA_MSG_POSTVIEW == msg.msgType) {
            //pContinuousShot->handlePostViewData( msg.puData, msg.u4Size);  
        } else if (NSCamShot::ECamShot_DATA_MSG_JPEG == msg.msgType) {
            //TODO
        } else if (NSCamShot::ECamShot_DATA_MSG_YUV == msg.msgType) {
			pNightShot->handleYuvDataCallback((IImageBuffer*)msg.pBuffer, msg.pBuffer->getBufSizeInBytes(0));  
        }
    }

    return MTRUE; 
}


/******************************************************************************
*
*******************************************************************************/
MBOOL
NightShot::
handlePostViewData(MUINT8* const puBuf, MUINT32 const u4Size)
{
    return  MTRUE;
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
NightShot::
handleJpegData(IImageBuffer* pJpeg, IImageBuffer* pThumb, IDbgInfoContainer* pDbg)
{
	AutoCPTLog cptlog(Event_Shot_handleJpegData);
    MUINT8* puJpegBuf = (MUINT8*)pJpeg->getBufVA(0);
    MUINT32 u4JpegSize = pJpeg->getBitstreamSize();
    MUINT8* puThumbBuf = NULL;
    MUINT32 u4ThumbSize = 0;
	
    if( pThumb != NULL ) {
        puThumbBuf = (MUINT8*)pThumb->getBufVA(0);
        u4ThumbSize = pThumb->getBitstreamSize();
    }

    MY_LOGD("+ (puJpgBuf, jpgSize, puThumbBuf, thumbSize, dbg) = (%p, %d, %p, %d, %p)",
            puJpegBuf, u4JpegSize, puThumbBuf, u4ThumbSize, pDbg); 

    MUINT8 *puExifHeaderBuf = new MUINT8[DBG_EXIF_SIZE]; 
    MUINT32 u4ExifHeaderSize = 0; 

    CPTLogStr(Event_Shot_handleJpegData, CPTFlagSeparator, "makeExifHeader");
    makeExifHeader(eAppMode_PhotoMode, puThumbBuf, u4ThumbSize, puExifHeaderBuf, u4ExifHeaderSize, pDbg); 
    MY_LOGD("(thumbbuf, size, exifHeaderBuf, size) = (%p, %d, %p, %d)", 
                      puThumbBuf, u4ThumbSize, puExifHeaderBuf, u4ExifHeaderSize);   

	// dummy raw callback 
    mpShotCallback->onCB_RawImage(0, 0, NULL);

    // Jpeg callback 
    CPTLogStr(Event_Shot_handleJpegData, CPTFlagSeparator, "onCB_CompressedImage");
    mpShotCallback->onCB_CompressedImage(0,
                                         u4JpegSize, 
                                         reinterpret_cast<uint8_t const*>(puJpegBuf),
                                         u4ExifHeaderSize,                       //header size 
                                         puExifHeaderBuf,                    	//header buf
                                         0,                       				//callback index 
                                         true                     				//final image 
                                         ); 

	::sem_post(&semMShotEnd); 
	
    MY_LOGD("-"); 

    delete [] puExifHeaderBuf; 

    return MTRUE; 

}

MBOOL
NightShot::
handleYuvDataCallback(IImageBuffer* puBuf, MUINT32 const u4Size)
{
    MY_LOGD("+ (puBuf, size) = (%p, %d)", puBuf, u4Size);
	
    MUINT8* puYuvBuf = NULL;

	puYuvBuf = (MUINT8*)puBuf->getBufVA(0);
    mpShotCallback->onCB_YUVData(0, 
                                 u4Size, 
                                 reinterpret_cast<uint8_t *>(puYuvBuf)
                                ); 

    mu4ShotConut++;
	if(mu4ShotConut == NIGHT_SHOT_CAP_CNT) {
		handleProcessedYuvData(puBuf);
	}

	MY_LOGD("-"); 
	return  MTRUE;
}

//
MBOOL
NightShot::
releaseBufs()
{
    MY_LOGD("[releaseBufs] - E.");
    deallocMem(mJpegBuf);
// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> begin
#if ORIGINAL_VERSION
#else
	if(NULL != mThumbBuf)
#endif
// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> end
    deallocMem(mThumbBuf);

    MY_LOGD("[releaseBufs] - X.");
    return  MTRUE;

}

void
NightShot::
deallocMem(IImageBuffer *pBuf)
{
	pBuf->unlockBuf(LOG_TAG);
	if (pBuf->getImgFormat() == eImgFmt_JPEG) {
		mpIImageBufAllocator->free(pBuf);
	} else {
		pBuf->decStrong(pBuf);
		for (Vector<ImageBufferMap>::iterator it = mvImgBufMap.begin(); it != mvImgBufMap.end(); it++) {
			if (it->pImgBuf == pBuf){
				mpIMemDrv->unmapPhyAddr(&it->memBuf);
				if (mpIMemDrv->freeVirtBuf(&it->memBuf)) {
					MY_LOGE("m_pIMemDrv->freeVirtBuf() error");
				} else {
					mvImgBufMap.erase(it);
				}
				
				break;
			}
		}
	}
}

MBOOL
NightShot::
requestBufs()
{
     	MY_LOGD("[requestBufs] - E.");
		MBOOL	ret = MTRUE;
		MBOOL bVertical = ( mShotParam.mu4Transform == NSCam::eTransform_ROT_90 
			              || mShotParam.mu4Transform == NSCam::eTransform_ROT_270 );

		mJpgWidth = bVertical? mShotParam.mi4PictureHeight: mShotParam.mi4PictureWidth;
		mJpgHeight = bVertical? mShotParam.mi4PictureWidth: mShotParam.mi4PictureHeight;

		mThumbWidth = bVertical? mJpegParam.mi4JpegThumbHeight: mJpegParam.mi4JpegThumbWidth;
		mThumbHeight = bVertical? mJpegParam.mi4JpegThumbWidth: mJpegParam.mi4JpegThumbHeight;	

		mYuvWidth = mShotParam.mi4PictureWidth;
		mYuvHeight = mShotParam.mi4PictureHeight ;
		
        mJpegBuf = allocMem(eImgFmt_JPEG, 
                                     mJpgWidth, 
                                     mJpgHeight);
// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> begin
#if ORIGINAL_VERSION
#else
		if(!mShotParam.mb4BuildInThumbnail)
			mThumbBuf = NULL;
		else
#endif
// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> end
        mThumbBuf = allocMem(eImgFmt_JPEG, 
                                     mThumbWidth, 
                                     mThumbHeight); 
		
        MY_LOGD("[requestBufs] mJpgWidth %d , mJpgHeight %d, JpegBuf(0x%x),ThumbnailBuf(0x%x)", 
			mJpgWidth, mJpgHeight, mJpegBuf,mThumbBuf);
		
		ret = MTRUE;
		MY_LOGD("[requestBufs] - X.");
		
		return	ret;
}

IImageBuffer*
NightShot::
allocMem(MUINT32 fmt, MUINT32 w, MUINT32 h)
{
    IImageBuffer* pBuf;

    if( fmt != eImgFmt_JPEG )
    {
        /* To avoid non-continuous multi-plane memory, allocate ION memory and map it to ImageBuffer */
        MUINT32 plane = NSCam::Utils::Format::queryPlaneCount(fmt);
        ImageBufferMap bufMap;

        bufMap.memBuf.size = 0;
        for (int i = 0; i < plane; i++)
        {
            bufMap.memBuf.size += (NSCam::Utils::Format::queryPlaneWidthInPixels(fmt,i, w) * NSCam::Utils::Format::queryPlaneBitsPerPixel(fmt,i) / 8) * NSCam::Utils::Format::queryPlaneHeightInPixels(fmt, i, h);
        }

        if (mpIMemDrv->allocVirtBuf(&bufMap.memBuf)) {
            MY_LOGE("g_pIMemDrv->allocVirtBuf() error \n");
            return NULL;
        }
		
        //memset((void*)bufMap.memBuf.virtAddr, 0 , bufMap.memBuf.size);
        if (mpIMemDrv->mapPhyAddr(&bufMap.memBuf)) {
            MY_LOGE("mpIMemDrv->mapPhyAddr() error \n");
            return NULL;
        }

        MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
        MUINT32 bufStridesInBytes[3] = {0};

        for (MUINT32 i = 0; i < plane; i++) { 
            bufStridesInBytes[i] = NSCam::Utils::Format::queryPlaneWidthInPixels(fmt,i, w) 
				* NSCam::Utils::Format::queryPlaneBitsPerPixel(fmt,i) / 8;
        }
		
        IImageBufferAllocator::ImgParam imgParam(
                fmt, 
                MSize(w,h), 
                bufStridesInBytes, 
                bufBoundaryInBytes, 
                plane
                );

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1(
                                        bufMap.memBuf.memID,
                                        bufMap.memBuf.virtAddr,
                                        bufMap.memBuf.useNoncache, 
                                        bufMap.memBuf.bufSecu, 
                                        bufMap.memBuf.bufCohe);

        sp<ImageBufferHeap> pHeap = ImageBufferHeap::create(
                                                        LOG_TAG,
                                                        imgParam,
                                                        portBufInfo);
        if(pHeap == 0) {
            MY_LOGE("pHeap is NULL");
            return NULL;
        }
        //
        pBuf = pHeap->createImageBuffer();
        pBuf->incStrong(pBuf);

        bufMap.pImgBuf = pBuf;
        mvImgBufMap.push_back(bufMap);
    } else {
        MINT32 bufBoundaryInBytes = 0;
        IImageBufferAllocator::ImgParam imgParam(
                MSize(w,h), 
                w * h * 6 / 5,  //FIXME
                bufBoundaryInBytes
                );

        pBuf = mpIImageBufAllocator->alloc_ion(LOG_TAG, imgParam);
    }
	
    if (!pBuf 
		|| !pBuf->lockBuf(LOG_TAG, eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN)){
        MY_LOGE("Null allocated or lock Buffer failed\n");
        return  NULL;
    }

    pBuf->syncCache(eCACHECTRL_INVALID);

    return pBuf;
}

MBOOL
NightShot::
createJpegImg(IImageBuffer const * rSrcImgBufInfo
		  , NSCamShot::JpegParam const & rJpgParm
		  , MUINT32 const u4Transform
		  , IImageBuffer const * rJpgImgBufInfo
		  , MUINT32 & u4JpegSize)
{
	MBOOL ret = MTRUE;
	// (0). debug
	#if 0
	MY_LOGD("[createJpegImg] - E.");
	MY_LOGD("[createJpegImg] - rSrcImgBufInfo.eImgFmt=%d", rSrcImgBufInfo->getImgFormat());
	MY_LOGD("[createJpegImg] - u4Transform=%d", u4Transform);
	MY_LOGD("[createJpegImg]  dst imgbuf(0x%x)", rJpgImgBufInfo);
	#endif

	CPTLog(Event_FBShot_JpegEncodeImg, CPTFlagStart);
	//
	// (1). Create Instance
	NSCam::NSIoPipe::NSSImager::ISImager *pISImager = NSCam::NSIoPipe::NSSImager::ISImager::createInstance(rSrcImgBufInfo);
	if(!pISImager) {
		MY_LOGE("createJpegImg can't get ISImager instance.");
		return MFALSE;
	}

	// init setting
	pISImager->setTargetImgBuffer(rJpgImgBufInfo);
	//
	pISImager->setTransform(u4Transform);
	//
	pISImager->setEncodeParam(rJpgParm.fgIsSOI, rJpgParm.u4Quality);
	//
	pISImager->execute();
	//
	u4JpegSize = rJpgImgBufInfo->getBitstreamSize();

	pISImager->destroyInstance();
	CPTLog(Event_FBShot_JpegEncodeImg, CPTFlagEnd);

	MY_LOGD("[init] - X. ret: %d.", ret);
	return ret;
}


MBOOL
NightShot::
handleProcessedYuvData(IImageBuffer* puBuf)
{
    MY_LOGD("[handleProcessedYuvData] - E.");
	//
    MUINT32 u4JpegSize = 0;
    MUINT32 u4ThumbSize = 0; 	
	
    //1. create jpeg
    // jpeg param 
    NSCamShot::JpegParam rJpegParam(
            mJpegParam.mu4JpegQuality,         //Quality 
            MFALSE                             //isSOI 
            );
	
	//main yuv has rotate in pass2
    createJpegImg(puBuf,rJpegParam,0,mJpegBuf,u4JpegSize);
	
	//2.create thumbnail
	// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> begin
#if ORIGINAL_VERSION
#else
	if(mShotParam.mb4BuildInThumbnail && (NULL != mThumbBuf))
#endif
	// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> end
    if (0 != mJpegParam.mi4JpegThumbWidth && 0 != mJpegParam.mi4JpegThumbHeight) {
        NSCamShot::JpegParam rParam(mJpegParam.mu4JpegThumbQuality, MTRUE);
        createJpegImg(puBuf, rParam, 0, mThumbBuf, u4ThumbSize);
    }

	mJpegBuf->syncCache(eCACHECTRL_INVALID);
// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> begin
#if ORIGINAL_VERSION
#else
	if(mShotParam.mb4BuildInThumbnail && (NULL != mThumbBuf))
#endif
// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> end
    mThumbBuf->syncCache(eCACHECTRL_INVALID);
   
	//3.make exif and jpeg callback
    handleJpegData(mJpegBuf, mThumbBuf, NULL);	

	MY_LOGD("[handleProcessedYuvData] - X.");
    return MTRUE; 
}
