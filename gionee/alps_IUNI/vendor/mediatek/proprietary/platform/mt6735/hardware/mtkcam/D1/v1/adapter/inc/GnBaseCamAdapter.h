/*************************************************************************************
 * 
 * Description:
 * 	Defines gionee camera adapter.
 *<CR01402918>
 * Author : litao
 * Email  : litao@gionee.com
 * Date   : 2014/9/28
 *
 *************************************************************************************/
#ifndef _MTK_HAL_CAMADAPTER_GIONEE_INC_GNBASECAMADAPTER_H_
#define _MTK_HAL_CAMADAPTER_GIONEE_INC_GNBASECAMADAPTER_H_
#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/String8.h>
#include <IExtImgProc.h>
#include <ExtImgProc.h>
//#include <ExtImgProcImp.h>
#include <camera/MtkCamera.h>
//
#include <inc/CamUtils.h>
using namespace android;
using namespace MtkCamUtils;
//
#include <inc/ImgBufProvidersManager.h>
//
#include <mtkcam/v1/IParamsManager.h>
#include <mtkcam/v1/ICamAdapter.h>
#include <inc/BaseCamAdapter.h>

#include <Scenario/Shot/IShot.h>
#include <math.h>
#include "GNListener.h"
using namespace NSShot;
 
class GnBaseCamAdapter{
	public:
		virtual ~ GnBaseCamAdapter();
		GnBaseCamAdapter(BaseCamAdapter * owner)
		{
			this->owner = owner;
		}
		
		virtual bool                    init();
		virtual bool                    uninit();
		virtual void                    enableShotYuvMsg(ShotParam * shotParam,uint32_t mShotMode);
		virtual void   					destroyInstance()=0; 
		virtual void                    handlerReturnYuvData(
																	int64_t    i8Timestamp, 
																	uint32_t   u4PostviewSize, 
																	uint8_t *  puPostviewBuf){};
		virtual bool   					setParameters(){return true;}; 
		virtual bool   					setExParameters(int type, void* param) {return true;}; 
		virtual void   					setGnListener();
	 																							
		BaseCamAdapter * owner;
	protected:
		GNCameraFeature* mpGNCameraFeature;
		sp<IParamsManager>              mpParamsMgr;
		ExtImgProc* 					mpExtImgProc;
		GNListener		 mGNListener;
};
#endif