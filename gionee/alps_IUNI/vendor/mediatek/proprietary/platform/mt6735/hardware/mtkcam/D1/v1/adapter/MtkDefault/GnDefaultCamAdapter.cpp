/*************************************************************************************
 * 
 * Description:
 * 	Defines gionee camera adapter.
 *<CR01402918>
 * Author : litao
 * Email  : litao@gionee.com
 * Date   : 2014/9/28
 *
 *************************************************************************************/

#define LOG_TAG "MtkCam/GnCamAdapter"


#include <camera/GnCameraParameters.h>

#include "inc/GnDefaultCamAdapter.h"


#define MY_LOGV(fmt, arg...)       CAM_LOGV("[%s] "fmt, __func__, ##arg)
#define MY_LOGD(fmt, arg...)       CAM_LOGD("[%s] "fmt, __func__, ##arg)
#define MY_LOGI(fmt, arg...)       CAM_LOGI("[%s] "fmt, __func__, ##arg)
#define MY_LOGW(fmt, arg...)       CAM_LOGW("[%s] "fmt, __func__, ##arg)
#define MY_LOGE(fmt, arg...)       CAM_LOGE("[%s] "fmt, __func__, ##arg)
#define MY_LOGA(fmt, arg...)       CAM_LOGA("[%s] "fmt, __func__, ##arg)
#define MY_LOGF(fmt, arg...)       CAM_LOGF("[%s] "fmt, __func__, ##arg)


/******************************************************************************
*
*******************************************************************************/
GnDefaultCamAdapter*
GnDefaultCamAdapter::
createInstance(BaseCamAdapter* owner)
{
		MY_LOGE("%s",__func__);
		return new GnDefaultCamAdapter(owner);
}

/******************************************************************************
*
*******************************************************************************/
GnDefaultCamAdapter::
~GnDefaultCamAdapter()
{
	MY_LOGD("%s",__func__);

	//Gionee <wutangzhi><2015-06-02> modify for CR01492399 begin
	if (mGnPerfService != NULL) {
		mGnPerfService->disableHighPerformance();
		
		delete mGnPerfService;
		mGnPerfService = NULL;
	}
	//Gionee <wutangzhi><2015-06-02> modify for CR01492399 end
}

/******************************************************************************
*
*******************************************************************************/
GnDefaultCamAdapter::
GnDefaultCamAdapter(BaseCamAdapter* owner)
:GnBaseCamAdapter(owner)
,mSceneDetectionEnabled(false)
{
	//Gionee <wutangzhi><2015-06-02> modify for CR01492399 begin
	mGnPerfService = new GnPerfService();
	//Gionee <wutangzhi><2015-06-02> modify for CR01492399 end
}


/******************************************************************************
*
*******************************************************************************/
void 
GnDefaultCamAdapter::
destroyInstance()
{
	MY_LOGE("%s",__func__);
	delete this;
}


/******************************************************************************
*
*******************************************************************************/
void 
GnDefaultCamAdapter::
handlerReturnYuvData(int64_t    i8Timestamp,
					uint32_t   u4BufSize,
					uint8_t *  puBuf)
{
    int curFeatureMask = 0;
	int mask = GN_CAMERA_FEATURE_FACE_BEAUTY | GN_CAMERA_FEATURE_EFFECT
		|GN_CAMERA_FEATUER_NIGHT_SHOT | GN_CAMERA_FEATUER_MIRROR|GN_CAMERA_FEATUER_AGEGENDER_DETECTION
		|GN_CAMERA_FEATURE_SUPER_PHOTO | GN_CAMERA_FEATURE_SINGLE_HDR | GN_CAMERA_FEATURE_DEFOG_SHOT;
	
	curFeatureMask = mpGNCameraFeature->getFeatureMask();
	if (mpGNCameraFeature != NULL && (curFeatureMask & mask)) {
		int picWidth = 0;
		int picHeight = 0;
		owner->getParamsManager()->getPictureSize(&picWidth, &picHeight); 

		int rotation = mpParamsMgr->getInt(CameraParameters::KEY_ROTATION);
		if (rotation == 90 || rotation == 270) {
			picWidth = picWidth + picHeight;
			picHeight = picWidth - picHeight;
			picWidth = picWidth - picHeight;
		}	

		//Gionee <wutangzhi><2015-06-02> modify for CR01492399 begin
		if (mGnPerfService != NULL) {
			mGnPerfService->enableHighPerformance();
		}
		//Gionee <wutangzhi><2015-06-02> modify for CR01492399 end

		if (mSceneDetectionEnabled) {
			processYuvInScenePass(puBuf, u4BufSize, picWidth, picHeight);
		} else {
			processYuvInNormalPass(puBuf, u4BufSize, picWidth, picHeight, curFeatureMask);
		}

		//Gionee <wutangzhi><2015-06-02> modify for CR01492399 begin
		if (mGnPerfService != NULL) {
			mGnPerfService->disableHighPerformance();
		}
		//Gionee <wutangzhi><2015-06-02> modify for CR01492399 end
	}
}

/******************************************************************************
*
*******************************************************************************/
bool 
GnDefaultCamAdapter::
setParameters() 
{
	updateOrientation();
	
	updateSceneDetection();
	//If is in scene detection, update parameter via scene pass.
	if (mSceneDetectionEnabled) {
		configSceneParameter();
		return true;
	}
	
	updateFaceBeauty();
	updateGestureMode();
	updateLiveEffect();
	updateNightShot();
	updateNightVideo();
	updateMirror();
	updateAgeGenderDetection();
	updateGestureDetection();
	updateSingleHDRMode();
	updateSuperPhotoMode();
	updateSuperZoomMode();
	updateDefogShotMode();

	setGnListener();
	return true;
}

bool
GnDefaultCamAdapter::
setExParameters(int type, void* param)
{
	int32* value = (int32*)param;

	MY_LOGD("%s type = %d", __func__, type);

	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setExParameters(type, param);
	}

	return true;
}

/******************************************************************************
*
*******************************************************************************/
void 
GnDefaultCamAdapter::
updateFaceBeauty() 
{
	if (mpGNCameraFeature != NULL) {
		FaceBeautyParam param;	
		FaceBeautyLevelParam_t* levelParam = &param.faceBeautyLevelParam;
		
	    String8 const value = mpParamsMgr->getStr(GnCameraParameters::KEY_FACE_BEAUTY_MODE);

		memset(&param, 0, sizeof(param));
		
  	    if (value.string() != NULL && strcmp(value.string(), GnCameraParameters::FACE_BEAUTY_ON) == 0) {
  		    param.faceBeautyState = GN_FACEBEAUTY_ON;
  	    } else {
  		    param.faceBeautyState = GN_FACEBEAUTY_OFF;
  	    }	
		
		if (param.faceBeautyState == GN_FACEBEAUTY_ON) {
			//Face Beauty default mode, compatible with the old version.
			levelParam->faceBeautyLevel = mpParamsMgr->getInt(GnCameraParameters::KEY_FACE_BEAUTY_LEVEL);				
			 if (levelParam->faceBeautyLevel < 0) {
				levelParam->eyeEnlargmentLevel = mpParamsMgr->getInt(GnCameraParameters::KEY_EYE_ENLARGMENT_LEVEL);
				levelParam->slenderFaceLevel = mpParamsMgr->getInt(GnCameraParameters::KEY_SLENDER_FACE_LEVEL);
				levelParam->skinSoftenLevel = mpParamsMgr->getInt(GnCameraParameters::KEY_SKIN_SOFTEN_LEVEL);
				levelParam->skinBrightLevel = mpParamsMgr->getInt(GnCameraParameters::KEY_SKIN_BRIGHT_LEVEL);
			}				
		} 

		mpGNCameraFeature->setFaceBeauty(param);
	}	
}

/******************************************************************************
*
*******************************************************************************/
void GnDefaultCamAdapter::
updateSceneDetection()
{
	GNSceneDetectionParam param;
	
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_SCENE_DETECTION_MODE);

	param.sceneDetectionMode = GN_SCENE_DETECTION_OFF;

	mSceneDetectionEnabled = false;
	
	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::SCENE_DETECTION_ON) == 0) {
		int picWidth = 0;
		int picHeight = 0;
		int preWidth = 0;
		int preHeight = 0;
		
		mpParamsMgr->getPictureSize(&picWidth, &picHeight);
		mpParamsMgr->getPreviewSize(&preWidth, &preHeight);

		if (picWidth*picHeight >= preWidth*preHeight) {
			param.imageWidth = picWidth;
			param.imageHeight = picHeight;
		} else {
			param.imageWidth = preWidth;
			param.imageHeight = preHeight;
		}
		param.sceneDetectionMode = GN_SCENE_DETECTION_ON;
		param.imageFormat = GN_IMG_FORMAT_YUYV;
		mSceneDetectionEnabled = true;
	}  

	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setSceneDetection(param);
	}
	//MY_LOGD("sceneDetectionMode-----------------%d", sceneDetectionMode);
}

/******************************************************************************
*
*******************************************************************************/
void GnDefaultCamAdapter::
updateGestureMode()
{
	String8 const value = mpParamsMgr->getStr(GnCameraParameters::KEY_GESTURE_MODE);
	
    //MY_LOGD("----------value:%s", value.string());
	GNGestureShot_t mode = GN_GESTURE_SHOT_OFF;


	if (value.string() != NULL && strcmp(value.string(), GnCameraParameters::GESTURE_MODE_ON) == 0) {
		mode = GN_GESTURE_SHOT_ON;
	} else {
		mode = GN_GESTURE_SHOT_OFF;
	}

	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setGestureShot(mode);
	}
}

/******************************************************************************
*
*******************************************************************************/
void GnDefaultCamAdapter::
updateLiveEffect()
{
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_LIVE_EFFECT);
    //MY_LOGD("values:%s", values.string());
	
	GNLiveEffect_t liveEffect = GN_LIVE_EFFECT_NONE;

	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::LIVE_EFFECT_NONE) != 0) {
		liveEffect = getEffectIndex(values);		
	}  

	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setEffect(liveEffect);
	}
}

/******************************************************************************
*
*******************************************************************************/
GNLiveEffect_t GnDefaultCamAdapter::
getEffectIndex(String8 const effect)
{
	ALOGD("effect = %s", effect.string());
	
#ifdef GN_SCALADO_LIVE_EFFECT_SUPPORT
	if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_ANTIQUE) == 0)
	{
		return GN_LIVE_EFFECT_ANTIQUE;
	}
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_EMBOSS) == 0)
	{
		return GN_LIVE_EFFECT_EMBOSS;
	}
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_GRAYSCALE) == 0)
	{
		return GN_LIVE_EFFECT_GRAYSCALE;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_LOMO_GREEN) == 0)
	{
		return GN_LIVE_EFFECT_LOMO_GREEN;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_LOMO_NEUTRAL) == 0)
	{
		return GN_LIVE_EFFECT_LOMO_NEUTRAL;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_LOMO_YELLOW) == 0)
	{
		return GN_LIVE_EFFECT_LOMO_YELLOW;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_NEGATIVE) == 0)
	{
		return GN_LIVE_EFFECT_NEGATIVE;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_SEPIA) == 0)
	{
		return GN_LIVE_EFFECT_SEPIA;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_VIGNETTING) == 0)
	{	
		return GN_LIVE_EFFECT_VIGNETTING;
	} 	
#elif defined  GN_ARCSOFT_LIVE_EFFECT_SUPPORT
	if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_FLEETINGTIME) == 0)
	{
		return GN_LIVE_EFFECT_FLEETINGTIME;
	}
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_CRAYON) == 0)
	{
		return GN_LIVE_EFFECT_CRAYON;
	}
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_SNOWFLAKES) == 0)
	{
		return GN_LIVE_EFFECT_SNOWFLAKES;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_LIGHTBEAM) == 0)
	{
		return GN_LIVE_EFFECT_LIGHTBEAM;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_REFLECTION) == 0)
	{
		return GN_LIVE_EFFECT_REFLECTION;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_SUNSET) == 0)
	{
		return GN_LIVE_EFFECT_SUNSET;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_REVERSAL) == 0)
	{
		return GN_LIVE_EFFECT_REVERSAL;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_WARMLOMO) == 0)
	{
		return GN_LIVE_EFFECT_WARMLOMO;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_COLDLOMO) == 0)
	{	
		return GN_LIVE_EFFECT_COLDLOMO;
	} 	
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_SOFTPINK) == 0)
	{
		return GN_LIVE_EFFECT_SOFTPINK;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_JAPANBACKLIGHT) == 0)
	{
		return GN_LIVE_EFFECT_JAPANBACKLIGHT;
	} 
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_COSMETOLOGY_BACKLIGHT) == 0)
	{	
		return GN_LIVE_EFFECT_COSMETOLOGY_BACKLIGHT;
	}
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_BLACK_WHITE) == 0)
	{	
		return GN_LIVE_EFFECT_BLACKWHITE;
	}
	else if(strcmp(effect.string(), GnCameraParameters::LIVE_EFFECT_FINEFOOD) == 0)
	{	
		return GN_LIVE_EFFECT_FINEFOOD;
	}
#endif
	return GN_LIVE_EFFECT_NONE;
}

/******************************************************************************
*
*******************************************************************************/
void GnDefaultCamAdapter::
updateNightShot()
{
	GNNightShot_t nightShotMode = GN_NIGHT_SHOT_OFF;

	if (IsNightShotEnabled()) {
		nightShotMode = GN_NIGHT_SHOT_ON;
	}  

	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setNightShot(nightShotMode);
	}		
}

void GnDefaultCamAdapter::
updateNightVideo()
{
	GNNightVideo_t nightVideoMode = GN_NIGHT_VIDEO_OFF;

	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_NIGHT_VIDEO_MODE);
	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::NIGHT_VIDEO_ON) == 0) {
		nightVideoMode = GN_NIGHT_VIDEO_ON;
	}

	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setNightVideo(nightVideoMode);
	}		
}

/******************************************************************************
*
*******************************************************************************/
void
GnDefaultCamAdapter::
updateMirror()
{
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_MIRROR_MODE);
	GNMirror_t mirrorMode = GN_MIRROR_OFF;
	
	//Fixme: so far the live effect is invalid
	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::KEY_MIRROR_ON) == 0) {
		mirrorMode = GN_MIRROR_ON;
	}  
	
	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setMirror(mirrorMode);
	}
}

void GnDefaultCamAdapter::updateAgeGenderDetection()
{
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_AGEGENDER_DETECTION_MODE);
	GNAgeGenderDetection_t ageGenderMode = GN_AGEGENDER_DETECTION_OFF;

	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::AGEGENDER_DETECTION_ON) == 0) {
		ageGenderMode = GN_AGEGENDER_DETECTION_ON;
	}  
	
	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setAgeGenderDetection(ageGenderMode);
	}
}

void
GnDefaultCamAdapter::
updateGestureDetection()
{
	int values = mpParamsMgr->getInt(GnCameraParameters::KEY_GESTURE_EVENT_TYPE);

	if (mpGNCameraFeature != NULL) {
		if (values & GnCameraParameters::KEY_GESTURE_EVENT_OPEN_HAND_PRESENCE) {
			mpGNCameraFeature->setGestureDetection(GN_GESTURE_DETECTION_OPEN_HAND_PRESENCE);
		}

		if (values & GnCameraParameters::KEY_GESTURE_EVENT_FIST_PRESENCE) {
			mpGNCameraFeature->setGestureDetection(GN_GESTURE_DETECTION_FIST_PRESENCE);
		}

		if (values & GnCameraParameters::KEY_GESTURE_EVENT_FACE_PRESENCE) {
			mpGNCameraFeature->setGestureDetection(GN_GESTURE_DETECTION_FACE_PRESENCE);
		}

		if (values & GnCameraParameters::KEY_GESTURE_EVENT_THUMBS_UP_PRESENCE) {
			mpGNCameraFeature->setGestureDetection(GN_GESTURE_DETECTION_THUMBS_UP_PRESENCE);
		}

		if (values & GnCameraParameters::KEY_GESTURE_EVENT_VSIGN_PRESENCE) {
			mpGNCameraFeature->setGestureDetection(GN_GESTURE_DETECTION_VSIGN_PRESENCE);
		}
	}
}

void
GnDefaultCamAdapter::
updateSingleHDRMode()
{
	GNHdr_t hdrMode = GN_HDR_OFF;
	
	if (IsSingleHDREnabled()) {
		hdrMode= GN_HDR_ON;
	}  
	
	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setSingleHdr(hdrMode);
	}
}

void
GnDefaultCamAdapter::
updateSuperPhotoMode()
{
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_CAPTURE_MODE);
	GNSuperPhotoParam_t param;

	param.mode = GN_SUPER_PHOTO_OFF;
	
	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::CAPTURE_MODE_SUPER_PHOTO) == 0) {
		float scale = sqrt(mpParamsMgr->getFloat(GnCameraParameters::KEY_SUPER_PHOTO_SCALE_VALUE));
		if (scale > 1.0) {
			param.type = GN_SUPER_PHOTO_TYPE_SCALE;
			param.scale = scale;
		} else {
			param.type = GN_SUPER_PHOTO_TYPE_ZOOM;
			param.scale = (float)mpParamsMgr->getZoomRatioByIndex(mpParamsMgr->getInt(CameraParameters::KEY_ZOOM))/100.0f;
		}
		
		param.mode = GN_SUPER_PHOTO_ON;
	}  
	
	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setSuperPhoto(param);
	}
}

void
GnDefaultCamAdapter::
updateSuperZoomMode()
{
	float zoom = 0;
	float totalZoom = 0;
	float zoomRatio = 0;
	int zoomIdx = 0;
	int zoomLevel = 0;
	PicZoomParam PicZoomParam = {0};

	PicZoomParam.picZoomMode = GN_PIC_ZOOM_OFF;

	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_SUPER_ZOOM_MODE);
	if (values.string() != NULL &&  strcmp(values.string(), GnCameraParameters::SUPER_ZOOM_MODE_ON) == 0) {
		zoomIdx = mpParamsMgr->getInt(CameraParameters::KEY_ZOOM);
		zoomLevel = mpParamsMgr->getZoomRatioByIndex(zoomIdx);
		zoomRatio = (float)zoomLevel/100.0f;
		
		if (zoomRatio >= SUPER_ZOOM_RATIO_THRESHOLD) {
			PicZoomParam.picZoomMode = GN_PIC_ZOOM_ON;
			PicZoomParam.zoom = 1.0;
			PicZoomParam.totalZoom = zoomRatio;
		}
	}
	
	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setPicZoom(PicZoomParam);
	}
}

void
GnDefaultCamAdapter::
updateDefogShotMode()
{
	int picWidth = 0;
	int picHeight = 0;
	
	GNDefogShot_t defogShotMode = GN_DEFOG_SHOT_OFF;

	if (IsDefogShotEnabled()) {
		defogShotMode = GN_DEFOG_SHOT_ON;
	} 
	
	mpParamsMgr->getPictureSize(&picWidth, &picHeight);
	
	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setDefogShot(defogShotMode, GN_IMG_FORMAT_YUYV);
	}

}


void
GnDefaultCamAdapter::
updateOrientation()
{
	if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->setOrientation(mpParamsMgr->getInt(CameraParameters::KEY_ROTATION));
	}
}

bool
GnDefaultCamAdapter::
IsNightShotEnabled()
{
	bool res = false;
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_CAPTURE_MODE);

	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::CAPTURE_MODE_NIGHT_SHOT) == 0) {
		res = true;
	}  

	return res;
}

bool
GnDefaultCamAdapter::
IsSingleHDREnabled()
{
	bool res = false;
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_SINGLE_HDR_MODE);
	
	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::SINGLE_HDR_ON) == 0) {
		res = true;
	}  

	return res;
}

bool
GnDefaultCamAdapter::
IsDefogShotEnabled()
{
	bool res = false;
	String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_DEFOG_SHOT_MODE);

	if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::DEFOG_SHOT_MODE_ON) == 0) {
		res = true;
	}

	return res;
}

void
GnDefaultCamAdapter::
configSceneParameter()
{
	//disable other feature that dont related to scene detection
	FaceBeautyParam FBParam;
	FBParam.faceBeautyState = GN_FACEBEAUTY_OFF;
	mpGNCameraFeature->setFaceBeauty(FBParam);

	GNSuperPhotoParam_t superPhotoParam;
	superPhotoParam.mode = GN_SUPER_PHOTO_OFF;
	mpGNCameraFeature->setSuperPhoto(superPhotoParam);
	
	mpGNCameraFeature->setEffect(GN_LIVE_EFFECT_NONE);
	mpGNCameraFeature->setMirror(GN_MIRROR_OFF);
	mpGNCameraFeature->setAgeGenderDetection(GN_AGEGENDER_DETECTION_OFF);
	mpGNCameraFeature->setNightVideo(GN_NIGHT_VIDEO_OFF);
	
	//Enable Single HDR
	mpGNCameraFeature->setSingleHdr(GN_HDR_ON);
	//Enable Night Shot
	mpGNCameraFeature->setNightShot(GN_NIGHT_SHOT_ON);
	//Enable Defog Shot
	mpGNCameraFeature->setDefogShot(GN_DEFOG_SHOT_ON, GN_IMG_FORMAT_YUYV);
}

void
GnDefaultCamAdapter::
processYuvInScenePass(uint8_t* puBuf, uint32_t u4BufSize, int width, int height)
{
	if (IsNightShotEnabled()) {
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATUER_NIGHT_SHOT);	
	} else if (IsSingleHDREnabled()) {
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATURE_SINGLE_HDR);	
	}  else if (IsDefogShotEnabled()) {
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATURE_DEFOG_SHOT);
	}
}
void
GnDefaultCamAdapter::
processYuvInNormalPass(uint8_t* puBuf, uint32_t u4BufSize, int width, int height, int featureMask)
{
	if (featureMask & GN_CAMERA_FEATUER_MIRROR) {				
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATUER_MIRROR);	
	}
	
	if (featureMask & GN_CAMERA_FEATURE_DEFOG_SHOT) {
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATURE_DEFOG_SHOT);	
	}

	if (featureMask & GN_CAMERA_FEATURE_SINGLE_HDR) {
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATURE_SINGLE_HDR);	
	}
		
	if (featureMask & GN_CAMERA_FEATURE_FACE_BEAUTY) {						
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATURE_FACE_BEAUTY);
	}
	
	if (featureMask & GN_CAMERA_FEATUER_AGEGENDER_DETECTION) {
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATUER_AGEGENDER_DETECTION);
	}

	if (featureMask & GN_CAMERA_FEATURE_EFFECT) {					
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATURE_EFFECT);		
	}

	if (featureMask & GN_CAMERA_FEATUER_NIGHT_SHOT) {			
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_YUYV, GN_CAMERA_FEATUER_NIGHT_SHOT);			
	}	

	if (featureMask & GN_CAMERA_FEATURE_SUPER_PHOTO) {
		mpGNCameraFeature->processRaw((void*) puBuf, u4BufSize,
			width, height, GN_IMG_FORMAT_NV21, GN_CAMERA_FEATURE_SUPER_PHOTO);	
	}
}

void
GnDefaultCamAdapter::
enableShotYuvMsg(ShotParam * shotParam,uint32_t mShotMode)
{	
		String8 const values = mpParamsMgr->getStr(GnCameraParameters::KEY_MIRROR_MODE);
		int mask = GN_CAMERA_FEATURE_FACE_BEAUTY | GN_CAMERA_FEATURE_EFFECT 
			| GN_CAMERA_FEATURE_SINGLE_HDR | GN_CAMERA_FEATURE_DEFOG_SHOT;	
		
		if(strcmp(mpParamsMgr->getStr(CameraParameters::KEY_PICTURE_FORMAT), CameraParameters::PIXEL_FORMAT_YUV422I) == 0
			||strcmp(mpParamsMgr->getStr(CameraParameters::KEY_PICTURE_FORMAT), CameraParameters::PIXEL_FORMAT_YUV420SP) == 0
			||(mpGNCameraFeature != NULL && mpGNCameraFeature->getFeatureMask()& mask))
		{
			shotParam->mu4EnableYuvMsg = 1;
		}  else {
			shotParam->mu4EnableYuvMsg = 0;				
		}	
		
		if (values.string() != NULL && strcmp(values.string(), GnCameraParameters::KEY_MIRROR_ON) == 0) {
			shotParam->mu4IsFlip = true;
		}  		
		// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> begin
		int mBITmask = GN_CAMERA_FEATURE_FACE_BEAUTY | GN_CAMERA_FEATURE_EFFECT
					| GN_CAMERA_FEATUER_NIGHT_SHOT | GN_CAMERA_FEATURE_SINGLE_HDR
					| GN_CAMERA_FEATURE_DEFOG_SHOT;	
        #if ORIGINAL_VERSION
        #else
		if(mpGNCameraFeature != NULL && mpGNCameraFeature->getFeatureMask()& mBITmask){
            shotParam->mb4BuildInThumbnail = false;
		}
		else{
            shotParam->mb4BuildInThumbnail = true;
		}
		#endif
		// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> end
}
