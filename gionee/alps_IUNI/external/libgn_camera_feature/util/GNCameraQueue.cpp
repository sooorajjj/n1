/*******************************************************
 *
 * Description: Define API for process thread.
 *
 * Author:	zhuangxiaojian
 * E-mail:	zhuangxj@gionee.com
 * Date:	2015-06-30
 *
 ******************************************************/

#include <utils/Errors.h>
#include <utils/Log.h>
#include "GNCameraQueue.h"

namespace android {

GNCameraQueue::
GNCameraQueue()
{
    pthread_mutex_init(&m_lock, NULL);
    dlist_init(&m_head.list);
    m_size = 0;
    m_dataFn = NULL;
    m_userData = NULL;
}

GNCameraQueue::
GNCameraQueue(release_data_fn data_rel_fn, void *user_data)
{
    pthread_mutex_init(&m_lock, NULL);
    dlist_init(&m_head.list);
    m_size = 0;
    m_dataFn = data_rel_fn;
    m_userData = user_data;
}

GNCameraQueue::
~GNCameraQueue()
{
    flush();
    pthread_mutex_destroy(&m_lock);
}

bool
GNCameraQueue::
isEmpty()
{
    bool flag = true;
    pthread_mutex_lock(&m_lock);
    if (m_size > 0) {
        flag = false;
    }
    pthread_mutex_unlock(&m_lock);
    return flag;
}

bool
GNCameraQueue::
enqueue(void *data)
{
    DlistNode_t *node =
        (DlistNode_t *)malloc(sizeof(DlistNode_t));
    if (NULL == node) {
        ALOGE("%s: No memory for DlistNode_t", __func__);
        return false;
    }

    memset(node, 0, sizeof(DlistNode_t));
    node->data = data;

    pthread_mutex_lock(&m_lock);
    dlist_add_tail_node(&node->list, &m_head.list);
    m_size++;
    pthread_mutex_unlock(&m_lock);
    return true;
}

bool
GNCameraQueue::
enqueueWithPriority(void *data)
{
    DlistNode_t *node =
        (DlistNode_t *)malloc(sizeof(DlistNode_t));
    if (NULL == node) {
        ALOGE("%s: No memory for DlistNode_t", __func__);
        return false;
    }

    memset(node, 0, sizeof(DlistNode_t));
    node->data = data;

    pthread_mutex_lock(&m_lock);
    struct dlist *p_next = m_head.list.next;

    m_head.list.next = &node->list;
    p_next->prev = &node->list;
    node->list.next = p_next;
    node->list.prev = &m_head.list;

    m_size++;
    pthread_mutex_unlock(&m_lock);
    return true;
}

void*
GNCameraQueue::
dequeue(bool bFromHead)
{
    DlistNode_t* node = NULL;
    void* data = NULL;
    struct dlist *head = NULL;
    struct dlist *pos = NULL;

    pthread_mutex_lock(&m_lock);
    head = &m_head.list;
    if (bFromHead) {
        pos = head->next;
    } else {
        pos = head->prev;
    }
    if (pos != head) {
        node = member_of(pos, DlistNode_t, list);
        dlist_del_node(&node->list);
        m_size--;
    }
    pthread_mutex_unlock(&m_lock);

    if (NULL != node) {
        data = node->data;
        free(node);
    }

    return data;
}

void
GNCameraQueue::
flush()
{
    DlistNode_t* node = NULL;
    struct dlist *head = NULL;
    struct dlist *pos = NULL;

    pthread_mutex_lock(&m_lock);
    head = &m_head.list;
    pos = head->next;

    while(pos != head) {
        node = member_of(pos, DlistNode_t, list);
        pos = pos->next;
        dlist_del_node(&node->list);
        m_size--;

        if (NULL != node->data) {
            if (m_dataFn) {
                m_dataFn(node->data, m_userData);
            }
            free(node->data);
        }
        free(node);

    }
    m_size = 0;
    pthread_mutex_unlock(&m_lock);
}

void
GNCameraQueue::
flushNodes(match_fn match)
{
    DlistNode_t* node = NULL;
    struct dlist *head = NULL;
    struct dlist *pos = NULL;

    if ( NULL == match ) {
        return;
    }

    pthread_mutex_lock(&m_lock);
    head = &m_head.list;
    pos = head->next;

    while(pos != head) {
        node = member_of(pos, DlistNode_t, list);
        pos = pos->next;
        if ( match(node->data, m_userData) ) {
            dlist_del_node(&node->list);
            m_size--;

            if (NULL != node->data) {
                if (m_dataFn) {
                    m_dataFn(node->data, m_userData);
                }
                free(node->data);
            }
            free(node);
        }
    }
    pthread_mutex_unlock(&m_lock);
}

void
GNCameraQueue::
flushNodes(match_fn_data match, void *match_data){
    DlistNode_t* node = NULL;
    struct dlist *head = NULL;
    struct dlist *pos = NULL;

    if ( NULL == match ) {
        return;
    }

    pthread_mutex_lock(&m_lock);
    head = &m_head.list;
    pos = head->next;

    while(pos != head) {
        node = member_of(pos, DlistNode_t, list);
        pos = pos->next;
        if ( match(node->data, m_userData, match_data) ) {
            dlist_del_node(&node->list);
            m_size--;

            if (NULL != node->data) {
                if (m_dataFn) {
                    m_dataFn(node->data, m_userData);
                }
                free(node->data);
            }
            free(node);
        }
    }
    pthread_mutex_unlock(&m_lock);
}

}; // namespace android
