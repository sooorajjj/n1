/*************************************************************************************
 * 
 * Description:
 * 	Defines common variables.
 *
 * Author : zhuangxiaojian
 * Email  : zhuangxiaojian@gionee.com
 * Date   : 2015-02-03
 *
 *************************************************************************************/

#ifndef GN_FACE_BEAUTY_DEFS_H
#define GN_FACE_BEAUTY_DEFS_H

#define FB_PARAMS_ARRAY_IUNI_2_1

#define FLAWLESS_MIN_LEVEL 2
#define FLAWLESS_MAX_LEVEL_MALE 4
#define FLAWLESS_MAX_LEVEL_FEMALE 6

enum {
	GENDER_TYPE_UNKNOW 	= -1,
	GENDER_TYPE_MALE 	= 0,
	GENDER_TYPE_FEMALE	= 1,
};

#if defined(FB_PARAMS_ARRAY_IUNI_2_1)

#define EYE_BRIGHT_VALUE 30
#define TEETH_WHITE_VALUE 50
#define REMOVE_SHINE_VALUE 0
#define EYE_ENLARGEMENT_VALUE 10
#define SKIN_RUDDY_VALUE 15
#define SHAPE_STRENGTH_VALUE 0//100

#define BLUSH_VALUE 25

// Twelve groups of face beauty settings.
#define MAX_FB_LEVEL_SUPPORT 7 

#if 0
static BEAUTY_PARAM mFBParamArray[MAX_FB_LEVEL_SUPPORT] = 
{
	/*******************************************************************************************************************
	  * {[1]Skine Soften, [2]Skin Bright, [3]Slender Face, [4]Eye Enlargment, [5]Eye Bright, [6]Teeth White, [7]Remove shine, [8]Skin Ruddy}
	  ******************************************************************************************************************/
	{20, 25,  5, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, SHAPE_STRENGTH_VALUE, BLUSH_VALUE},
	{30, 25,  5, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, SHAPE_STRENGTH_VALUE, BLUSH_VALUE},
	{40, 35, 10, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, SHAPE_STRENGTH_VALUE, BLUSH_VALUE},
	{50, 35, 10, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, SHAPE_STRENGTH_VALUE, BLUSH_VALUE},
	{60, 35, 10, 30, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, SHAPE_STRENGTH_VALUE, BLUSH_VALUE},
	{70, 35, 15, 30, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, SHAPE_STRENGTH_VALUE, BLUSH_VALUE},
	{100,35, 15, 30, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, SHAPE_STRENGTH_VALUE, BLUSH_VALUE},
};
#else
static BEAUTY_PARAM mFBParamArray[MAX_FB_LEVEL_SUPPORT] = 
{
	/*******************************************************************************************************************
	  * {[1]Skine Soften, [2]Skin Bright, [3]Slender Face, [4]Eye Enlargment, [5]Eye Bright, [6]Teeth White, [7]Remove shine, [8]Skin Ruddy, [9]Shape, [10]Blush}
	  ******************************************************************************************************************/
	{25, 25,  5, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, 10, 0, 0},
	{30, 25,  5, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, 10, 0, 5},
	{35, 35, 10, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, 15, 0, 5},
	{40, 35, 10, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, 15, 0, 10},
	{45, 35, 10, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, 20, 0, 10},
	{50, 35, 15, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, 20, 0, 15},
	{60, 35, 15, 20, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, 15, 0, 15},
};
#endif

static inline int32 calculateFlawlessBeautyLevel(long age, long gender) 
{
	int32 beautyLevel = 0;

	if (gender == GENDER_TYPE_UNKNOW || age == 0) {
		return FLAWLESS_MIN_LEVEL;
	}

	switch(gender) {
		case GENDER_TYPE_MALE: {
			if (age <= 25) {
				beautyLevel = 1;
			} else if (age > 25 && age <= 35) {
				beautyLevel = 2;
			} else if (age > 35 && age <= 45) {
				beautyLevel = 3;
			} else if (age > 45 && age <= 55) {
				beautyLevel = 4;
			} else {
				beautyLevel = 5;
			}
			break;
		}
		case GENDER_TYPE_FEMALE: {
			if (age <= 15) {
				beautyLevel = 1;
			} else if (age > 15 && age <= 25) {
				beautyLevel = 2;
			} else if (age > 25 && age <= 35) {
				beautyLevel = 3;
			} else if (age > 35 && age <= 45) {
				beautyLevel = 4;
			} else if (age > 45 && age <= 55) {
				beautyLevel = 5;
			} else {
				beautyLevel = 6;
			}
			break;
		}
	}

	return beautyLevel;
}

#elif defined(FB_PARAMS_ARRAY_AMIGO_4_0) //Amigo_Camera4.0

#define EYE_BRIGHT_VALUE 30
#define TEETH_WHITE_VALUE 30
#define REMOVE_SHINE_VALUE 30
#define SKIN_RUDDY_VALUE 1
#define BLUSH_VALUE 0

// Twelve groups of face beauty settings. 
#define MAX_FB_LEVEL_SUPPORT 7

static BEAUTY_PARAM mFBParamArray[MAX_FB_LEVEL_SUPPORT] = 
{
	/*****************************************************************************************************
	  * {[1]Skine Soften, [2]Skin Bright, [3]Slender Face, [4]Eye Enlargment, [5]Eye Bright, [6]Teeth White, [7]Remove shine}
	  ****************************************************************************************************/
	//male
	{33, 22, 11, 1, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, 0, BLUSH_VALUE}, //0~20
	{40, 27,  5, 1, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, 0, BLUSH_VALUE}, //20~30
	{46, 33, 11, 1, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, 0, BLUSH_VALUE}, // > 30
	//female
	{40, 50,  5, 1, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, 0, BLUSH_VALUE}, //0~20
	{55, 40, 14, 1, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, 0, BLUSH_VALUE}, //20~30
	{45, 60,  5, 1, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, 0, BLUSH_VALUE}, // 30~40
	{40, 55,  5, 1, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE, 0, BLUSH_VALUE}, //>40
};

static inline int32 calculateFlawlessBeautyLevel(long age, long gender) 
{
	int32 beautyLevel = 0;

	if (gender == GENDER_TYPE_UNKNOW || age == 0) {
		return FLAWLESS_MIN_LEVEL;
	}

	switch(gender) {
		case GENDER_TYPE_MALE: {
			if(age < 20) {
				beautyLevel = 1;
			} else if (age > 20 && age < 30) {
				beautyLevel = 2;
			} else {
				beautyLevel = 3;
			}
			break;
		}
		case GENDER_TYPE_FEMALE: {
			if(age < 20) {
				beautyLevel = 4;
			} else if (age > 20 && age < 30) {
				beautyLevel = 5;
			} else if (age > 30 && age < 40) {
				beautyLevel = 6;
			} else {
				beautyLevel = 7;
			}
			break;
		}
	}

	return beautyLevel;
}

#else //for Amigo_Camera3.0

#define MAX_FB_LEVEL_SUPPORT 12

static BEAUTY_PARAM mFBParamArray[MAX_FB_LEVEL_SUPPORT] = 
{
	/*****************************************************************************************************
	  * {[1]Skine Soften, [2]Skin Bright, [3]Slender Face, [4]Eye Enlargment, [5]Eye Bright, [6]Teeth White, [7]Remove shine}
	  ****************************************************************************************************/
	{10, 10, 10, 10, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{15, 15, 15, 15, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{25, 25, 25, 25, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{30, 30, 30, 30, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{35, 35, 35, 35, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{40, 40, 40, 40, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{45, 45, 45, 45, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{55, 55, 55, 55, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{60, 60, 60, 60, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{65, 65, 65, 65, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{70, 70, 70, 70, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE},
	{75, 75, 75, 75, EYE_BRIGHT_VALUE, TEETH_WHITE_VALUE, REMOVE_SHINE_VALUE, SKIN_RUDDY_VALUE}
};

static inline int32 calculateFlawlessBeautyLevel(long age, long gender) 
{
	return 0;
}

#endif

#endif
