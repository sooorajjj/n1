/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/

#include <android/log.h>
#include <string.h>
#include "GioneeDefogShot.h"

#define LOG_TAG "GioneeDefogShot"

#define SUPPORT_MAX_WIDTH	4272
#define SUPPORT_MAX_HEIGHT	5696

namespace android {
GioneeDefogShot::GioneeDefogShot()
    : mInitialized(false)
    , mFrameProcessor(NULL)
{

}

GioneeDefogShot::~GioneeDefogShot()
{

}

int32
GioneeDefogShot::
init()
{

    int32 res = 0;

	if (mFrameProcessor == NULL) {
		mFrameProcessor = FrameProcessor::getInstance();
	}

    return res;
}

void
GioneeDefogShot::
deinit()
{
	if (mFrameProcessor != NULL) {
		mFrameProcessor->deinit();
	}

	mInitialized = false;
}

int32
GioneeDefogShot::
enableDefogShot(GNImgFormat format)
{
    int32 res = 0;

	if (!mInitialized) {
		if (mFrameProcessor != NULL) {
			res = mFrameProcessor->enableFrameProcess(SUPPORT_MAX_WIDTH, SUPPORT_MAX_HEIGHT, format, GN_CAMERA_FEATURE_DEFOG_SHOT);
			if (res) {
				PRINTE("Faile to enable Defog Shot");
				return -1;
			}
		}
	}

	mInitialized = true;

    return res;
}

void
GioneeDefogShot::
disableDefogShot()
{
	if (mFrameProcessor != NULL) {
		mFrameProcessor->disableFrameProcess(GN_CAMERA_FEATURE_DEFOG_SHOT);
	}

	mInitialized = false;
}

int32
GioneeDefogShot::
processDefogShot(PIMGOFFSCREEN imgSrc)
{
    int32 res = 0;

	if (!mInitialized) {
		PRINTD("processDefogShot not Initialized");
		return -1;
	}
	
	if (mFrameProcessor != NULL) {
		res = mFrameProcessor->processFrame(imgSrc, false);
		if (res != 0) {
			PRINTD("Failed to process Defog Shot");
		}
	}

	return res;
}

};
