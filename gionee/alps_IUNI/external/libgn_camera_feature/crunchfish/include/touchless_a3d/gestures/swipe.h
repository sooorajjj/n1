#ifndef SWIPE_AOFNOAWNFEAOWF
#define SWIPE_AOFNOAWNFEAOWF

#include "../gesture.h"


namespace TouchlessA3D {

  /**
   * Emitted when the user swipes left or right, with a fairly high speed.
   *
   * Copyright © 2013 Crunchfish AB. All rights reserved. All information herein is
   * or may be trade secrets of Crunchfish AB.
   */
  class TA3D_EXPORT Swipe : public Gesture {
  public:
    static const GestureType TYPE = 6;

    enum Direction {
      LEFT = 0,
      RIGHT
    };

  protected:
    Direction m_eDirection;

  public:
    Swipe(unsigned long long uTime, Direction eDirection);
    Gesture *clone() const;
    GestureType getType() const;
    Direction getDirection() const;
  };

}  // namespace TouchlessA3D


#endif
