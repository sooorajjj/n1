#ifndef THUMBS_UP_PRESENCE_GJKALMNXVNN
#define THUMBS_UP_PRESENCE_GJKALMNXVNN

#include "../object_presence.h"


namespace TouchlessA3D {

  /**
   * Emitted when thumbs up gestures are present in the input images to Engine#handleImage().
   *
   * Whenever a new thumbs up gesture is detected in an input image, a %ThumbsUpPresence
   * object with the Action START is emitted. After that, a %ThumbsUpPresence object
   * with the Action MOVEMENT is emitted for every input image in which the thumbs up
   * gesture is still seen. When the thumbs up gesture is no longer found in the input
   * images, a %ThumbsUpPresence object with the Action END is sent.  After that, no
   * more %ThumbsUpPresence objects are sent for that object id, but objects with a new
   * id will be sent if a thumbs up gesture is again * seen in the input images.
   *
   * Copyright © 2014 Crunchfish AB. All rights reserved. All information herein is
   * or may be trade secrets of Crunchfish AB.
   */
  class TA3D_EXPORT ThumbsUpPresence : public ObjectPresence {
  public:
    static const GestureType TYPE = 3;

    ThumbsUpPresence(unsigned long long uTime, int nObjectId, Action eAction,
      int x, int y, int z, int prevX, int prevY, int prevZ, int nWidth, int nHeight);
    Gesture *clone() const;
    GestureType getType() const;
  };

}  // namespace TouchlessA3D


#endif
