LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS:= optional

LOCAL_SRC_FILES:= \
	CrunchfishCameraFeature.cpp \

LOCAL_MODULE:= libcrunchfish

LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH)/include \
	$(TOP)/external/libgn_camera_feature/include \
	$(TOP)/bionic \
	$(TOP)/bionic/libstdc++/include \
	$(TOP)/external/stlport/stlport/ \

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/libs/armeabi-v7a/libtouchless_a3d.so:system/lib/libtouchless_a3d.so \
	$(LOCAL_PATH)/libs/arm64-v8a/libtouchless_a3d.so:system/lib64/libtouchless_a3d.so

ifeq "$(strip $(GN_CRUNCHFISH_FEATURE_SUPPORT))" "yes"
include $(BUILD_STATIC_LIBRARY)
endif
