/** @file VDRTSuperPhotoAPI.h
@brief Contains API for Visidon Real-time Super Photo algorithm


Copyright (C) 2014 Visidon Oy
This file is part of Visidon RTSuperPhoto SDK

*/
#ifndef VDRTSUPERPHOTOAPI_H
#define VDRTSUPERPHOTOAPI_H

/**
Enumeration for different error codes returned by API functions.
*/
typedef enum _VDRTSuperPhotoErrorCode
{
	VDRTSUPERPHOTO_OK=0, /**< Operation ok*/
	VDRTSUPERPHOTO_NOK=1, /**< Operation not ok*/
} VDRTSuperPhotoErrorCode;

/**
Enumeration for different parameters that can be controlled by API function.
*/
typedef enum _VDRTSuperPhotoParam
{
	VDRTSUPERPHOTO_ZOOM=0, /**< Zoom ratio defining how much to zoom in. Float value >= 1.0 */
	VDRTSUPERPHOTO_TOTALZOOM=1, /**< Zoom ratio observed by user. This is result of multiplying zoom (VDRTSUPERPHOTO_ZOOM) and zoom already performed for input by ISP for example. Used for quality finetuning. Float value >= 1.0 */
	VDRTSUPERPHOTO_STABILIZATION=2, /**< Enable / disable video stabilization mode (Possible values for this parameters are 0 (=disable) and 1 (=enable). Default value 0 (disabled).*/
	VDRTSUPERPHOTO_NOISEREDUCTION_STRENGTH=3, /**< Adjust noise reduction strength (Possible values for this parameter are interger values between 0 (disable) and 100 (maximum strength). Default value 65.*/
	VDRTSUPERPHOTO_SHARPENING=4, /**< Adjust sharpening strength (Possible values for this parameter are interger values between 0 (disable) and 100 (maximum strength). Default value 80.*/
	VDRTSUPERPHOTO_CONTRAST=5, /**< Adjust contrast strength (Possible values for this parameter are interger values between 0 (disable) and 100 (maximum strength). Default value 0.*/
	VDRTSUPERPHOTO_DEGHOSTING=6, /**< Set strength / disable de-ghosting (Possible values for this parameters are 0 (=disable) and 1-10. Larger values are stronger deghosting (more easily interpreted as moving target). For noisy images, smaller value may be required for smooth output. Default value 5.*/
	VDRTSUPERPHOTO_FLIPCHROMA=7, /**< Flip chroma channel (UV -> VU or VU -> UV). Possible values for this parameter are 0 (default) and 1. */
	VDRTSUPERPHOTO_LIGHTMODE = 8, /**< Skip most of the processing (reduces CPU and GPU usage, but decreases quality). Possible values for this parameter are 0 (default) and 1. */
	VDRTSUPERPHOTO_IMAGEFORMAT = 9, /**< Input YUV image format */
} VDRTSuperPhotoParam;

/**
Enumeration for different supported input image formats.
*/
typedef enum _VDRTSuperPhotoImageFormat
{
	VDRTSUPERPHOTO_IMAGE_FORMAT_NV21 = 17, /**< Input image is NV21*/
	VDRTSUPERPHOTO_IMAGE_FORMAT_YV12 = 842094169, /**< Input image is YV12*/
}VDRTSuperPhotoImageFormat;

#ifdef __cplusplus
extern "C" {
#endif

	/**
	* Initialize real-time superphoto engine
	* @param cols Input image width in pixels
	* @param rows Input image height in pixels	
	* @param engine Engine instance pointer
	* @return Error code indicating if initialization was succeed (VDRTSUPERPHOTO_OK) or failed (VDRTSUPERPHOTO_NOK)
	**/
	VDRTSuperPhotoErrorCode VDRTSuperPhoto_Initialize(int cols, int rows, void **engine);
	
	/**
	* Initialize real-time superphoto engine with external display where to render output.
	* @param cols Input image width in pixels
	* @param rows Input image height in pixels
	* @param window Output window where to render output
	* @param engine Engine instance pointer
	* @return Error code indicating if initialization was succeed (VDRTSUPERPHOTO_OK) or failed (VDRTSUPERPHOTO_NOK)
	**/
	VDRTSuperPhotoErrorCode VDRTSuperPhoto_InitializeWithWindow(int cols, int rows, void *window, void **engine);

	/**
	* Set engine parameters.
	* @param what Parameter to be set (please refer VDRTSuperPhotoParam structure for supported parameters)
	* @param value Parameter value (please refer VDRTSuperPhotoParam structure for supported values for each parameter)
	* @param engine Engine instance pointer
	* @return Error code indicating if parameter set was succeed (VDRTSUPERPHOTO_OK) or failed (VDRTSUPERPHOTO_NOK)
	**/
	VDRTSuperPhotoErrorCode VDRTSuperPhoto_SetParameter(int what, void* value, void *engine);

	/**
	* Process input frame with algorithm.
	* @param image YUV input frame data (NV21/NV12). Output will be written to this same buffer.
	* @param engine Engine instance pointer
	* @return Error code indicating if processing was succeed (VDRTSUPERPHOTO_OK) or failed (VDRTSUPERPHOTO_NOK)
	**/
	VDRTSuperPhotoErrorCode VDRTSuperPhoto_ProcessFrame(unsigned char *image, void *engine);

	/**
	* Release engine and allocated resources.
	* @param engine Engine instance pointer
	* @return Error code indicating if release set was succeed (VDRTSUPERPHOTO_OK) or failed (VDRTSUPERPHOTO_NOK)
	**/
	VDRTSuperPhotoErrorCode VDRTSuperPhoto_Release(void ** engine);

	/**
	* Get Real-time superphoto library version string.
	* @return Version string
	**/
	char * VDRTSuperPhoto_GetVersion();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // VDRTSUPERPHOTOAPI_H
