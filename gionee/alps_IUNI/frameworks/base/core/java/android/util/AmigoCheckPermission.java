// Gionee <Amigo_AppCheckPermission> <cheny> <20140704> add for CR01316056 begin
package android.util;

import java.util.HashSet;
import android.os.SystemProperties;

public class AmigoCheckPermission {
    public static HashSet<String> mCheckPermissions = new HashSet<String>();
    static {
        mCheckPermissions.add("android.permission.CALL_PHONE");
        mCheckPermissions.add("android.permission.SEND_SMS");
        mCheckPermissions.add("android.permission.SEND_SMS_MMS");
        mCheckPermissions.add("android.permission.READ_SMS");
        mCheckPermissions.add("android.permission.READ_SMS_MMS");
        mCheckPermissions.add("android.permission.READ_CONTACTS");
        mCheckPermissions.add("android.permission.READ_CALL_LOG");
        mCheckPermissions.add("android.permission.WRITE_SMS");
        mCheckPermissions.add("android.permission.WRITE_SMS_MMS");
        mCheckPermissions.add("android.permission.WRITE_CONTACTS");
        mCheckPermissions.add("android.permission.WRITE_CALL_LOG");
        mCheckPermissions.add("android.permission.ACCESS_FINE_LOCATION");
        mCheckPermissions.add("android.permission.ACCESS_COARSE_LOCATION");
        mCheckPermissions.add("android.permission.RECORD_AUDIO");
        mCheckPermissions.add("android.permission.READ_PHONE_STATE");
        mCheckPermissions.add("android.permission.CAMERA");
        mCheckPermissions.add("android.permission.BLUETOOTH");
        mCheckPermissions.add("android.permission.BLUETOOTH_ADMIN");
        mCheckPermissions.add("android.permission.CHANGE_WIFI_STATE");
        mCheckPermissions.add("android.permission.CHANGE_WIFI_STATE/amigo");
        mCheckPermissions.add("android.permission.NFC");
    }

    public static HashSet<String> mSpecialPerHs = new HashSet<String>();
    static {
        mSpecialPerHs.add("android.permission.CHANGE_WIFI_STATE");
        mSpecialPerHs.add("android.permission.BLUETOOTH_ADMIN"); 
    }
	
    public static HashSet<String> mImportantPermissions = new HashSet<String>();
    static {
        mImportantPermissions.add("android.permission.READ_SMS");
        mImportantPermissions.add("android.permission.READ_SMS_MMS");
        mImportantPermissions.add("android.permission.READ_CONTACTS");
        mImportantPermissions.add("android.permission.READ_CONTACTS_CALLS");
        mImportantPermissions.add("android.permission.READ_CALL_LOG");
        mImportantPermissions.add("android.permission.ACCESS_FINE_LOCATION");
        mImportantPermissions.add("android.permission.ACCESS_COARSE_LOCATION");
        mImportantPermissions.add("android.permission.RECORD_AUDIO");
	
    }
	
  public static HashSet<String> mSpecialPermissions = new HashSet<String>();
    static {
        mSpecialPermissions.add("android.permission.CALL_PHONE");
        mSpecialPermissions.add("android.permission.ACCESS_FINE_LOCATION");
        mSpecialPermissions.add("android.permission.READ_PHONE_STATE");
	
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-10> add for CR01452396 begin
    public static HashSet<String> mSepcialPackage = new HashSet<String>();
    static {
        mSepcialPackage.add("com.gionee.gsp");
        mSepcialPackage.add("com.gionee.wallet");	
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-10> add for CR01452396 end
    
    //Gionee <Amigo_CTS> <guozj> <2014-04-29> modify for CR01228673 begin
    public static boolean isGnAppPermCtrlSupport() {
    	//if cts is running ,shut perm control down
        boolean ctsTestRunning = SystemProperties.get("persist.sys.cts.test").equals("yes");
        return !ctsTestRunning;
    }
    //Gionee <Amigo_CTS> <guozj> <2014-04-29> modify for CR01228673 end
}
// Gionee <Amigo_AppCheckPermission> <cheny> <20140704> add for CR01316056 end