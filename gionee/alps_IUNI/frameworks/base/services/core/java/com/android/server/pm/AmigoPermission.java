//Gionee <Amigo_AppCheckPermission> <qiuxd> <2012-10-30> add for CR00711865 begin

/**
    File Description: <Amigo_AppCheckPermission> Support for PackageManagerService.

    Author:             lwzh

    Create Date:     2013/09/22

    Change List:      
*/

package com.android.server.pm;

import java.io.BufferedReader;
import android.text.TextUtils;
import android.database.ContentObserver;
import android.database.Cursor;
import android.content.ContentResolver;
import java.util.HashMap;
import android.net.Uri;
import android.os.SystemProperties;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.content.Context;
import android.content.pm.PackageParser;
import android.provider.Settings.Secure;
import android.os.Environment;
import android.util.Log;
import android.content.ContentValues;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageInfo;
import java.util.Set;
import android.content.pm.PermissionInfo;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;
import android.util.AtomicFile;
import com.android.internal.util.FastXmlSerializer;
import libcore.io.IoUtils;
import android.content.pm.PackageManager;
import static org.xmlpull.v1.XmlPullParser.END_DOCUMENT;
import static org.xmlpull.v1.XmlPullParser.START_TAG;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import android.util.Xml;
import android.util.AmigoCheckPermission;
import android.database.SQLException;

public class AmigoPermission {
    static final String TAG = "AmigoPermission";
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-10> modify for CR01452396 begin
    static final Uri GN_TRUST_URI = Uri.parse("content://com.amigo.settings.PermissionProvider/whitelist");  
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-10> modify for CR01452396 end
    private HashMap<String, Integer> mPkgNmaeMap = new HashMap<String, Integer>();
    private static String PKG_TAG = "pkg";
    private static String VERSION_TAG = "version";
    private static int IS_SYSTEMAPP = 1;
    private AtomicFile mPakageNameFile;
    private String mRomVersion;
    private static AmigoPermission mInstance = null;

    private Context mContext;
    private Handler mHandler;
    private PermObserver mPermObserver;

    static final Uri GN_PERM_URI = Uri.parse("content://com.amigo.settings.PermissionProvider/permissions");
    private static final String ACTION_UPDATE_PERM_MAP = "com.gionee.action.UPDATE_PERM_DB";
    private HashMap<String, Integer> mPermsMap = new HashMap<String, Integer>();

	private HashMap<String, String> mPermsGroupMap = new HashMap<String, String>();
    private HashMap<String, Integer> mPermsDeniedTimeMap = new HashMap<String, Integer>();

    static final int UPDATE_PERM_MAP_AFTER_PKG_ADDED = 17;
	static final int POST_INSTALL = 9;
	static final int UPDATE_GNPERMISSIN_DB = 20;
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-06-02> add for CR01492248 begin	
	static final int DELETE_GNPERMISSIN_DB = 21;
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-06-02> add for CR01492248 end	
    private BroadcastReceiver mIntentReceiver = null;

    private static final String DATA_APP_PATH="data/app";
    private static final String SYSTEM_APP_PATH="system/app";
	private static final String PRIV_APP_PATH="system/priv-app";
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-27> add for CR01441941 begin
    private static final String OPERATOR_APP_PATH="system/vendor/operator/app";
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-27> add for CR01441941 end
    private List<String> mPakagenameList = new ArrayList<String>();
    public static final String ALL_TRUST = "1";
    public static final String PARTIAL_TRUST = "0";
    public static final String NONE_TRUST = "-1";
	public static final String DEFULT_LEVEL = NONE_TRUST;


    public static AmigoPermission getInstance() {
        if (mInstance != null) {
            return mInstance;
        }

        mInstance = new AmigoPermission();

        return mInstance; 
    }

    public void init(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
    }
    
    public static void destory() {
        mInstance.mPermObserver.unObserve();
        mInstance.mPermsMap.clear();
		mInstance.mPermsGroupMap.clear();
        mInstance.mContext.unregisterReceiver(mInstance.mIntentReceiver);
    }

    public boolean CheckPermDenied(String permName, String pkgName) {
        boolean denied = false;
        initInteral();
        if(mPermsMap.isEmpty()){
            return denied;
        }
        String temp = permName+"-"+pkgName;
        synchronized (mPermsMap) {
            if(mPermsMap.containsKey(temp)){
                denied = (mPermsMap.get(temp) != 1);
            }
        }
        return denied;
    }

    public int amigoGetPermStatus(String permName, String pkgName) {
        int status = -1;
        initInteral();
        if(mPermsMap.isEmpty()){
            return status;
        }
        String temp = permName+"-"+pkgName;
        synchronized (mPermsMap) {
            if(mPermsMap.containsKey(temp)){
                status = mPermsMap.get(temp);
            }
        }
        return status;
    }	


    public void amigoUpdatePermStatus(String permName, String pkgName, int status) {
        if(mPermsMap.isEmpty() || null == permName || null == pkgName){
            return;
        }
        String temp = permName+"-"+pkgName;
        synchronized (mPermsMap) {
            if(mPermsMap.containsKey(temp)){
                mPermsMap.put(temp, status);
            }
        }
    }	

    public int amigoGetPermDeniedTime(String permName, String pkgName) {
        int times = 0;
        if(mPermsDeniedTimeMap.isEmpty() || null == permName || null == pkgName){
            return times;
        }
        String temp = permName+"-"+pkgName;
        synchronized (mPermsDeniedTimeMap) {
            if(mPermsDeniedTimeMap.containsKey(temp)){
                times = mPermsDeniedTimeMap.get(temp);
            }
        }
        return times;
    }	

    public void amigoSetPermDeniedTime(String permName, String pkgName, int times) {
        if (null == permName || null == pkgName) {
            return;
        }
        String temp = permName+"-"+pkgName;
        synchronized (mPermsDeniedTimeMap) {
            mPermsDeniedTimeMap.put(temp, times);
        }
    }	

    public boolean amigoIsContainPermDenied(String permName, String pkgName) {
        if (null == permName || null == pkgName) {
            return false;
        }	
        String temp = permName+"-"+pkgName;
        return mPermsDeniedTimeMap.containsKey(temp);
    }			
	
	public String amigoGetPkgPermGrp(String pkgName, String permName){
        String permGrup = null;
        initInteral();
        if(mPermsGroupMap.isEmpty()){
            return permGrup;
        }
        String temp = permName+"-"+pkgName;
        synchronized (mPermsGroupMap) {
            if(mPermsGroupMap.containsKey(temp)){
                permGrup = mPermsGroupMap.get(temp);
            }
        }
        return permGrup;	
    }
	
    public void doHandleMessageCheckPermission(Message msg) {
        switch(msg.what) {
            case  UPDATE_PERM_MAP_AFTER_PKG_ADDED: {
                mPermsMap.clear();
				mPermsGroupMap.clear();
                gnInitPermissionsMap();
                break;
            }

		    case  UPDATE_GNPERMISSIN_DB: {
		        if(mPkgNmaeMap.containsKey((String) msg.obj)){
		        	return;
		        }

		        initPermissionProp();
				updateGnPermissinDB((String) msg.obj);
				insertTrustDB((String) msg.obj);
				break;
		    }				
            // Gionee <Amigo_AppCheckPermission> <cheny> <2015-06-02> add for CR01492248 begin	
            case DELETE_GNPERMISSIN_DB: {
                try {
			 	    String packageName = (String) msg.obj;
                    Log.i(TAG, "remove PermDb for " + packageName);
                    mContext.getContentResolver().delete(AmigoPermission.GN_PERM_URI,
                        " packagename = ?", new String[] { packageName });
                    mContext.getContentResolver().delete(AmigoPermission.GN_TRUST_URI, 
                    " packagename = ?", new String[]{packageName});
                } catch (Exception unused) {
                    Log.e(TAG, "remove PermDb failed ");
                }
			 	break;
            }
            // Gionee <Amigo_AppCheckPermission> <cheny> <2015-06-02> add for CR01492248 end
            
        }
    }

    
    private AmigoPermission() {
    }

    private void initInteral() {
        if (mIntentReceiver == null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_UPDATE_PERM_MAP);

            mIntentReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if(action.equals(ACTION_UPDATE_PERM_MAP)){
                        Log.d("perm_ctrl", "pms mIntentReceiver");
                        mHandler.sendEmptyMessage(UPDATE_PERM_MAP_AFTER_PKG_ADDED);
                    }
                }
            };     
            
            mContext.registerReceiver(mIntentReceiver, filter);

            mPermObserver = new PermObserver(mHandler);
            mPermObserver.observe();

            gnInitPermissionsMap();
        }
    }

    private void gnInitPermissionsMap() {
        String[] projection = new String[]{"permission","packagename","status","permissiongroup"};
		try {
            Cursor cursor = mContext.getContentResolver().query(GN_PERM_URI, projection, null, null, null);
            Log.d("perm_ctrl", "pms gnInitPermissionsMap!");
            if (cursor != null) {
                cursor.moveToFirst();                        
                while (!cursor.isAfterLast()) {
                        String keys = cursor.getString(0)+"-"+ cursor.getString(1);
                        int value = cursor.getInt(2);
						String permGrup = cursor.getString(3);
						synchronized (mPermsMap) {
                          mPermsMap.put(keys, value);
						}
						synchronized (mPermsGroupMap) {
						  mPermsGroupMap.put(keys, permGrup);
                        }
                        cursor.moveToNext();                
                }
                cursor.close();
            }
        } catch (Exception e) {
        }
    }
    
    class PermObserver extends ContentObserver{
        
        PermObserver(Handler handler) {
            super(handler);
        }
        
        public void observe() {
            ContentResolver resolver = mContext.getContentResolver();
            resolver.registerContentObserver(GN_PERM_URI,false,this);
            Log.d("perm_ctrl", "pms observe DB");
        }
        
        @Override
        public void onChange(boolean selfChange) {
            synchronized (mPermsMap) {
                mPermsMap.clear();
            }

			synchronized (mPermsGroupMap) {
				mPermsGroupMap.clear();
            }
            gnInitPermissionsMap();
        }

        public void unObserve() {
            ContentResolver resolver = mContext.getContentResolver();
            resolver.unregisterContentObserver(this);
        }
    }    

    private void updateGnPermissinDB(String packageName) {
        List<PermissionInfo> mPermsList = new ArrayList<PermissionInfo>();
        Set<PermissionInfo> permSet = new HashSet<PermissionInfo>();

        PackageInfo pkgInfo;
        try {
            pkgInfo = mContext.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
        } catch (NameNotFoundException e) {
            return;
        }
        // Extract all user permissions
        if ((pkgInfo.applicationInfo != null) && (pkgInfo.applicationInfo.uid != -1)) {
            getAllUsedPermissions(pkgInfo.applicationInfo.uid, permSet);
        }
        for (PermissionInfo tmpInfo : permSet) {
            mPermsList.add(tmpInfo);
        }

        int status = ALL_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))?1:0 ; 
        ContentResolver cr = mContext.getContentResolver();
        for (PermissionInfo pInfo : mPermsList) {
            if (!AmigoCheckPermission.mCheckPermissions.contains(pInfo.name)) {
                continue;
            }

            int level = status;
            if (PARTIAL_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))
                    && !AmigoCheckPermission.mImportantPermissions.contains(pInfo.name)) {
                level = 1;
            }
	    if(AmigoCheckPermission.mSpecialPermissions.contains(pInfo.name))
	    {
		level = 1;
	    }	
            try {
                String grpName = (pInfo.group == null) ? "default" : pInfo.group;
                if (!isInsertedPermStatus(packageName, pInfo.name)) {
                    ContentValues cv = new ContentValues();
                    cv.put("packagename", packageName);
                    cv.put("permission", pInfo.name);
                    cv.put("permissiongroup", grpName);
                    cv.put("status", level);
                    cr.insert(GN_PERM_URI, cv);
                    Log.i(TAG, "---->add permission " + pInfo.name + " to GnPermissinDB for " + packageName);
                    if (pInfo.name.contains("_SMS") || pInfo.name.contains("INTERNET")) {
                        insertMmsPermission(packageName, pInfo.name, grpName);
                    }
                }
            
                if (pInfo.name.contains("_SMS")) {
                    String mmsPermission = pInfo.name + "_MMS";
                    if (!isInsertedPermStatus(packageName, mmsPermission)){
                        ContentValues cv1 = new ContentValues();
                        cv1.put("packagename", packageName);
                        cv1.put("permission", mmsPermission);
                        cv1.put("permissiongroup", grpName);
                        cv1.put("status", level);
                        cr.insert(GN_PERM_URI, cv1);
                    }
                }
            } catch (SQLException unused) {
        	    Log.e(TAG, "Insert PermDb failed ");
            } catch (IllegalArgumentException argExp) {
                Log.e(TAG, "Insert PermDb failed argExp ");
            }
        }
        Intent intent = new Intent();
        intent.setAction("com.gionee.action.UPDATE_PERM_DB");
        mContext.sendBroadcast(intent);
    }
    private void insertTrustDB(String packageName) {
         //Gionee <Amigo_AppCheckPermission> <cheny> <2015-05-13> delete for CR01477688 begin
    	 if(getInstalledAppTrustCount(packageName) > 0) {
             //deleteTrustByPkgName(packageName); 
         } else {
    	     int status = ALL_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))?1:0 ; 
             ContentResolver cr = mContext.getContentResolver(); 
             ContentValues cv = new ContentValues();
             cv.put("packagename", packageName);
             cv.put("status", status); 
             cr.insert(GN_TRUST_URI, cv);
         }
         //Gionee <Amigo_AppCheckPermission> <cheny> <2015-05-13> delete for CR01477688 end
    }
    private int getInstalledAppTrustCount(String packageName){
        int count = 0;
        Cursor cursor = mContext.getContentResolver().query(GN_TRUST_URI, new String[]{"status"},
                " packagename = ?", new String[]{packageName}, null);
        if(cursor != null){
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }
    private void deleteTrustByPkgName(String packageName){
    	mContext.getContentResolver().delete(GN_TRUST_URI, " packagename = ?", new String[]{packageName});
    }
	
	   private void insertMmsPermission(String packageName, String permission, String grpName) {
        if (permission.contains("SEND_SMS") || permission.contains("INTERNET")) {
            if (isInsertedPermStatus(packageName, "android.permission.SEND_SMS_MMS")) {
                return;
            }
        }

        String mmsPermission = permission;
        if (permission.contains("_SMS")) {
            mmsPermission = permission + "_MMS";
        }
        if (permission.contains("INTERNET")) {
            mmsPermission = "android.permission.SEND_SMS_MMS";
        }

        int status = ALL_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))?1:0 ; 
        int level = status;
        if (PARTIAL_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))
                && !AmigoCheckPermission.mImportantPermissions.contains(permission)) {
            level = 1;
        }

        try {
            ContentValues cv = new ContentValues();
            cv.put("packagename", packageName);
            cv.put("permission", mmsPermission);
            cv.put("permissiongroup", grpName);
            cv.put("status", level);
            mContext.getContentResolver().insert(GN_PERM_URI, cv);
        }  catch (SQLException unused) {
            Log.e(TAG, "Insert PermDb failed ");
        }
    }

    private void getAllUsedPermissions(int sharedUid, Set<PermissionInfo> permSet) {
        String[] sharedPkgList = mContext.getPackageManager().getPackagesForUid(sharedUid);
        if (sharedPkgList == null || (sharedPkgList.length == 0)) {
            return;
        }
        for (String sharedPkg : sharedPkgList) {
            getPermissionsForPackage(sharedPkg, permSet);
        }
    }

    private void getPermissionsForPackage(String packageName, Set<PermissionInfo> permSet) {
        PackageInfo pkgInfo;
        try {
            pkgInfo = mContext.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
        } catch (NameNotFoundException e) {
            Log.v(TAG, "Could'nt retrieve permissions for package:" + packageName);
            return;
        }
        if ((pkgInfo != null) && (pkgInfo.requestedPermissions != null)) {
            extractPerms(pkgInfo.requestedPermissions, permSet);
        }
    }

    private void extractPerms(String[] strList, Set<PermissionInfo> permSet) {
        if ((strList == null) || (strList.length == 0)) {
            return;
        }
        for (String permName : strList) {
            try {
                PermissionInfo tmpPermInfo = mContext.getPackageManager().getPermissionInfo(permName, 0);
                if (tmpPermInfo != null) {
                    permSet.add(tmpPermInfo);
                }
            } catch (NameNotFoundException e) {
            }
        }
    }

    private boolean isInsertedPermStatus(String pkgName, String permission) {
        boolean status = false;
        Cursor c = null;
        try {
            c = mContext.getContentResolver().query(GN_PERM_URI, new String[] {"status"},
                    "  packagename = ? and permission =? ", new String[] {pkgName, permission}, null);
            if (c != null && c.getCount() > 0) {
                status = true;
            }
        } finally {
            if (c != null && !c.isClosed()) {
                c.close();
                c = null;
            }
        }
        return status;
    }
    public void readSystemAppInfo() {
    	mPakageNameFile = new AtomicFile(new File("/data/system/", "pakagename.xml"));
    	Handler mHandler = new Handler();
    	mHandler.post(new Runnable() {
			@Override
			public void run() {
				boolean read_sucucess = Amigo_readSystemAppInfoPkgname();
				if (getRomVersion().equals(mRomVersion) && read_sucucess){
					return;
				}
				getPreInstallAppInfo();
				Amigo_writeSystemAppInfoPkgname(mPakagenameList);
			}
		});
    }
    
    private void getPreInstallAppInfo() {   	
    	File systemAppDir = new File(SYSTEM_APP_PATH);
    	listPreInstallAppFile(systemAppDir,true);
    	File dataAppDir = new File(DATA_APP_PATH);
    	listPreInstallAppFile(dataAppDir,false);
    	File privAppDir = new File(PRIV_APP_PATH);
    	listPreInstallAppFile(privAppDir,true);	
        // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-27> add for CR01441941 begin
		File operatorAppDir = new File(OPERATOR_APP_PATH);
   	    listPreInstallAppFile(operatorAppDir,true);  	
        // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-27> add for CR01441941 end
    }	

	private void listPreInstallAppFile(File f, boolean isSystemApp) {
	    if(f == null) {
            return;
	    }
        if(f.isDirectory()){
            File[] fileArray=f.listFiles();
            if(fileArray!=null){
                for (int i = 0; i < fileArray.length; i++) {
                    listPreInstallAppFile(fileArray[i], isSystemApp);
                }
            }
        } else{
        //Gionee <Amigo_AppCheckPermission> <zenggz> <2015-03-10> modify for CR01450302 begin
            //fillPkgNmaeMapAndList(f);
			getPkgNameOfPreInstallApp(f, isSystemApp);
		//Gionee <Amigo_AppCheckPermission> <zenggz> <2015-03-10> modify for CR01450302 end
		
        }
    
    }
    	   
    private void getPkgNameOfPreInstallApp(File f, boolean isSystemApp){
	    if(f == null) {
            return;
	    }
        String filename = f.getName();
        if(isSystemApp){
            if(filename.endsWith(".apk")){
                fillPkgNmaeMapAndList(f);
            }	
        } else {
            // Gionee <Amigo_AppCheckPermission> <cheny> <2015-07-21> add for CR01483596 begin
            if (filename.equals("base.apk") && f.getPath().split("\\.").length > 2){
                try {
                    PackageManager pm = mContext.getPackageManager(); 
                    PackageInfo appinfo = pm.getPackageArchiveInfo(f.getPath(), PackageManager.GET_ACTIVITIES);
                    if(appinfo != null && appinfo.applicationInfo!= null && appinfo.applicationInfo.packageName != null){
                        updateGnPermissinDB(appinfo.applicationInfo.packageName);
                        insertTrustDB(appinfo.applicationInfo.packageName);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "InertDB Failed!!!");
                }
            } else
            // Gionee <Amigo_AppCheckPermission> <cheny> <2015-07-21> add for CR01483596 end
            //Gionee <Amigo_AppCheckPermission> <cheny> <2015-06-04> modify for CR01492130 begin
            if(filename.endsWith(".apk") && filename.split("\\.").length == 2){
            //Gionee <Amigo_AppCheckPermission> <cheny> <2015-06-04> modify for CR01492130 end
        		fillPkgNmaeMapAndList(f);
            }
        }
    }

    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-07-02> add for CR01511003 begin
    public static HashSet<String> mTrustPackage = new HashSet<String>();
    static {
        mTrustPackage.add("com.cttl.testService");
        mTrustPackage.add("com.cttl.usbtest");
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-07-02> add for CR01511003 end
    
    private void fillPkgNmaeMapAndList(File f){
    	PackageManager pm = mContext.getPackageManager(); 
		PackageInfo appinfo = pm.getPackageArchiveInfo(f.getPath(), PackageManager.GET_ACTIVITIES);
        // Gionee <Amigo_AppCheckPermission> <cheny> <2015-07-02> modify for CR01511003 begin
        if(appinfo != null 
            && appinfo.applicationInfo!= null 
            && !mTrustPackage.contains(appinfo.applicationInfo.packageName)){
        // Gionee <Amigo_AppCheckPermission> <cheny> <2015-07-02> modify for CR01511003 end
			mPkgNmaeMap.put(appinfo.applicationInfo.packageName, 0);
			mPakagenameList.add(appinfo.applicationInfo.packageName); 
        }
    	
    }
    
    public boolean isSystemApp(String pkgName){
    	if(pkgName != null){
    		if(mPkgNmaeMap.containsKey(pkgName)){
    			return true ;
    		}
    	}
    	return false ;
    }

    private boolean Amigo_readSystemAppInfoPkgname() {
    	boolean state = false ;
    	mPkgNmaeMap.clear();
        FileInputStream fis = null;
        try {
            fis = mPakageNameFile.openRead();
            final XmlPullParser in = Xml.newPullParser();
            in.setInput(fis, null);
            int type;
            while ((type = in.next()) != END_DOCUMENT) {
                final String tag = in.getName();
                if (type == START_TAG) {
                    if (PKG_TAG.equals(tag)) {
                        String pkgName = in.nextText();
                        mPkgNmaeMap.put(pkgName, IS_SYSTEMAPP);
                    }
                    if(VERSION_TAG.equals(tag)){
                    	mRomVersion = in.nextText();
                    }
                }
            }
            if(mPkgNmaeMap.size() > 0) {
            	state = true ;
            }
        } catch (FileNotFoundException e) {
        	state = false ;
            Log.e(TAG, "Amigo_readSystemAppInfoPkgname", e);    
        } catch (IOException e) {
        	state = false ;
            Log.e(TAG, "Amigo_readSystemAppInfoPkgname", e);
        } catch (XmlPullParserException e) {
        	state = false ;
            Log.e(TAG, "Amigo_readSystemAppInfoPkgname", e);
        } finally {
            IoUtils.closeQuietly(fis);
        }
        return state ;
    }
    public static String getRomVersion() {
        String version = "";
        try {
            Class<?> clazz = Class.forName("android.os.SystemProperties");
            Method method = clazz.getMethod("get", String.class, String.class);
            version = (String) method.invoke(null, "ro.gn.gnznvernumber", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }
    private void Amigo_writeSystemAppInfoPkgname(List pkgNameList) {
        FileOutputStream fos = null;
        try {
            fos = mPakageNameFile.startWrite();
            XmlSerializer out = new FastXmlSerializer();
            out.setOutput(fos, "utf-8");
            out.startDocument(null, true);
            for (int i = 0; i < pkgNameList.size(); i++) {
                out.startTag(null, PKG_TAG);
                out.text("" + pkgNameList.get(i));
                out.endTag(null, PKG_TAG);
            }
            out.startTag(null, VERSION_TAG);
            out.text("" + getRomVersion());
            out.endTag(null, VERSION_TAG);
            out.endDocument();
            mPakageNameFile.finishWrite(fos);
        } catch (IOException e) {
            if (fos != null) {
            	mPakageNameFile.failWrite(fos);
            }
            Log.e(TAG, "Amigo_writeSystemAppInfoPkgname", e);
        }
    }

    public void initPermissionProp() {
        if("".equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))){
            SystemProperties.set("persist.sys.permission.level", SystemProperties.get("ro.gn.permission.trust.level", DEFULT_LEVEL));
        }
    }
}
//Gionee <Amigo_AppCheckPermission> <qiuxd> <2012-10-30> add for CR00711865 end