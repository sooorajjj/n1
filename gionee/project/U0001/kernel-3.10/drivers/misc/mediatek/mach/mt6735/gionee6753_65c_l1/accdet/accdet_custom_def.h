// Headset mode MIC define
typedef enum
{
	ACCDET_MIC_MODE_ACC = 1,
	ACCDET_MIC_MODE_LOW_COST_WITHOUT_IN_BIAS = 2,
	ACCDET_MIC_MODE_LOW_COST_WITH_IN_BIAS = 6,
} ACCDET_MIC_MODE;
//Gionee zhangliu 20150330 modify for headset detect begin
#if defined(CONFIG_GN_BSP_AUDIO_SUPPORT)
#define ACCDET_MIC_MODE	(1)
//#define ACCDET_MIC_MODE	(6)
#endif
//Gionee zhangliu 20150330 modify for headset detect end

// use accdet + EINT solution
//Gionee zhangliu 20150330 modify for headset detect begin
#if defined(CONFIG_GN_BSP_AUDIO_SUPPORT)
#define ACCDET_EINT   //ACC mode
#endif
//Gionee zhangliu 20150330 modify for headset detect end
#ifndef ACCDET_EINT
#define ACCDET_EINT_IRQ  //DCC mode
#endif
//#define ACCDET_PIN_SWAP
//#define ACCDET_PIN_RECOGNIZATION
#define ACCDET_HIGH_VOL_MODE
#ifdef ACCDET_HIGH_VOL_MODE
#define ACCDET_MIC_VOL 7     //2.5v
#else
#define ACCDET_MIC_VOL 2     //1.9v
#endif

#define ACCDET_SHORT_PLUGOUT_DEBOUNCE
#define ACCDET_SHORT_PLUGOUT_DEBOUNCE_CN 20
//#define FOUR_KEY_HEADSET


