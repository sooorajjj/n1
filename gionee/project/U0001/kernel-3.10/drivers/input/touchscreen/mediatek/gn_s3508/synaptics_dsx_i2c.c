/*
 * Synaptics DSX touchscreen driver
 *
 * Copyright (C) 2012 Synaptics Incorporated
 *
 * Copyright (C) 2012 Alexandra Chin <alexandra.chin@tw.synaptics.com>
 * Copyright (C) 2012 Scott Lin <scott.lin@tw.synaptics.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/types.h>
#include <linux/of_gpio.h>
#include <linux/platform_device.h>
#include "tpd.h"
#include <linux/dma-mapping.h>

#include "synaptics_dsx.h"
#include "synaptics_dsx_core.h"

#define SYN_I2C_RETRY_TIMES 10

//Gionee BSP1 yaoyc 20150317 add for test TP IC full reset CR01455491 begin
#define SYN_IC_RESET_RETRY_TIMES 5
//Gionee BSP1 yaoyc 20150317 add for test TP IC full reset CR01455491 end


// for DMA accessing
static u8 *gpwDMABuf_va = NULL;
static dma_addr_t *gpwDMABuf_pa = 0;
static u8 *gprDMABuf_va = NULL;
static dma_addr_t *gprDMABuf_pa = 0;
struct i2c_msg *read_msg = NULL;

#define I2C_DMA_LIMIT 252


static int synaptics_rmi4_i2c_set_page(struct synaptics_rmi4_data *rmi4_data,
		unsigned short addr)
{
	int retval;
	unsigned char retry;
	unsigned char buf[PAGE_SELECT_LEN];
	unsigned char page;
	struct i2c_client *i2c = to_i2c_client(rmi4_data->pdev->dev.parent);

	page = ((addr >> 8) & MASK_8BIT);
	if (page != rmi4_data->current_page) {
		buf[0] = MASK_8BIT;
		buf[1] = page;
		for (retry = 0; retry < SYN_I2C_RETRY_TIMES; retry++) {
			retval = i2c_master_send(i2c, buf, PAGE_SELECT_LEN);
			if (retval != PAGE_SELECT_LEN) {
				dev_err(rmi4_data->pdev->dev.parent,
						"%s: I2C retry %d\n",
						__func__, retry + 1);
				msleep(20);
			} else {
				rmi4_data->current_page = page;
				break;
			}
		}
	} else {
		retval = PAGE_SELECT_LEN;
	}

	return retval;
}

#if 1
static int synaptics_rmi4_i2c_read(struct synaptics_rmi4_data *rmi4_data,
		unsigned short addr, unsigned char *data, unsigned short length)
{
	int retval;
	unsigned char retry;
	unsigned char buf;
	unsigned char *buf_va = NULL;

	struct i2c_client *i2c = to_i2c_client(rmi4_data->pdev->dev.parent);

	mutex_lock(&(rmi4_data->rmi4_io_ctrl_mutex));

	if(!gprDMABuf_va){
		rmi4_data->input_dev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
	  gprDMABuf_va = (u8 *)dma_alloc_coherent(&rmi4_data->input_dev->dev, 1024, &gprDMABuf_pa, GFP_KERNEL);
	  if(!gprDMABuf_va){
		printk("[Error] Allocate DMA I2C Buffer failed!\n");
	  }
	}

	buf_va = gprDMABuf_va;

	if (!read_msg) {
		printk("%s ; alloc i2c_msg buf \n", __func__);
		read_msg = kcalloc(2, sizeof(struct i2c_msg), GFP_KERNEL);
	}

	read_msg[0].addr = i2c->addr;
	read_msg[0].flags = 0;
	read_msg[0].len = 1;
	read_msg[0].buf = &buf;
	read_msg[0].timing = 400;
	
	read_msg[1].addr = i2c->addr;
	read_msg[1].flags = I2C_M_RD;
	read_msg[1].len = length;
	read_msg[1].buf = gprDMABuf_pa;
	read_msg[1].ext_flag = (i2c->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG);
	read_msg[1].timing = 400;



	buf = addr & MASK_8BIT;

	retval = synaptics_rmi4_i2c_set_page(rmi4_data, addr);
	if (retval != PAGE_SELECT_LEN) {
		retval = -EIO;
		goto exit;
	}

	for (retry = 0; retry < SYN_I2C_RETRY_TIMES; retry++) {
		if (i2c_transfer(i2c->adapter, read_msg, 2 ) == 2) {
			retval = length;
			break;
		}
		dev_err(rmi4_data->pdev->dev.parent,
				"%s: I2C retry %d\n",
				__func__, retry + 1);
		msleep(20);
//Gionee BSP1 yaoyc 20150317 add for test TP IC full reset CR01455491 begin
		if(retry == SYN_IC_RESET_RETRY_TIMES){
			dev_err(rmi4_data->pdev->dev.parent,
				"%s: i2c fail, so reset the IC\n",
				__func__);
			synaptics_rmi4_full_reset(rmi4_data);
		}
//Gionee BSP1 yaoyc 20150317 add for test TP IC full reset CR01455491 end
	}

	if (retry == SYN_I2C_RETRY_TIMES) {
		dev_err(rmi4_data->pdev->dev.parent,
				"%s: I2C read over retry limit\n",
				__func__);
		retval = -EIO;
	}
	memcpy(data, buf_va, length);

exit:
	mutex_unlock(&(rmi4_data->rmi4_io_ctrl_mutex));

	return retval;
}

#else
static int synaptics_rmi4_i2c_read(struct synaptics_rmi4_data *rmi4_data,
		unsigned short addr, unsigned char *data, unsigned short length)
{
	int retval;
	unsigned char retry;
	unsigned char buf;
	unsigned char *buf_va = NULL;
	int full = length / I2C_DMA_LIMIT;
	int partial = length % I2C_DMA_LIMIT;
	int total;
	int last;
	int ii;
	static int msg_length;
	struct i2c_client *i2c = to_i2c_client(rmi4_data->pdev->dev.parent);

	mutex_lock(&(rmi4_data->rmi4_io_ctrl_mutex));

	if(!gprDMABuf_va){
		rmi4_data->input_dev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
	  gprDMABuf_va = (u8 *)dma_alloc_coherent(&rmi4_data->input_dev->dev, 1024, &gprDMABuf_pa, GFP_KERNEL);
	  if(!gprDMABuf_va){
		printk("[Error] Allocate DMA I2C Buffer failed!\n");
	  }
	}

	buf_va = gprDMABuf_va;

	if ((full + 2) > msg_length) {
		kfree(read_msg);
		msg_length = full + 2;
		read_msg = kcalloc(msg_length, sizeof(struct i2c_msg), GFP_KERNEL);
	}

	read_msg[0].addr = i2c->addr;
	read_msg[0].flags = 0;
	read_msg[0].len = 1;
	read_msg[0].buf = &buf;
	read_msg[0].timing = 400;

	if (partial) {
		total = full + 1;
		last = partial;
	} else {
		total = full;
		last = I2C_DMA_LIMIT;
	}

	for (ii = 1; ii <= total; ii++) {
		read_msg[ii].addr = i2c->addr;
		read_msg[ii].flags = I2C_M_RD;
		read_msg[ii].len = (ii == total) ? last : I2C_DMA_LIMIT;
		read_msg[ii].buf = gprDMABuf_pa + I2C_DMA_LIMIT * (ii - 1);
		read_msg[ii].ext_flag = (i2c->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG);
		read_msg[ii].timing = 400;
	}


	buf = addr & MASK_8BIT;

	retval = synaptics_rmi4_i2c_set_page(rmi4_data, addr);
	if (retval != PAGE_SELECT_LEN) {
		retval = -EIO;
		goto exit;
	}

	for (retry = 0; retry < SYN_I2C_RETRY_TIMES; retry++) {
		if (i2c_transfer(i2c->adapter, read_msg, (total+1) ) == (total+1)) {
			retval = length;
			break;
		}
		dev_err(rmi4_data->pdev->dev.parent,
				"%s: I2C retry %d\n",
				__func__, retry + 1);
		msleep(20);
	}

	if (retry == SYN_I2C_RETRY_TIMES) {
		dev_err(rmi4_data->pdev->dev.parent,
				"%s: I2C read over retry limit\n",
				__func__);
		retval = -EIO;
	}
	memcpy(data, buf_va, length);

exit:
	mutex_unlock(&(rmi4_data->rmi4_io_ctrl_mutex));

	return retval;
}
#endif
static int synaptics_rmi4_i2c_write(struct synaptics_rmi4_data *rmi4_data,
		unsigned short addr, unsigned char *data, unsigned short length)
{
	int retval;
	unsigned char retry;
	unsigned char buf[length + 1];
	unsigned char *buf_va = NULL;
	struct i2c_client *i2c = to_i2c_client(rmi4_data->pdev->dev.parent);
	
	mutex_lock(&(rmi4_data->rmi4_io_ctrl_mutex));
	if(!gpwDMABuf_va){
	  rmi4_data->input_dev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
	  gpwDMABuf_va = (u8 *)dma_alloc_coherent(&rmi4_data->input_dev->dev, 1024, &gpwDMABuf_pa, GFP_KERNEL);
	  if(!gpwDMABuf_va){
		printk("[Error] Allocate DMA I2C Buffer failed!\n");
	  }
	}
	buf_va = gpwDMABuf_va;

	struct i2c_msg msg[] = {
		{
			.addr = i2c->addr,
			.flags = 0,
			.len = length + 1,
			.buf = gpwDMABuf_pa,
			.ext_flag=(i2c->ext_flag|I2C_ENEXT_FLAG|I2C_DMA_FLAG),
			.timing = 400,
		}
	};

	retval = synaptics_rmi4_i2c_set_page(rmi4_data, addr);
	if (retval != PAGE_SELECT_LEN) {
		retval = -EIO;
		goto exit;
	}

	buf_va[0] = addr & MASK_8BIT;

	memcpy(&buf_va[1],&data[0] , length);

	for (retry = 0; retry < SYN_I2C_RETRY_TIMES; retry++) {
		if (i2c_transfer(i2c->adapter, msg, 1) == 1) {
			retval = length;
			break;
		}
		dev_err(rmi4_data->pdev->dev.parent,
				"%s: I2C retry %d\n",
				__func__, retry + 1);
		msleep(20);
//Gionee BSP1 yaoyc 20150317 add for test TP IC full reset CR01455491 begin
		if(retry == SYN_IC_RESET_RETRY_TIMES){
			dev_err(rmi4_data->pdev->dev.parent,
				"%s: i2c fail, so reset the IC\n",
				__func__);
			synaptics_rmi4_full_reset(rmi4_data);
		}
//Gionee BSP1 yaoyc 20150317 add for test TP IC full reset CR01455491 end
	}

	if (retry == SYN_I2C_RETRY_TIMES) {
		dev_err(rmi4_data->pdev->dev.parent,
				"%s: I2C write over retry limit\n",
				__func__);
		retval = -EIO;
	}

exit:
	mutex_unlock(&(rmi4_data->rmi4_io_ctrl_mutex));

	return retval;
}

static struct synaptics_dsx_bus_access bus_access = {
	.type = BUS_I2C,
	.read = synaptics_rmi4_i2c_read,
	.write = synaptics_rmi4_i2c_write,
};

static struct synaptics_dsx_hw_interface hw_if;

static struct platform_device *synaptics_dsx_i2c_device;

static void synaptics_rmi4_i2c_dev_release(struct device *dev)
{
	kfree(synaptics_dsx_i2c_device);

	return;
}

static int synaptics_rmi4_i2c_probe(struct i2c_client *client,
		const struct i2c_device_id *dev_id)
{
	int retval;
	printk(KERN_ERR "synaptics_rmi4_i2c_probe  enter\n");
	if (!i2c_check_functionality(client->adapter,
			I2C_FUNC_SMBUS_BYTE_DATA)) {
		dev_err(&client->dev,
				"%s: SMBus byte data commands not supported by host\n",
				__func__);
		return -EIO;
	}

	synaptics_dsx_i2c_device = kzalloc(
			sizeof(struct platform_device),
			GFP_KERNEL);
	if (!synaptics_dsx_i2c_device) {
		dev_err(&client->dev,
				"%s: Failed to allocate memory for synaptics_dsx_i2c_device\n",
				__func__);
		return -ENOMEM;
	}

	hw_if.board_data = client->dev.platform_data;

	hw_if.bus_access = &bus_access;

	synaptics_dsx_i2c_device->name = PLATFORM_DRIVER_NAME;
	synaptics_dsx_i2c_device->id = 0;
	synaptics_dsx_i2c_device->num_resources = 0;
	synaptics_dsx_i2c_device->dev.parent = &client->dev;
	synaptics_dsx_i2c_device->dev.platform_data = &hw_if;
	synaptics_dsx_i2c_device->dev.release = synaptics_rmi4_i2c_dev_release;

	retval = platform_device_register(synaptics_dsx_i2c_device);
	if (retval) {
		dev_err(&client->dev,
				"%s: Failed to register platform device\n",
				__func__);
		return -ENODEV;
	}
	printk(KERN_ERR "synaptics_rmi4_i2c_probe  exit\n");

	return 0;
}

static int synaptics_rmi4_i2c_remove(struct i2c_client *client)
{
	platform_device_unregister(synaptics_dsx_i2c_device);

	return 0;
}

static const struct i2c_device_id synaptics_rmi4_id_table[] = {
	{I2C_DRIVER_NAME, 0},
	{},
};
MODULE_DEVICE_TABLE(i2c, synaptics_rmi4_id_table);


static struct i2c_driver synaptics_rmi4_i2c_driver = {
	.driver = {
		.name = I2C_DRIVER_NAME,
		.owner = THIS_MODULE,
	},
	.probe = synaptics_rmi4_i2c_probe,
	.remove = synaptics_rmi4_i2c_remove,
	.id_table = synaptics_rmi4_id_table,
};

int synaptics_rmi4_bus_init(void)
{
	printk(KERN_ERR "synaptics_rmi4_bus_init \n");
	return i2c_add_driver(&synaptics_rmi4_i2c_driver);
}
EXPORT_SYMBOL(synaptics_rmi4_bus_init);

void synaptics_rmi4_bus_exit(void)
{
	i2c_del_driver(&synaptics_rmi4_i2c_driver);

	return;
}
EXPORT_SYMBOL(synaptics_rmi4_bus_exit);

MODULE_AUTHOR("Synaptics, Inc.");
MODULE_DESCRIPTION("Synaptics DSX I2C Bus Support Module");
MODULE_LICENSE("GPL v2");
