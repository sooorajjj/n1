/*
** =============================================================================
** Copyright (c) 2009-2014  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     Device.java
**
** Description:
**     Java Device class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

import com.immersion.ImmVibe;
import com.immersion.EffectHandle;

/**
 * Encapsulates a device handle representing an open device.
 */
public class Device
{
    protected int deviceHandle = ImmVibe.VIBE_INVALID_DEVICE_HANDLE_VALUE;
    protected int deviceIndex;

    /**
     * Opens and initializes a composite device that supports playing different
     * effects simultaneously on different actuators.
     * <p>
     * Effects may specify the index of an actuator on which to play the
     * effect. When played on a composite device, the effects are rendered on
     * the actuator corresponding to the specified actuator index.
     * <p>
     * Effects played on this device can address any of the available
     * actuators.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error opening the composite device.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>The tethered handset does not support
     *                                  this function (Windows-only).</dd>
     *                          </dl>
     */
    public static Device newDevice() throws java.lang.RuntimeException
    {
        Device device = newDeviceInstance();

        if (device != null)
        {
            device.deviceIndex = ImmVibe.VIBE_INVALID_INDEX;
            device.deviceHandle = ImmVibe.getInstance().openCompositeDevice(Device.getCount());
        }
        else
        {
            throw new java.lang.RuntimeException("VIBE_E_FAIL");
        }
        return device;
    }

    /**
     * Opens and initializes a device representing a specified actuator.
     * <p>
     * Effects played on this device can play only on the indexed actuator.
     *
     * @param   deviceIndex     Index of the actuator to use. The index of the
     *                          actuator must be greater than or equal to zero,
     *                          and less than the number of actuators returned
     *                          by {@link #getCount getCount}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getCount getCount}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error opening the device.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public static Device newDevice(int deviceIndex) throws java.lang.RuntimeException
    {
        Device device = newDeviceInstance();

        if (device != null)
        {
            device.deviceIndex = deviceIndex;
            device.deviceHandle = ImmVibe.getInstance().openDevice(deviceIndex);
        }
        else
        {
            throw new java.lang.RuntimeException("VIBE_E_FAIL");
        }
        return device;
    }

    /**
     * Closes this device.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error closing this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void close()
    {
        ImmVibe.getInstance().closeDevice(deviceHandle);
        deviceHandle = ImmVibe.VIBE_INVALID_DEVICE_HANDLE_VALUE;
        deviceIndex = ImmVibe.VIBE_INVALID_INDEX;
    }

    /**
     * Gets the number of available devices.
     * <p>
     * On platforms such as Windows that support a dynamic device configuration,
     * the list of supported devices is determined during initialization by
     * enumerating the available device drivers. The list of supported devices
     * includes attached devices but may also include devices that are not
     * physically connected. {@link #getCount getCount} returns at least one
     * device, a virtual generic device, if no physical device is present.
     * <p>
     * On embedded systems where the device configuration is static, a virtual
     * generic device is not provided. The device count corresponds with the
     * actual number of devices that are present on the embedded system.
     *
     * @return                  Number of available devices that are supported
     *                          by the API.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device count.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public static int getCount()
    {
        return ImmVibe.getInstance().getDeviceCount();
    }

    /**
     * Gets the status bits of a device.
     *
     * @param   deviceIndex     Index of the device for which to get the status
     *                          bits. The index of the device must be greater
     *                          than or equal to zero and less than the number
     *                          of devices returned by
     *                          {@link #getCount getCount}.
     * @return                  Status bits of the device, bitwise ORing of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_DEVICESTATE_ATTACHED}</li>
     *                              <li>{@link ImmVibe#VIBE_DEVICESTATE_BUSY}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getCount getCount}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device state.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public static int getState(int deviceIndex)
    {
        return ImmVibe.getInstance().getDeviceState(deviceIndex);
    }

    /**
     * Gets the status bits of this device.
     *
     * @return                  Status bits of this device, bitwise ORing of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_DEVICESTATE_ATTACHED}</li>
     *                              <li>{@link ImmVibe#VIBE_DEVICESTATE_BUSY}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device is a composite device. The
     *                                  API only supports getting the status
     *                                  bits of single-actuator devices
     *                                  created using
     *                                  {@link #newDevice(int)}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting this device's state.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getState()
    {
        return ImmVibe.getInstance().getDeviceState(deviceIndex);
    }

    /**
     * Gets a boolean capability of a device.
     * <p>
     * This function is provided for future use. The devices supported by the
     * current version of the API do not have boolean capabilities.
     *
     * @param   deviceIndex     Index of the device for which to get a boolean
     *                          capability. The index of the device must be
     *                          greater than or equal to zero and less than the
     *                          number of devices returned by
     *                          {@link #getCount getCount}.
     * @param   devCapType      Device capability type of the boolean capability
     *                          to get.
     * @return                  Requested boolean capability of the device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getCount getCount}.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a boolean capability of
     *                                  the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public static boolean getCapabilityBool(int deviceIndex, int devCapType)
    {
        return ImmVibe.getInstance().getDeviceCapabilityBool(deviceIndex, devCapType);
    }

    /**
     * Gets a boolean capability of this device.
     * <p>
     * This function is provided for future use. The devices supported by the
     * current version of the API do not have boolean capabilities.
     *
     * @param   devCapType      Device capability type of the boolean capability
     *                          to get.
     * @return                  Requested boolean capability of this device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a boolean capability of
     *                                  this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public boolean getCapabilityBool(int devCapType)
    {
        return ImmVibe.getInstance().getDeviceCapabilityBool(deviceIndex, devCapType);
    }

    /**
     * Gets a 32-bit integer capability of a device.
     *
     * @param   deviceIndex     Index of the device for which to get a boolean
     *                          capability. The index of the device must be
     *                          greater than or equal to zero and less than the
     *                          number of devices returned by
     *                          {@link #getCount getCount}.
     * @param   devCapType      Device capability type of the 32-bit integer
     *                          capability to get.
     * @return                  Requested 32-bit integer capability of the
     *                          device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getCount getCount}.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a 32-bit integer
     *                                  capability of the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public static int getCapabilityInt32(int deviceIndex, int devCapType)
    {
        return ImmVibe.getInstance().getDeviceCapabilityInt32(deviceIndex, devCapType);
    }

    /**
     * Gets a 32-bit integer capability of this device.
     *
     * @param   devCapType      Device capability type of the 32-bit integer
     *                          capability to get.
     * @return                  Requested 32-bit integer capability of this
     *                          device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a 32-bit integer
     *                                  capability of this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getCapabilityInt32(int devCapType)
    {
        return ImmVibe.getInstance().getDeviceCapabilityInt32(deviceIndex, devCapType);
    }

    /**
     * Gets a string capability of a device.
     *
     * @param   deviceIndex     Index of the device for which to get a boolean
     *                          capability. The index of the device must be
     *                          greater than or equal to zero and less than the
     *                          number of devices returned by
     *                          {@link #getCount getCount}.
     * @param   devCapType      Device capability type of the string capability
     *                          to get.
     * @return                  Requested string capability of the device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getCount getCount}.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a string capability of
     *                                  the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public static String getCapabilityString(int deviceIndex, int devCapType)
    {
        return ImmVibe.getInstance().getDeviceCapabilityString(deviceIndex, devCapType);
    }

    /**
     * Gets a string capability of this device.
     *
     * @param   devCapType      Device capability type of the string capability
     *                          to get.
     * @return                  Requested string capability of this device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a string capability of
     *                                  this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public String getCapabilityString(int devCapType)
    {
        return ImmVibe.getInstance().getDeviceCapabilityString(deviceIndex, devCapType);
    }

    /**
     * Gets a boolean property of this open device.
     *
     * @param   devPropType     Property type of the boolean property to get.
     * @return                  Requested boolean property of this device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a boolean property of this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public boolean getPropertyBool(int devPropType)
    {
        return ImmVibe.getInstance().getDevicePropertyBool(deviceHandle, devPropType);
    }

    /**
     * Sets a boolean property of this open device.
     *
     * @param   devPropType     Property type of the boolean property to set.
     * @param   devPropValue    Value of the boolean property to set.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error setting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a boolean property of this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void setPropertyBool(int devPropType, boolean devPropValue)
    {
        ImmVibe.getInstance().setDevicePropertyBool(deviceHandle, devPropType, devPropValue);
    }

    /**
     * Gets a 32-bit integer property of this open device.
     *
     * @param   devPropType     Property type of the 32-bit integer property to
     *                          get.
     * @return                  Requested 32-bit integer property of this device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a 32-bit integer property of this
     *                                  device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getPropertyInt32(int devPropType)
    {
        return ImmVibe.getInstance().getDevicePropertyInt32(deviceHandle, devPropType);
    }

    /**
     * Sets a 32-bit integer property of this open device.
     *
     * @param   devPropType     Property type of the 32-bit integer property to
     *                          set.
     * @param   devPropValue    Value of the 32-bit integer property to set.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error setting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a 32-bit integer property of this
     *                                  device.</dd>
     *                              <dt>"VIBE_E_INSUFFICIENT_PRIORITY"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies the
     *                                  {@link ImmVibe#VIBE_DEVPROPTYPE_MASTERSTRENGTH}
     *                                  property type but the maximum OEM
     *                                  priority
     *                                  ({@link ImmVibe#VIBE_MAX_OEM_DEVICE_PRIORITY})
     *                                  has not been associated with the device
     *                                  handle.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void setPropertyInt32(int devPropType, int devPropValue)
    {
        ImmVibe.getInstance().setDevicePropertyInt32(deviceHandle, devPropType, devPropValue);
    }

    /**
     * Gets a string property of this open device.
     *
     * @param   devPropType    Property type of the string property to get.
     * @return                  Requested string property of this device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a string property of this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public String getPropertyString(int devPropType)
    {
        return ImmVibe.getInstance().getDevicePropertyString(deviceHandle, devPropType);
    }

    /**
     * Sets a string property of this open device.
     *
     * @param   devPropType     Property type of the string property to set.
     * @param   devPropValue    Value of the string property to set.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The <code>devPropType</code> parameter
     *                                          specifies the
     *                                          {@link ImmVibe#VIBE_DEVPROPTYPE_LICENSE_KEY}
     *                                          property type and the given
     *                                          license key is invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error setting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a string property of this device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void setPropertyString(int devPropType, String devPropValue)
    {
        ImmVibe.getInstance().setDevicePropertyString(deviceHandle, devPropType, devPropValue);
    }

    /**
     * Plays an effect defined in IVT data.
     *
     * @param   ivt             IVT buffer containing the definition of the
     *                          effect to play. Use
     *                          {@link IVTBuffer#getBuiltInEffects IVTBuffer.getBuiltInEffects}
     *                          to access built-in IVT effects.
     * @param   effectIndex     Index of the effect to play. The index of the
     *                          effect must be greater than or equal to zero and
     *                          less than the number of effects returned by
     *                          {@link IVTBuffer#getEffectCount IVTBuffer.getEffectCount}.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link IVTBuffer#getEffectCount IVTBuffer.getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public EffectHandle playIVTEffect(IVTBuffer ivt, int effectIndex)
    {
        return newEffectHandle(deviceHandle, ImmVibe.getInstance().playIVTEffect(deviceHandle, ivt.getBuffer(), effectIndex));
    }

    /**
     * Plays an interpolated effect defined in IVT data.
     *
     * @param   ivt             IVT buffer containing the definition of the
     *                          effect to play. Use
     *                          {@link IVTBuffer#getBuiltInEffects IVTBuffer.getBuiltInEffects}
     *                          to access built-in IVT effects.
     * @param   effectIndex     Index of the effect to play. The index of the
     *                          effect must be greater than or equal to zero and
     *                          less than the number of effects returned by
     *                          {@link IVTBuffer#getEffectCount IVTBuffer.getEffectCount}.
     * @param   interpolant     Initial interpolant value for the interpolated effect.
     *                          The interpolant value must be greater than or equal to 0 and
     *                          less than or equal to VIBE_MAX_INTERPOLANT.
     *                          The interpolant value may be subsequently modified by calling
     *                          {@link EffectHandle#modifyPlayingInterpolatedEffectInterpolant}.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link IVTBuffer#getEffectCount IVTBuffer.getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public EffectHandle playIVTInterpolatedEffect(IVTBuffer ivt, int effectIndex, int interpolant)
    {
        return newEffectHandle(deviceHandle, ImmVibe.getInstance().playIVTInterpolatedEffect(deviceHandle, ivt.getBuffer(), effectIndex, interpolant));
    }

    /**
     * Repeatedly plays a Timeline effect defined in IVT data.
     * <p>
     * This function repeats only Timeline effects. If the given effect index
     * refers to a simple effect,
     * {@link #playIVTEffectRepeat playIVTEffectRepeat} ignores the
     * <code>repeat</code> parameter and plays the simple effect once. In that
     * case, {@link #playIVTEffectRepeat playIVTEffectRepeat} behaves like
     * {@link #playIVTEffect playIVTEffect}. To determine the type of an IVT
     * effect, call {@link IVTBuffer#getEffectType IVTBuffer.getEffectType}.
     *
     * @param   ivt             IVT buffer containing the definition of the
     *                          effect to play. Use
     *                          {@link IVTBuffer#getBuiltInEffects IVTBuffer.getBuiltInEffects}
     *                          to access built-in IVT effects.
     * @param   effectIndex     Index of the effect to play. The index of the
     *                          effect must be greater than or equal to zero and
     *                          less than the number of effects returned by
     *                          {@link IVTBuffer#getEffectCount IVTBuffer.getEffectCount}.
     * @param   repeat          Number of times to repeat the effect. To play
     *                          the effect indefinitely, set
     *                          <code>repeat</code> to
     *                          {@link ImmVibe#VIBE_REPEAT_COUNT_INFINITE}.
     *                          To repeat the effect a finite number of times,
     *                          set <code>repeat</code> to a value from zero to
     *                          {@link ImmVibe#VIBE_REPEAT_COUNT_INFINITE}<code>&nbsp;-&nbsp;1</code>.
     *                          Setting <code>repeat</code> to zero plays the
     *                          effect once (repeats the effect zero times) and
     *                          is equivalent to calling
     *                          {@link #playIVTEffect playIVTEffect}. To stop
     *                          the effect before it has repeated the requested
     *                          number of times, or to stop an effect that is
     *                          playing indefinitely, call
     *                          {@link EffectHandle#stop EffectHandle.stop} or
     *                          {@link #stopAllPlayingEffects stopAllPlayingEffects}.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link IVTBuffer#getEffectCount IVTBuffer.getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public EffectHandle playIVTEffectRepeat(IVTBuffer ivt, int effectIndex, byte repeat)
    {
        return newEffectHandle(deviceHandle, ImmVibe.getInstance().playIVTEffectRepeat(deviceHandle, ivt.getBuffer(), effectIndex, repeat));
    }

    /**
     * Plays a MagSweep effect given the definition of the effect.
     *
     * @param   definition      Definition of the effect parameters.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public EffectHandle playMagSweepEffect(MagSweepEffectDefinition definition)
    {
        return newEffectHandle(deviceHandle, ImmVibe.getInstance().playMagSweepEffect(deviceHandle,
                                                                                      definition.getDuration(),
                                                                                      definition.getMagnitude(),
                                                                                      definition.getStyle(),
                                                                                      definition.getAttackTime(),
                                                                                      definition.getAttackLevel(),
                                                                                      definition.getFadeTime(),
                                                                                      definition.getFadeLevel()));
    }

    /**
     * Plays a Periodic effect given the parameters defining the effect.
     *
     * @param   definition      Definition of the effect parameters.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public EffectHandle playPeriodicEffect(PeriodicEffectDefinition definition)
    {
        return newEffectHandle(deviceHandle, ImmVibe.getInstance().playPeriodicEffect(deviceHandle,
                                                                                      definition.getDuration(),
                                                                                      definition.getMagnitude(),
                                                                                      definition.getPeriod(),
                                                                                      definition.getStyleAndWaveType(),
                                                                                      definition.getAttackTime(),
                                                                                      definition.getAttackLevel(),
                                                                                      definition.getFadeTime(),
                                                                                      definition.getFadeLevel()));
    }

    /**
     * Plays a Waveform effect given the parameters defining the effect.
     * <p>
     * This function is supported only in the 5000 API edition. To get the
     * API edition level, call
     * {@link #getCapabilityInt32 getCapabilityInt32} with the
     * {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   definition      Definition of the effect parameters.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link #getCapabilityInt32 getCapabilityInt32}
     *                                  with the
     *                                  {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public EffectHandle playWaveformEffect(WaveformEffectDefinition definition)
    {
         return newEffectHandle(deviceHandle, ImmVibe.getInstance().playWaveformEffect(deviceHandle,
                                                                                       definition.getData(),
                                                                                       definition.getDataSize(),
                                                                                       definition.getSampleRate(),
                                                                                       definition.getBitDepth(),
                                                                                       definition.getMagnitude()));
    }

    /**
     * Creates a Streaming effect.
     * <p>
     * Call {@link #createStreamingEffect createStreamingEffect} to create a new
     * Streaming effect and get a handle to the new effect. Use that effect
     * handle to play Streaming Samples by calling
     * {@link EffectHandle#playStreamingSample EffectHandle.playStreamingSample} or
     * {@link EffectHandle#playStreamingSampleWithOffset EffectHandle.playStreamingSampleWithOffset}.
     *
     * @return                  Handle to the created effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</li>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error creating the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public EffectHandle createStreamingEffect()
    {
        return newEffectHandle(deviceHandle, ImmVibe.getInstance().createStreamingEffect(deviceHandle));
    }

    /**
     * Stops all playing and paused effects on a device.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This device has been closed by a
     *                                  previous call to
     *                                  {@link #close close}.</li>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error stopping all effects.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void stopAllPlayingEffects()
    {
        ImmVibe.getInstance().stopAllPlayingEffects(deviceHandle);
    }

    /**
     * Plays an Enhanced Waveform effect given the parameters defining the effect.
     * <p>
     * This function is supported only with Enhanced Waveform feature. To get the
     * the supported feature of the TouchSense Player, call
     * {@link ImmVibeAPI#getCapabilityInt32 getCapabilityInt32} with the
     * {@link ImmVibeAPI#VIBE_DEVCAPTYPE_SUPPORTED_FEATURES} device capability type.
     *
     * @param   definition      Definition of the effect parameters.
     * @return                  Handle to the playing effect.
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link #getCapabilityInt32 getCapabilityInt32}
     *                                  with the
     *                                  {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public EffectHandle playEnhancedWaveformEffect(EnhancedWaveformEffectDefinition definition)
    {
         return newEffectHandle(deviceHandle, ImmVibe.getInstance().playEnhancedWaveformEffect(deviceHandle,
                                                                                       definition.getData(),
                                                                                       definition.getSampleRate(),
                                                                                       definition.getFormat(),
                                                                                       definition.getMagnitude(),
                                                                                       definition.getSecureMode()));
    }
     
     /**
     * Gets an IVTBuffer from an XIVT buffer
     *
     * @param   xivt            A byte array containing valid XIVT data.
     * @return                  An instance of the IVTBuffer class.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This device has been closed by a
     *                                          previous call to
     *                                          {@link #close close}.</li>
     *                                      <li>The <code>xivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          XIVT data.</li>
     *                                  </ul>
     *                              </dd>
     *                          </dl>
     */
    public IVTBuffer getIVTBufferFromXIVT(byte xivt[])
    {
        return new IVTBuffer(ImmVibe.getInstance().loadIVTFromXIVT(deviceHandle, xivt));
    }

    /**
     * Protected constructor
     */
    protected Device()
    {
    }

    /**
     * Returns an instance of EffectHandle class
     */
    protected EffectHandle newEffectHandle(int deviceHandle, int effectHandle)
    {
        return new EffectHandle(deviceHandle, effectHandle);
    }

    /**
     * Returns an instance of a class by specifying its package name.
     */
    private static Device newDeviceInstanceForName(String deviceClassName)
    {
        Device device = null;

        try
        {
            Class t = Class.forName(deviceClassName);

            device = (Device)t.newInstance();
        }
        catch(Exception e)
        {
            // Ignore ClassNotFoundException - return null instead.
        }
        return device;
    }

    /**
     * Returns an appropriate instance of a Device class for the active platform.
     */
    private static Device newDeviceInstance()
    {
        Device device = null;

        device = newDeviceInstanceForName("com.immersion.android.Device");

        if (device == null)
        {
            device = newDeviceInstanceForName("com.immersion.J2ME.Device");

            if (device == null)
            {
            	device = new Device();
            }
        }
        return device;
    }
}
