/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     IVTMagSweepElement.java
**
** Description:
**     Java IVTMagSweepElement class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Represents a MagSweep effect within a Timeline effect in an IVT buffer.
 */
public class IVTMagSweepElement extends IVTElement
{
    private MagSweepEffectDefinition definition;

    /**
     * Initializes this IVT MagSweep element.
     *
     * @param   time            Time in milliseconds of this IVT MagSweep
     *                          element within the Timeline effect.
     * @param   definition      Definition of this IVT MagSweep element.
     */
    public IVTMagSweepElement(int time, MagSweepEffectDefinition definition)
    {
        super(ImmVibeAPI.VIBE_ELEMTYPE_MAGSWEEP, time);
        this.definition = definition;
    }

    /**
     * Sets the definition of this IVT MagSweep element.
     *
     * @param   definition      Definition of this IVT MagSweep element.
     */
    public void setDefinition(MagSweepEffectDefinition definition)
    {
        this.definition = definition;
    }

    /**
     * Gets the definition of this IVT MagSweep element.
     *
     * @return                  Definition of this IVT MagSweep element.
     */
    public MagSweepEffectDefinition getDefinition()
    {
        return definition;
    }

    public int[] getBuffer()
    {
        int[] retVal = new int[10];

        retVal[0] = getType();
        retVal[1] = getTime();
        retVal[2] = definition.getDuration();
        retVal[3] = definition.getMagnitude();
        retVal[4] = definition.getStyle();
        retVal[5] = definition.getAttackTime();
        retVal[6] = definition.getAttackLevel();
        retVal[7] = definition.getFadeTime();
        retVal[8] = definition.getFadeLevel();
        retVal[9] = definition.getActuatorIndex();

        return retVal;
    }
}
