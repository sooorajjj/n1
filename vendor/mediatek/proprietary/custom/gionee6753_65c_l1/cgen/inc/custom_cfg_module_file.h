#include "../cfgfileinc/CFG_Custom1_File.h"
#include "../cfgfileinc/CFG_GPS_File.h"
#include "../cfgfileinc/CFG_PRODUCT_INFO_File.h"
#include "../cfgfileinc/CFG_Wifi_File.h"
/* Gionee yang_yang 20150313 add for CR01454126 begin */
#if defined(CONFIG_GN_BSP_PS_STATIC_CALIBRATION)
#include "../cfgfileinc/CFG_PS_Cali_File.h"
#endif
/* Gionee yang_yang 20150313 add for CR01454126 end */