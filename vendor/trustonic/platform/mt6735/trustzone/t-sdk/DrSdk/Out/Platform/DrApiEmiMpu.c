/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "DrApi/DrApi.h"
#include "DrApi/DrApiPlat.h"

/* EMI memory protection align 64K */
#define EMI_MPU_ALIGNMENT   0x10000
#define EMI_PHY_OFFSET      0x40000000
#define SEC_PHY_SIZE        0x06000000

#define IOMEM(reg) (reg)

unsigned char *emi_mpu_va_buffer;
#define EMP_MPU_VA_BUFFER   emi_mpu_va_buffer
#define EMI_BASE            (0x10203000)
#define EMI_VA_MPUJ         ((volatile unsigned int*)(EMP_MPU_VA_BUFFER+0x01A8))
#define EMI_VA_MPUJ_2ND     ((volatile unsigned int*)(EMP_MPU_VA_BUFFER+0x01AC))
#define EMI_VA_MPUC         ((volatile unsigned int*)(EMP_MPU_VA_BUFFER+0x0170))
#define EMI_VA_MPUD         ((volatile unsigned int*)(EMP_MPU_VA_BUFFER+0x0178))

inline unsigned int readl(volatile unsigned int* addr)
{
    return *( volatile unsigned int*)addr;
}

inline void writel(unsigned int val, volatile unsigned int* addr)
{
    *(volatile unsigned int*)addr = val;
}

/*
 * emi_mpu_set_region_protection: protect a region.
 * @start: start address of the region
 * @end: end address of the region
 * @region: EMI MPU region id
 * @access_permission: EMI MPU access permission
 * Return 0 for success, otherwise negative status code.
 */
int emi_mpu_set_region_protection(unsigned int start, unsigned int end, int region, unsigned int access_permission)
{
    int ret = 0;
    unsigned int tmp, tmp2;
    unsigned int ax_pm, ax_pm2;
        
    if((end != 0) || (start !=0)) 
    {
        /*Address 64KB alignment*/
        start -= EMI_PHY_OFFSET;
        end -= EMI_PHY_OFFSET;
        start = start >> 16;
        end = end >> 16;

        if (end <= start) 
        {
            return -1;
        }
    }

    //map register set
    if((ret = drApiMapPhysicalBuffer((uint64_t)EMI_BASE, SIZE_4KB, MAP_HARDWARE, (void **)&EMP_MPU_VA_BUFFER)) != DRAPI_OK){
        drApiLogPrintf("[emi_mpu_set_region_protection] map EMI_BASE failed! ERROR: %d\n", ret);
        return -2;
    }

    //set EMI MPU protection
    ax_pm  = (access_permission << 16) >> 16;
    ax_pm2 = (access_permission >> 16);   
    
    switch (region) {        
    case 2:
        tmp = readl(IOMEM(EMI_VA_MPUJ)) & 0xFFFF0000;
        tmp2 = readl(IOMEM(EMI_VA_MPUJ_2ND)) & 0xFFFF0000;
        writel(0, EMI_VA_MPUJ);
        writel(0, EMI_VA_MPUJ_2ND);
        writel((start << 16) | end, EMI_VA_MPUC); 
        writel(tmp2 | ax_pm2, EMI_VA_MPUJ_2ND);
        writel(tmp | ax_pm, EMI_VA_MPUJ);
        break;       
    case 3:
        tmp = readl(IOMEM(EMI_VA_MPUJ)) & 0x0000FFFF;
        tmp2 = readl(IOMEM(EMI_VA_MPUJ_2ND)) & 0x0000FFFF;
        writel(0, EMI_VA_MPUJ);
        writel(0, EMI_VA_MPUJ_2ND);
        writel((start << 16) | end, EMI_VA_MPUD);
        writel(tmp2 | (ax_pm2 << 16), EMI_VA_MPUJ_2ND);
        writel(tmp | (ax_pm << 16), EMI_VA_MPUJ);
        break;    
    default:
        drApiLogPrintf("[emi_mpu_set_region_protection] region %d is not supported\n", region);
        ret = -1;
        break;
    }

    //unmap register set
    drApiUnmapBuffer(EMP_MPU_VA_BUFFER);

    return ret;
}

