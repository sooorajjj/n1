; ------------------------------------------------------------------------
; Copyright (c) 2013 TRUSTONIC LIMITED
; All rights reserved
;
; The present software is the confidential and proprietary information of
; TRUSTONIC LIMITED. You shall not disclose the present software and shall
; use it only in accordance with the terms of the license agreement you
; entered into with TRUSTONIC LIMITED. This software may be subject to
; export or import laws in certain countries.
; ------------------------------------------------------------------------

TIMER_DRV_IRQ   EQU             0x23
TIMER_LOAD      EQU             0x20     ; ACVR rw
TIMER_VALUE     EQU             0x24     ; ACVR ro 
TIMER_CTRL      EQU             0x28     ; ACVR rw 
TIMER_INTCLR    EQU             0x2c     ; ACVR wo 

TIMER_CTRL_ONESHOT EQU          0x01     ;1 << 0   CVR
TIMER_CTRL_32BIT EQU            0x02     ;1 << 1   CVR
TIMER_CTRL_DIV1 EQU             0x00     ;0 << 2  ACVR
TIMER_CTRL_DIV16 EQU            0x04     ;1 << 2  ACVR
TIMER_CTRL_DIV256 EQU           0x08     ;2 << 2  ACVR
TIMER_CTRL_IE EQU               0x20     ;1 << 5    VR
TIMER_CTRL_PERIODIC EQU         0x40     ;1 << 6  ACVR
TIMER_CTRL_ENABLE EQU           0x80     ;1 << 7  ACVR

TIMER_CTRL_INIT_VALUE  EQU              TIMER_CTRL_32BIT \
                                :OR:    TIMER_CTRL_ENABLE \
                                :OR:    TIMER_CTRL_PERIODIC \
                                :OR:    TIMER_CTRL_IE \
                                :OR:    TIMER_CTRL_ONESHOT

        ARM
        REQUIRE8
        PRESERVE8

        AREA ||.text||, CODE, READONLY, ALIGN=2

plat_getTimerBase PROC
        LDR      r0,TIMER_BASE_PHYS
        BX       lr
        ENDP

plat_getIrqNumber PROC
        MOV      r0,#TIMER_DRV_IRQ
        BX       lr
        ENDP

plat_timerInit PROC
        LDR      r1,DATA_AREA
        STR      r0,[r1,#0]  ; virt_timer_base
        MOV      r1,#0
        STR      r1,[r0,#TIMER_CTRL]
        MVN      r1,#0
        STR      r1,[r0,#TIMER_LOAD]
        STR      r1,[r0,#TIMER_VALUE]
        MOV      r1,#TIMER_CTRL_INIT_VALUE
        STR      r1,[r0,#TIMER_CTRL]
        LDR      r1,LOAD_VALUE
        STR      r1,[r0,#TIMER_LOAD]
        BX       lr
        ENDP

plat_timerShutdown PROC
        LDR      r0,DATA_AREA
        MOV      r1,#0x20
        LDR      r0,[r0,#0]  ; virt_timer_base
        STR      r1,[r0,#TIMER_CTRL]
        MVN      r1,#0
        STR      r1,[r0,#TIMER_LOAD]
        STR      r1,[r0,#TIMER_VALUE]
        BX       lr
        ENDP

plat_clearInterrupt PROC
        LDR      r0,DATA_AREA
        MOV      r1,#1
        LDR      r0,[r0,#0]  ; virt_timer_base
        STR      r1,[r0,#TIMER_INTCLR]
        LDR      r1,LOAD_VALUE
        STR      r1,[r0,#TIMER_LOAD]
        BX       lr
        ENDP

TIMER_BASE_PHYS
        DCD      0x10012000
DATA_AREA
        DCD      ||.data||
;#if CORTEXA9_SHAPE==VE
#ifdef  __ARMCC__
LOAD_VALUE
        DCD      TIMER_LOAD_VALUE       ;Timer counter load value
#endif
        AREA ||.data||, DATA, ALIGN=2
virt_timer_base
        DCD      0x00000000

        EXPORT plat_getTimerBase [CODE]
        EXPORT plat_getIrqNumber [CODE]
        EXPORT plat_timerInit [CODE]
        EXPORT plat_timerShutdown [CODE]
        EXPORT plat_clearInterrupt [CODE]

        END
