#include "drStd.h"
#include "DrApi/DrApi.h"
#include "drError.h"

void __aeabi_memclr(void *dest, size_t n)
{
    memset(dest, 0, n);
}

void __aeabi_memclr4(void *dest, size_t n)
{
    memset(dest, 0, n);
}

void __aeabi_memset(void *dest, size_t n, int c)
{
    memset(dest, c, n);
}

void __aeabi_memset4(void *dest, size_t n, int c)
{
    memset(dest, c, n);
}

void __aeabi_memmove(void *dest, const void *src, size_t n)
{
    memmove(dest, src, n);
}

void __aeabi_memmove4(void *dest, const void *src, size_t n)
{
    memmove(dest, src, n);
}

void __aeabi_memcpy (void *dest, const void *src, size_t n)
{
  memcpy (dest, src, n);
}

void __aeabi_memcpy4 (void *dest, const void *src, size_t n)
{
  memcpy (dest, src, n);
}
