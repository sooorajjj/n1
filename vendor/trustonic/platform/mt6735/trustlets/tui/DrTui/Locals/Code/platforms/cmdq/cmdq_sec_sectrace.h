#ifndef __CMDQ_SEC_SECTRACE_H__
#define __CMDQ_SEC_SECTRACE_H__

/* IPC handler for sectrace related requests from cmdqSecTl */
int32_t cmdq_tz_sectrace_map(unsigned long pa, unsigned long size);
int32_t cmdq_tz_sectrace_unmap(void);
void cmdq_tz_sectrace_transact(void);

/* profile APIs */
void cmdq_tz_sectrace_profile_start(const char *tag);
void cmdq_tz_sectrace_profile_end(const char *tag);
void cmdq_tz_sectrace_profile_oneshot(const char *tag);

#endif				/* __CMDQ_SEC_SECTRACE_H__ */
