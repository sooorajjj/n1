################################################################################
#
# <t-sdk widevine classic drm decrypt Trustlet
#
################################################################################


# output binary name without path or extension
OUTPUT_NAME := tlWidevineClassicDrmDecrypt


#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------

TRUSTLET_UUID := 37c5b301175a4b81b58cb980e01de5fa
TRUSTLET_MEMTYPE := 2
TRUSTLET_NO_OF_THREADS := 1
TRUSTLET_SERVICE_TYPE := 3 # 2: service provider trustlet; 3: system trustlet
TRUSTLET_KEYFILE := pairVendorTltSig.pem #only valid for service provider trustlets.
TRUSTLET_FLAGS := 0
TRUSTLET_INSTANCES := 10

#-------------------------------------------------------------------------------
# For 302A and later version
#-------------------------------------------------------------------------------
TBASE_API_LEVEL := 5
#4K
HEAP_SIZE_INIT := 4096
#10M
HEAP_SIZE_MAX := 10485760

#-------------------------------------------------------------------------------
# use generic make file
TRUSTLET_DIR ?= Locals/Code
TLSDK_DIR_SRC ?= $(TLSDK_DIR)
include $(TLSDK_DIR)/trustlet_release.mk
