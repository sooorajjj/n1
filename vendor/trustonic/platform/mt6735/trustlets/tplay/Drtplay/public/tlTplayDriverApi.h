/*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

/*
 * @file   tlDriverApi.h
 * @brief  Contains trustlet API definitions
 *
 */

#ifndef __TL_M_TPLAY_DRIVER_API_H__
#define __TL_M_TPLAY_DRIVER_API_H__

#include "tlStd.h"
#include "TlApi/TlApiError.h"
//#include "TlApi/TPlay.h"
#include "tlApitplay.h"


_TLAPI_EXTERN_C int32_t exDrmApi_AES128CTR_Init(/*OUT*/int32_t *pHandle, const uint8_t *pKey, uint32_t key_len);
_TLAPI_EXTERN_C int32_t exDrmApi_AES128CTR_DecryptData(int32_t Handle, 
    const void *pSrc, void *pDst, uint32_t dataSize, /*INOUT*/ uint8_t iv[16], uint32_t aesSrcOffset, uint32_t bufOffset, EX_MEM_t DstMemType);
_TLAPI_EXTERN_C int32_t exDrmApi_AES128CTR_EncryptData(int32_t Handle, 
    const void *pSrc, void *pDst, uint32_t dataSize, /*INOUT*/ uint8_t iv[16], uint32_t bufOffset, EX_MEM_t SrcMemType);
_TLAPI_EXTERN_C int32_t exDrmApi_AES128CTR_Finish(int32_t Handle);
_TLAPI_EXTERN_C int32_t exDrmApi_AES128CBC_Init(/*OUT*/int32_t *pHandle, const uint8_t *pKey, uint32_t key_len);
_TLAPI_EXTERN_C int32_t exDrmApi_AES128CBC_DecryptData(int32_t Handle, 
    const void *pSrc, void *pDst, uint32_t dataSize, /*INOUT*/ uint8_t iv[16], uint32_t aesSrcOffset, uint32_t bufOffset, EX_MEM_t DstMemType);
_TLAPI_EXTERN_C int32_t exDrmApi_AES128CBC_Finish(int32_t Handle);
_TLAPI_EXTERN_C int32_t exDrmApi_CopyData( const void *pSrc, void *pDst, uint32_t dataSize, uint32_t bufOffset, EX_MEM_t DstMemType);
_TLAPI_EXTERN_C int32_t tlDrmApi_setHandleAddr( uint32_t handle_low_addr, uint32_t handle_high_addr );
_TLAPI_EXTERN_C int32_t tlDrmApi_dumpPhyAddr();
_TLAPI_EXTERN_C int32_t tlDebugApiGetDecryptionResult(/*OUT*/ uint8_t *pbOutBuffer, /*IN*/ uint32_t cbOutBufferLen, /*IN*/ uint32_t cbOutputOffset);

/** a single flag variable (32-bit) in the secured display driver.
 *  sets bit in the flag variable.
 *
 * @param bitToSet the bit need to be set
 */
_TLAPI_EXTERN_C tlApiResult_t exDrmApi_setLinkFlag(uint32_t bitToSet);

/** a single flag variable (32-bit) in the secured display driver.
 *  clears specific bit in the flag variable.
 *
 * @param bitToSet the bit need to be cleared.
 */
_TLAPI_EXTERN_C tlApiResult_t exDrmApi_clearLinkFlag(uint32_t bitToClear);


/** a single flag variable (32-bit) in the secured display driver.
 *  queries a single bit in the flag variable.
 *
 * @param bitToSet the bit need to be queried.
 * @return the value of the queried bit.
 */
_TLAPI_EXTERN_C uint32_t exDrmApi_testLinkFlag(uint32_t bitToTest);

#if 0
_TLAPI_EXTERN_C int32_t _tlDrmApi_processDrmContent_DONT_USE (
    uint8_t                             sHandle,
    tlApiDrmDecryptContext_t            decryptCtx,
    uint8_t                             *input,
    tlApiDrmInputSegmentDescriptor_t    inputDesc,
    uint16_t                            processMode,     
    uint8_t                             *rfu); 
#endif

#endif // __TLDRIVERAPI_H__
