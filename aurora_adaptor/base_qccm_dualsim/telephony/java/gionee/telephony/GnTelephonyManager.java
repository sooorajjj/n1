package gionee.telephony;

import android.content.Context;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telephony.PhoneStateListener;
import android.telephony.MSimTelephonyManager;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.TelephonyProperties;
// Gionee:wangth 20130228 add for CR00773823 begin
import java.io.IOException;
// Gionee:wangth 20130228 add for CR00773823 end
import android.util.Log;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.msim.ITelephonyMSim;
//import com.android.internal.telephony.IVideoTelephony;
import android.os.ServiceManager;
import com.codeaurora.telephony.msim.SubscriptionManager;
import com.codeaurora.telephony.msim.Subscription;

public class GnTelephonyManager{
    //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 begin
    /** SIM card state: SIM Card Deactivated, only the sim card is activated,
     *   we can get the other sim card state(eg:ready, pin lock...)
     *@hide
     */
     public static int SIM_STATE_DEACTIVATED = 0x0A;
   //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 end
     
     // Gionee:wangth 20130228 add for CR00773823 begin
//   private static QcNvItems mQcNvItems = new QcNvItems();
     // Gionee:wangth 20130228 add for CR00773823 end

    private static GnTelephonyManager mInstance = new GnTelephonyManager();
    //Gionee guoyx 20130218 add for CR00766605 begin
    //This is use for Quclomm solution if in the MTK solution return the value false. 
    public static boolean isMultiSimEnabled() {
        return MSimTelephonyManager.getDefault().isMultiSimEnabled();
    }
	//Gionee guoyx 20130218 add for CR00766605 end
    
    public static GnTelephonyManager getDefault() {
        return mInstance;
    }

    public static boolean hasIccCardGemini(int subscription) {
        return telManager.hasIccCard(subscription);
    }
    
    private static MSimTelephonyManager telManager = MSimTelephonyManager.getDefault();

    public static String getLine1Number() {
        return telManager.getLine1Number(telManager.getDefaultSubscription());
    }

    public static boolean hasIccCard() {
        return telManager.hasIccCard(telManager.getDefaultSubscription());
    }

    public static int getDataState() {
        return telManager.getDataState();
    }

    public static String getLine1NumberGemini(int slotId) {
//        return MmsApp.getApplication().getTelephonyManager().getLine1NumberGemini(slotId);
        return telManager.getLine1Number(slotId);
    }
    
    public static boolean isNetworkRoamingGemini(int slotId) {
        return telManager.isNetworkRoaming(slotId);
    }

    public static int getDataStateGemini(int simId) {
        try {
            return getDataState();
        } catch (Exception ex) {
            // the phone process is restarting.
            return TelephonyManager.DATA_DISCONNECTED;
        }
    }
    
    public static void listenGemini(PhoneStateListener listener, int state, int simId) {
//        ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE))
//                .listenGemini(listener, state, simId);
      //Gionee guoyx 20130201 add for MTK-QC compile pass CR00766605 begin
 //       listener.SetListenerSub(simId);
      //Gionee guoyx 20130201 add for MTK-QC compile pass CR00766605 end
        listenGemini(listener, state);
    }
    
    public static void listenGemini(PhoneStateListener listener, int state) {
        telManager.listen(listener,state);
    }
    
    public static int getCallStateGemini(int subscription) {
        return telManager.getCallState(subscription);
    }

    public static int getSimStateGemini(int currentSlotId) {
        return telManager.getSimState(currentSlotId);
    }

    //--------add for contacts--------
    private static String mResultStr = "iuni";
    
    public static String getVoiceMailNumberGemini(int slotId) {
        return mResultStr;
    }
    
    public static String getDeviceIdGemini(int simId) {
        return telManager.getDeviceId(simId);//mResultStr; //guoyx 20130116
    }
    
    public static String getSN(Context context) {
        // Gionee:wangth 20130228 modify for CR00773823 
/*        QcNvItems mQcNvItems = new QcNvItems(context);
        String factoryResult = mResultStr;
        try {
            factoryResult = mQcNvItems.getFactoryResult();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
   */
       String factoryResult="iuni";    
       factoryResult=SystemProperties.get("persist.sys.aurora.device.sn",factoryResult);     
        return factoryResult;
       // return null;
        // Gionee:wangth 20130228 modify for CR00773823 end
    }
    public static String getSN()
   {
       String factoryResult="iuni";    
       factoryResult=SystemProperties.get("persist.sys.aurora.device.sn", factoryResult);     
        return factoryResult;    
   }

 
    public static String getNetworkOperatorGemini(int slotId) {
        return telManager.getNetworkOperator(slotId);
    }

    public static String getSubscriberIdGemini(int slotId) {
        return telManager.getSubscriberId(slotId);
    }
    
    public static String getSimOperatorGemini(int slotId) {
        return telManager.getSimOperator(slotId);
    }

    public static boolean isValidSimState(int slotId) {
        return false;//telManager.isValidSimState();
    }
    
    public static int getIccSubSize(int slotId) {
        return 0;//TelephonyManager.getDefault().getIccSubSize();
    }

    public static int getSIMStateIcon(int simStatus) {
        return -1;
    }
    
    // Gionee:wangth 20130329 add for CR00791133 begin
    public static boolean isVTCallActive() {
        boolean isVTActive = false;
        
/*        try {
            if (isMultiSimEnabled()) {
                IVideoTelephony vtCall = IVideoTelephony.Stub.asInterface(ServiceManager
                        .checkService("videophone"));

                if (vtCall != null) {
                    if (!vtCall.isVtIdle()) {
                        isVTActive = true;
                    }
                }
            }
        } catch (RemoteException e) {
            Log.w("GnTelephonyManager", "isVTActive() failed", e);
        }
*/
        return isVTActive;
    }
    
    public static void showCallScreenWithDialpad(boolean show) {
        Log.w("GnTelephonyManager", "   show. " + show);
        ITelephonyMSim telephonyServiceMSim = ITelephonyMSim.Stub
                .asInterface(ServiceManager.checkService("phone_msim"));
        if (telephonyServiceMSim == null) {
            Log.w("GnTelephonyManager", "Unable to find ITelephony interface.");
            return;
        } else {
            try {
                telephonyServiceMSim.showCallScreenWithDialpad(show);
            } catch (RemoteException e) {
                Log.w("GnTelephonyManager", "phone.showCallScreenWithDialpad() failed", e);
            }
        }
    }
    // Gionee:wangth 20130329 add for CR00791133 end

//aurora add zhouxiaobing 20131115 start
public static int getNetworkTypeGemini(int simId)
{

    return telManager.getNetworkType(simId);
}


//aurora add zhouxiaobing 20131115 end
//aurora add zhouxiaobing 20140521 start
public static int getPhoneCount()
{
	
   return telManager.getPhoneCount();	
}
//aurora add zhouxiaobing 20140521 end

	public static int getDefaultSubscription(Context context) {
		return telManager.getDefaultSubscription();
	}

	public static int getPreferredDataSubscription(Context context) {
        String prop = SystemProperties.get("ro.gn.gnprojectid");
    	if(prop.contains("8910")) {
    		return 0;
    	} else {
    		return telManager.getPreferredDataSubscription();
    	}
	}

	public static String getNetworkOperatorNameGemini(int simId) {
		return telManager.getNetworkOperatorName(simId);
	}

	public static boolean isSimClosed(Context context, int slot) {
		try {
			int simstate = android.provider.Settings.Global.getInt(context.getContentResolver(), "mobile_data" + 2);
			return ((slot + 1) & simstate) == 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

} 
