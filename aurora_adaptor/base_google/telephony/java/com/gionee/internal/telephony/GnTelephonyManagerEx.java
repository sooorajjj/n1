package com.gionee.internal.telephony;

// gionee gaoj 2011-12-1 added for 8255 start
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.internal.telephony.ITelephony;
// gionee gaoj 2011-12-1 added for 8255 end
//import com.android.internal.telephony.msim.ITelephony;
import android.telephony.TelephonyManager;
import android.telephony.TelephonyManager;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.os.AsyncResult;
import android.os.Looper;
import com.android.internal.telephony.PhoneFactory;
import android.util.Log;
import android.os.Handler;
import android.os.HandlerThread;

import android.os.Message;
import com.android.internal.telephony.Phone;
import java.lang.reflect.Method;


public class GnTelephonyManagerEx {

    private static GnTelephonyManagerEx mInstance = new GnTelephonyManagerEx();
    
    public static GnTelephonyManagerEx getDefault() {
        // TODO Auto-generated method stub
        return mInstance;
    }

    public int getSimIndicatorStateGemini(int slotId) {
        // TODO Auto-generated method stub
        return getSimState(0); 
    }
    
    private ITelephony getITelephony() {
        return ITelephony.Stub.asInterface(ServiceManager.getService(Context.TELEPHONY_SERVICE));
    }
    // gionee gaoj 2011-12-1 added for 8255 end
	public String getScAddress(int slotId) {
    
/*	try {
				  return null;//getITelephony().getScAddressGemini(slotId);
			  } catch(RemoteException e1) {
				  return null;
			  } catch(NullPointerException e2) {
				  return null;
			  }
*/
      return null;
	}

    public boolean setScAddress(String string, int slotId) {
        // TODO Auto-generated method stub
        // Gionee jialf 20111228 modified for CR00486311 start
 /*       try {
            // Gionee jialf 20111231 added for CR00486311 start
            // format sc address to <"+8613010888500",145 or 129>
            String scAddress = getScAddress(slotId);
            if (scAddress != null && !"".equals(scAddress)) {
                int length = scAddress.length();
                int index = scAddress.lastIndexOf("\"");
                string = scAddress.substring(0, 1) + string + scAddress.substring(index, length);
            }
            // Gionee jialf 20111231 added for CR00486311 end
            getITelephony().setScAddressGemini(string, slotId);
        } catch (Exception e) {
            Log.e("MMSLog", "set service center failed");
            return false;
        }
        return true;*/
        return false;
        // Gionee jialf 20111228 modified for CR00486311 end
    }
    
    public int getSimState (int slotId) {
        return TelephonyManager.getDefault().getSimState(); 
    }
    
    //gionee jiaoyuan 20130131 add by CR00770292 start
    private ITelephony getITelephonyMSim() {
        return ITelephony.Stub.asInterface(ServiceManager.getService(Context.TELEPHONY_SERVICE));
    }
    
    public int getNetworkType(){
    	try {
    		if (getITelephonyMSim() != null) {
            return getITelephonyMSim().getNetworkType();
        } else {
           return TelephonyManager.NETWORK_TYPE_UNKNOWN;
        }
        } catch (Exception e) {
            Log.e("GnTelephonyManagerEx", "get network type unknow!!");
            return TelephonyManager.NETWORK_TYPE_UNKNOWN;
        }
    }
    
    /**
    * Returns the unique device ID of a subscription, for example, the IMEI for
    * GSM and the MEID for CDMA phones. Return null if device ID is not available.
    *
    * <p>Requires Permission:
    *   {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
    *
    * @param subscription of which deviceID is returned
    */
    public String getDeviceId(int subscription){
    	
    	return TelephonyManager.getDefault().getDeviceId();
    	
    }
    //gionee jiaoyuan 20130131 add by CR00770292 end
    /**
    * Returns the unique subscriber ID, for example, the IMSI for a GSM phone
    * for a subscription.
    * Return null if it is unavailable.
    * <p>
    * Requires Permission:
    *   {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
    *
    * @param subscription whose subscriber id is returned
    */
     public String getSubscriberId(int subscription){
	 return TelephonyManager.getDefault().getSubscriberId();
     }

     /**
     * Returns the Service Provider Name (SPN) of a subscription.
     * <p>
     * Availability: SIM state must be {@link #SIM_STATE_READY}
     *
     * @see #getSimState
     *
     * @hide
     */
     public String getSimOperatorName(int subscription){
	 return TelephonyManager.getDefault().getSimOperatorName();
     }
	 
	 //Gionee guoyx 20130221 add for CR00773050 begin
    public boolean setPreferredDataSubscription (int subscription) {
        return false;//TelephonyManager.getDefault().setPreferredDataSubscription();
    }
    
    public int getPreferredDataSubscription () {
        return 0;//TelephonyManager.getDefault().getPreferredDataSubscription();
    }
    //Gionee guoyx 20130221 add for CR00773050 end

    //gionee jiaoyuan 20130305 add for CR00779101 begin
    public String getMultiSimName (Context context, int subscription){
        return null;//Settings.System.getString(context.getContentResolver(), Settings.System.MULTI_SIM_NAME[subscription]);
    }
    //gionee jiaoyuan 20130305 add for CR00779101 end
   //aurora add zhouxiaobing 20131019 start for setting
    public void listen(PhoneStateListener listener, int events, int simId)
    {
         
	}
	
   //aurora add zhouxiaobing 20131019 end for setting	
   //aurora add zhouxiaobing 20131019 start for sms
 /*   private static final int CMD_HANDLE_GET_SCA = 31;
    private static final int CMD_GET_SCA_DONE = 32;
    private static final int CMD_HANDLE_SET_SCA = 33;
    private static final int CMD_SET_SCA_DONE = 34;
	private String LOG_TAG="CallCard";
    private class ScAddrGemini {
		public String scAddr;
		public int simId;

		public ScAddrGemini(String addr, int id) {
			this.scAddr = addr;

		}
     }	
   private static final class MainThreadRequest {
	   /** The argument to use for the request 
	   public Object argument;
	   /** The result of the request that is run on the main thread 
	   public Object result;
   
	   public MainThreadRequest(Object argument) {
		   this.argument = argument;
	   }
   }

    private final class MainThreadHandler extends Handler {

        public MainThreadHandler(Looper looper)
        {
            super(looper);
		}
        @Override
        public void handleMessage(Message msg) {
            MainThreadRequest request;
            Message onCompleted;
            AsyncResult ar;
			Log.v(LOG_TAG, "handleMessage: 1 ");
           
            switch (msg.what) {
                case CMD_HANDLE_GET_SCA:
					request = (MainThreadRequest)msg.obj;
					onCompleted = obtainMessage(CMD_GET_SCA_DONE, request);
                    Log.v(LOG_TAG, "handleMessage: 2 ");
					if(request.argument == null) {
						// non-gemini
					} else {
					    ScAddrGemini sca = (ScAddrGemini)request.argument;
						int simId = sca.simId;
						mPhone.getSmscAddress(onCompleted);
						Log.v(LOG_TAG, "handleMessage: 3 ");
					}
					Log.v(LOG_TAG, "handleMessage: 4 ");	
					break;

				case CMD_GET_SCA_DONE:
					ar = (AsyncResult)msg.obj;
					request = (MainThreadRequest)ar.userObj;
                    
					if(ar.exception == null && ar.result != null) {
						Log.v(LOG_TAG, "handleMessage: 5 ");
						request.result = ar.result;
					} else {
					    Log.v(LOG_TAG, "handleMessage: 6 ");
						request.result = new String("");
					}

					synchronized(request) {
						Log.v(LOG_TAG, "handleMessage: 7 ");
						request.notifyAll();
					}
					break;

				case CMD_HANDLE_SET_SCA:
					request = (MainThreadRequest)msg.obj;
					onCompleted = obtainMessage(CMD_SET_SCA_DONE, request);

					ScAddrGemini sca = (ScAddrGemini)request.argument;
					Log.d(LOG_TAG, "[sca set sc single");
					mPhone.setSmscAddress(sca.scAddr, onCompleted);
					break;
				case CMD_SET_SCA_DONE:
					ar = (AsyncResult)msg.obj;
                	request = (MainThreadRequest)ar.userObj;
                	if(ar.exception != null) {
                		Log.d(LOG_TAG, "[sca Fail: set sc address");
                	} else {
                	    Log.d(LOG_TAG, "[sca Done: set sc address");
                	}
                	request.result = new Object();

					synchronized(request) {
                		request.notifyAll();
                	}
					break;
                //MTK-END [mtk04070][111117][ALPS00093395]MTK added
				

                default:
                    Log.w(LOG_TAG, "MainThreadHandler: unexpected message code: " + msg.what);
                    break;
            }
        }

    }
    private Object sendRequest(int command, Object argument) {
		Log.v(LOG_TAG, "sendRequest: 1 ");
        if (Looper.myLooper() == mMainThreadHandler.getLooper()) {
            throw new RuntimeException("This method will deadlock if called from the main thread.");
        }
        Log.v(LOG_TAG, "sendRequest: 2 ");
        MainThreadRequest request = new MainThreadRequest(argument);
        Message msg = mMainThreadHandler.obtainMessage(command, request);
        msg.sendToTarget();
		Log.v(LOG_TAG, "sendRequest: 3 ");

        // Wait for the request to complete
        synchronized (request) {
            while (request.result == null) {
                try {
                    request.wait();
                } catch (InterruptedException e) {
                    // Do nothing, go back and wait until the request is complete
                }
            }
        }
		Log.v(LOG_TAG, "sendRequest: 4 ");
        return request.result;
    }	
		// gionee gaoj 2011-12-1 added for 8255 start
		/**
		   * Get service center address
		   * @param simId SIM ID
		* @return Current service center address
		
		public String getScAddress(int slotId) {
	/*	   try {
			   return null;//getITelephony().getScAddressGemini(slotId);
		   } catch(RemoteException e1) {
			   return null;
		   } catch(NullPointerException e2) {
			   return null;
		   }
	//aurora add zhouxiaobing 20131021 start for sms center number
	  if(mMainThreadHandler==null)
	  	{
	  	 HandlerThread ht=new HandlerThread("GetSC");
		 ht.start();
	  	 mMainThreadHandler=new MainThreadHandler(ht.getLooper());
		 
	  	}
	  mPhone=PhoneFactory.getDefaultPhone();
	  final ScAddrGemini addr = new ScAddrGemini(null, slotId);
	Log.v(LOG_TAG, "getScAddress: 1 mPhone="+mPhone);
	  Thread sender = new Thread() {
		  public void run() {
			  try {
				  addr.scAddr = (String)sendRequest(CMD_HANDLE_GET_SCA, addr);
			  } catch(RuntimeException e) {
				  Log.e(LOG_TAG, "[sca getScAddressGemini " + e);
			  }
		  }
	  };
	  sender.start();
	  Log.v(LOG_TAG, "getScAddress: 2 ");
	  try {
		  Log.d(LOG_TAG, "[sca thread join");
		  sender.join();
	  } catch(InterruptedException e) {
		Log.d(LOG_TAG, "[sca throw interrupted exception");
	  }
	
	  Log.d(LOG_TAG, "getScAddressGemini: exit with " + addr.scAddr);
	Log.v(LOG_TAG, "getScAddress: 3 ");
	  return addr.scAddr;
	
	//aurora add zhouxiaobing 20131021 end 
	//	   return null;
		}
		public String getScAddress(int slotId,Context context) {
	/*	   try {
			   return null;//getITelephony().getScAddressGemini(slotId);
		   } catch(RemoteException e1) {
			   return null;
		   } catch(NullPointerException e2) {
			   return null;
		   }
	//aurora add zhouxiaobing 20131021 start for sms center number
	  if(mMainThreadHandler==null)
	  	{
	  	 HandlerThread ht=new HandlerThread("GetSC");
		 ht.start();
	  	 mMainThreadHandler=new MainThreadHandler(ht.getLooper());
		 
	  	}
	  PhoneFactory.makeDefaultPhone(context);
	  mPhone=PhoneFactory.getDefaultPhone();
	  final ScAddrGemini addr = new ScAddrGemini(null, slotId);
	Log.v(LOG_TAG, "getScAddress: 1 mPhone="+mPhone);
	  Thread sender = new Thread() {
		  public void run() {
			  try {
				  addr.scAddr = (String)sendRequest(CMD_HANDLE_GET_SCA, addr);
			  } catch(RuntimeException e) {
				  Log.e(LOG_TAG, "[sca getScAddressGemini " + e);
			  }
		  }
	  };
	  sender.start();
	  Log.v(LOG_TAG, "getScAddress: 2 ");
	  try {
		  Log.d(LOG_TAG, "[sca thread join");
		  sender.join();
	  } catch(InterruptedException e) {
		Log.d(LOG_TAG, "[sca throw interrupted exception");
	  }
	
	  Log.d(LOG_TAG, "getScAddressGemini: exit with " + addr.scAddr);
	Log.v(LOG_TAG, "getScAddress: 3 ");
	  return addr.scAddr;
	
	//aurora add zhouxiaobing 20131021 end 
	//	   return null;
		}		
	private MainThreadHandler mMainThreadHandler;
	private Phone mPhone;*/
   //aurora add zhouxiaobing 20131019 start for sms   
}
