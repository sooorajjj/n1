/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gionee.hardware;

import java.util.ArrayList;
import java.util.List;

import android.hardware.Camera;
import android.text.TextUtils;

/**
 * The Camera class is used to set image capture settings, start/stop preview,
 * snap pictures, and retrieve frames for encoding for video.  This class is a
 * client for the Camera service, which manages the actual camera hardware.
 *
 * <p>To access the device camera, you must declare the
 * {@link android.Manifest.permission#CAMERA} permission in your Android
 * Manifest. Also be sure to include the
 * <a href="{@docRoot}guide/topics/manifest/uses-feature-element.html">&lt;uses-feature></a>
 * manifest element to declare camera features used by your application.
 * For example, if you use the camera and auto-focus feature, your Manifest
 * should include the following:</p>
 * <pre> &lt;uses-permission android:name="android.permission.CAMERA" />
 * &lt;uses-feature android:name="android.hardware.camera" />
 * &lt;uses-feature android:name="android.hardware.camera.autofocus" /></pre>
 *
 * <p>To take pictures with this class, use the following steps:</p>
 *
 * <ol>
 * <li>Obtain an instance of Camera from {@link #open(int)}.
 *
 * <li>Get existing (default) settings with {@link #getParameters()}.
 *
 * <li>If necessary, modify the returned {@link Camera.Parameters} object and call
 * {@link #setParameters(Camera.Parameters)}.
 *
 * <li>If desired, call {@link #setDisplayOrientation(int)}.
 *
 * <li><b>Important</b>: Pass a fully initialized {@link SurfaceHolder} to
 * {@link #setPreviewDisplay(SurfaceHolder)}.  Without a surface, the camera
 * will be unable to start the preview.
 *
 * <li><b>Important</b>: Call {@link #startPreview()} to start updating the
 * preview surface.  Preview must be started before you can take a picture.
 *
 * <li>When you want, call {@link #takePicture(Camera.ShutterCallback,
 * Camera.PictureCallback, Camera.PictureCallback, Camera.PictureCallback)} to
 * capture a photo.  Wait for the callbacks to provide the actual image data.
 *
 * <li>After taking a picture, preview display will have stopped.  To take more
 * photos, call {@link #startPreview()} again first.
 *
 * <li>Call {@link #stopPreview()} to stop updating the preview surface.
 *
 * <li><b>Important:</b> Call {@link #release()} to release the camera for
 * use by other applications.  Applications should release the camera
 * immediately in {@link android.app.Activity#onPause()} (and re-{@link #open()}
 * it in {@link android.app.Activity#onResume()}).
 * </ol>
 *
 * <p>To quickly switch to video recording mode, use these steps:</p>
 *
 * <ol>
 * <li>Obtain and initialize a Camera and start preview as described above.
 *
 * <li>Call {@link #unlock()} to allow the media process to access the camera.
 *
 * <li>Pass the camera to {@link android.media.MediaRecorder#setCamera(Camera)}.
 * See {@link android.media.MediaRecorder} information about video recording.
 *
 * <li>When finished recording, call {@link #reconnect()} to re-acquire
 * and re-lock the camera.
 *
 * <li>If desired, restart preview and take more photos or videos.
 *
 * <li>Call {@link #stopPreview()} and {@link #release()} as described above.
 * </ol>
 *
 * <p>This class is not thread-safe, and is meant for use from one event thread.
 * Most long-running operations (preview, focus, photo capture, etc) happen
 * asynchronously and invoke callbacks as necessary.  Callbacks will be invoked
 * on the event thread {@link #open(int)} was called from.  This class's methods
 * must never be called from multiple threads at once.</p>
 *
 * <p class="caution"><strong>Caution:</strong> Different Android-powered devices
 * may have different hardware specifications, such as megapixel ratings and
 * auto-focus capabilities. In order for your application to be compatible with
 * more devices, you should not make assumptions about the device camera
 * specifications.</p>
 *
 * <div class="special reference">
 * <h3>Developer Guides</h3>
 * <p>For more information about using cameras, read the
 * <a href="{@docRoot}guide/topics/media/camera.html">Camera</a> developer guide.</p>
 * </div>
 */
public class GnCamera {
    private static final String TAG = "GNCamera";

    private static final String KEY_CLICK_SOUND = "camera-click-sound";

	private static final String KEY_LIVE_EFFECT = "live-effect";

	private static final String KEY_PICTURE_FRAME = "picture-frame";
	private static final String KEY_PICTURE_FRAME_SUPPORTED = "picture-frame-supported";
	private static final String kEY_PICTURE_FRAME_NORMAL = "normal";

	private static final String KEY_GESTURE_SUPPORTED = "gesture-supported";
	private static final String KEY_GESTURE_MODE = "gesture";
	public  static final String KEY_GESTURE_ON = "on";
	public  static final String KEY_GESTURE_OFF = "off";

	public static final String KEY_FACE_BEAUTY_LEVEL = "face-beauty-level";

	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_NONE = "none";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_ANTIQUE = "antique";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_CARTOON = "cartoon";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_EMBOSS = "emboss";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_FLIP = "flip";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_FOG = "fog";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_GRAYSCALE = "grayscale";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_LOMO_BLUE = "lomo-blue";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_LOMO_GREEN = "lomo-green";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_LOMO_NEUTRAL = "lomo-neutral";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_LOMO_RED = "lomo-red";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_LOMO_YELLOW = "lomo-yellow";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_MAGICPEN = "magicpen";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_MILKY = "milky";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_MIRROR = "mirror";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_NEGATIVE = "negative";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_OILY = "oily";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_PAINT = "paint";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_SEPIA = "sepia";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_SKETCH_COLOR = "sketch-color";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_SKETCH_GRAY = "sketch-gray";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_SOLARIZE = "solarize";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_STAMP = "stamp";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_DOF = "dof";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_VIGNETTING = "vignetting";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_FISHEYE = "warp-fisheye";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_ALIENSCUM = "warp-alienscum";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_BIGBUPPA = "warp-bigbuppa";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_BOOZIEBOWL = "warp-booziebowl";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_CONEHEAD = "warp-conehead";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_FUNKYFRESH = "warp-funkyfresh";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_JELLYBELLY = "warp-jellybelly";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_PETTYPETIT = "warp-pettypetit";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_EYEBALLEDEVY = "warp-eyeballedevy";
	/**
	 * @hide
	*/
	public static final String LIVE_EFFECT_WARP_SPEEDYWEED = "warp-speedyweed";

	 /**
     * @hide
     */
     public static final String CAPTURE_MODE_LOWLIGHT = "lowlight";

     
     // Parameter key suffix for supported values.
     private static final String SUPPORTED_VALUES_SUFFIX = "-values";
     private static final String KEY_ZSD_SUPPORTED = "zsd-supported";
     private static final String TRUE = "true";

/**
* @hide
*/
    public void setCameraClickSound(Camera mCamera ,String sound) {
    	mCamera.getParameters().set(KEY_CLICK_SOUND, sound);
    }

/**
* @hide
*/
    public String getCameraClickSound(Camera mCamera) {
        return mCamera.getParameters().get(KEY_CLICK_SOUND);
    }

	/**
	 * @hide
	*/
	public void setLiveEffect(Camera mCamera ,String value) {
		mCamera.getParameters().set(KEY_LIVE_EFFECT, value);
    }

	/**
	 * @hide
	*/
	public String getLiveEffect(Camera mCamera) {
        return mCamera.getParameters().get(KEY_LIVE_EFFECT);
    }

    private ArrayList<String> split(String str) {
        if (str == null) return null;

        TextUtils.StringSplitter splitter = new TextUtils.SimpleStringSplitter(',');
        splitter.setString(str);
        ArrayList<String> substrings = new ArrayList<String>();
        for (String s : splitter) {
            substrings.add(s);
        }
        return substrings;
    }
    
	/**
	 * @hide
	*/
	public List<String> getSupportedLiveEffects(Camera mCamera) {
        String str = mCamera.getParameters().get(KEY_LIVE_EFFECT+ SUPPORTED_VALUES_SUFFIX);
        return split(str);
    }

	/**
	  * @hide
	  */
    public boolean isPictureFrameSupported(Camera mCamera) {
    	String str = mCamera.getParameters().get(KEY_PICTURE_FRAME_SUPPORTED);
        return TRUE.equals(str);
    }

	
	/**
	  * @hide
	  */
	public void setPictureFrame(Camera mCamera ,String value) {
		mCamera.getParameters().set(KEY_PICTURE_FRAME, value);
	}

	/**
	  * @hide
	  */
	public String getPictureFrame(Camera mCamera) {
        return mCamera.getParameters().get(KEY_PICTURE_FRAME);
    }

	/**
      * @hide
      */
	public void setGestureMode(Camera mCamera ,String value) {
		mCamera.getParameters().set(KEY_GESTURE_MODE, value);
	}

	/**
      * @hide
      */
	public String getGestureMode(Camera mCamera) {
		return mCamera.getParameters().get(KEY_GESTURE_MODE);
	}
	
	/**
      * @hide
      */
    public boolean isGestureSupported(Camera mCamera) {
    	String str = mCamera.getParameters().get(KEY_GESTURE_SUPPORTED);
        return TRUE.equals(str);
    }

	/**
	* @hide
	*/
	public void setFaceBeautyLevel(Camera mCamera ,int value) {
		mCamera.getParameters().set(KEY_FACE_BEAUTY_LEVEL, value);
	}

	/**
     * @hide
     */
    public boolean isZSDSupported(Camera mCamera) {
    	String str = mCamera.getParameters().get(KEY_ZSD_SUPPORTED);
		if (TRUE.equals(str)) {
			return true;
		} else {
			List<String> zsdMode = mCamera.getParameters().getSupportedZSDMode();
			for (String mode : zsdMode) {
				if ("on".equals(mode)) {
					return true;
				}
			}
		}

		return false;
    }
}
