package gionee.net.http;

import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;

import android.net.http.AndroidHttpClient;

public class GnAndroidHttpClient {
    /**
     * setHttpRequestRetryHandler for Enable HTTP Retry 
     * @param client
     */
    public static void setHttpRequestRetryHandler(AndroidHttpClient client) {
        client.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(1, true));
    }
}
