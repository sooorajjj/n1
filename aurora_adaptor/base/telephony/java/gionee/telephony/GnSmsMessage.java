package gionee.telephony;
import static android.telephony.TelephonyManager.PHONE_TYPE_CDMA;

import com.android.internal.telephony.SmsMessageBase;

import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;

public class GnSmsMessage{

//    private android.telephony.SmsMessage mMsg;
    
    private SmsMessageBase mMsg;
    
     //Gionee jialf 20120105 removed for CR00494057 start
//     public GnSmsMessage() {
//         mMsg = msg;
//     }
     //Gionee jialf 20120105 removed for CR00494057 end
    
    public GnSmsMessage() {
        getSmsFacility();
    }
     
    public void SmsMessage(SmsMessageBase smb) {
        mMsg = smb;
    }

    private static SmsMessageBase getSmsFacility() {
        int activePhone = TelephonyManager.getDefault().getCurrentPhoneType();
        if (PHONE_TYPE_CDMA == activePhone) {
            return new com.android.internal.telephony.cdma.SmsMessage();
        } else {
            return new com.android.internal.telephony.gsm.SmsMessage();
        }
    }
//MTK not need this method, remove for compile pass
//    public void setTelephonyMsg(android.telephony.SmsMessage msg) {
//        mMsg = msg;
//    }

    public boolean isReplace() {
        return mMsg.isReplace();
    }

    public String getMessageBody() {
        return mMsg.getMessageBody();
    }

    public String getDisplayMessageBody() {
        return mMsg.getDisplayMessageBody();
    }

    public String getOriginatingAddress() {
        return mMsg.getOriginatingAddress();
    }

    public int getProtocolIdentifier() {
        return mMsg.getProtocolIdentifier();
    }

    public String getDisplayOriginatingAddress() {
        return mMsg.getDisplayOriginatingAddress();
    }

    public int getMessageSimId() {
        return -1;
    }
    
    public int getMessageSimId(SmsMessage msg) {
//for MTK
        return msg.getMessageSimId();
//for QC        
//        return getMessageSimId();
    }

    public String getPseudoSubject() {
        return mMsg.getPseudoSubject();
    }

    public boolean isReplyPathPresent() {
        return mMsg.isReplyPathPresent();
    }

    public String getServiceCenterAddress() {
        return mMsg.getServiceCenterAddress();
    }

    public static int[] calculateLength(CharSequence text, boolean b,
            int encodingType) {
        return SmsMessage.calculateLength(text, b, encodingType);
    }

    public static String getDisplayOriginatingAddress(SmsMessage message) {
        return message.getDestinationAddress();
    }
    
	//Gionee <guoyx> <2013-04-28> added for CR00797539 begin
    public static String getDestinationAddress(SmsMessage message) {
        return message.getDestinationAddress();
    }
	//Gionee <guoyx> <2013-04-28> added for CR00797539 end
}