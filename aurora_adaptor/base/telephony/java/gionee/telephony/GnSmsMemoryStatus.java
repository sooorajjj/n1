package gionee.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.telephony.SmsMemoryStatus;

public class GnSmsMemoryStatus implements Parcelable {
    public int mUsed;
    public int mTotal;
    public static final Parcelable.Creator<GnSmsMemoryStatus> CREATOR = new Parcelable.Creator() {
        public GnSmsMemoryStatus createFromParcel(Parcel source) {
            int used = source.readInt();
            int total = source.readInt();
            return new GnSmsMemoryStatus(used, total);
        }

        public GnSmsMemoryStatus[] newArray(int size) {
            return new GnSmsMemoryStatus[size];
        }
    };

    public GnSmsMemoryStatus() {
        this.mUsed = 0;
        this.mTotal = 0;
    }

    public GnSmsMemoryStatus(int used, int total) {
        this.mUsed = used;
        this.mTotal = total;
    }

    public int getUsed() {
        return this.mUsed;
    }
    
    public static int getUsed(int currentSlotId) {
      SmsMemoryStatus SimMemStatus = null;
      SimMemStatus = GnSmsManager.getDefault().getSmsSimMemoryStatus(currentSlotId);
      return getUsed(SimMemStatus);
   }
    
    public static int getUsed(SmsMemoryStatus simMemStatus) {
        return simMemStatus.getUsed();
    }

    public static int getTotal(int currentSlotId) {
        SmsMemoryStatus SimMemStatus = null;
        SimMemStatus = GnSmsManager.getDefault().getSmsSimMemoryStatus(currentSlotId);
        return getTotal(SimMemStatus);
    }
    
    public static int getTotal(SmsMemoryStatus simMemStatus) {
        return simMemStatus.getTotal();
    }
    
    public int getTotal() {
        return this.mTotal;
    }

    public int getUnused() {
        return this.mTotal - this.mUsed;
    }

    public void reset() {
        this.mUsed = 0;
        this.mTotal = 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mUsed);
        dest.writeInt(this.mTotal);
    }
}
