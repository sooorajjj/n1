package gionee.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import com.android.internal.telephony.Phone;
import android.telephony.TelephonyManager;

public class GnConnectivityManager {
    //Gionee guoyx 20130328 added for CR00787716 begin
    private static int backupDataEnabled = -1;
	//Gionee guoyx 20130328 added for CR00787716 end
    public static int startUsingNetworkFeatureGemini(ConnectivityManager ConnMgr, int slotId) {
//        ConnMgr.startUsingNetworkFeatureGemini(
//                ConnectivityManager.TYPE_MOBILE, GnPhone.FEATURE_ENABLE_MMS, slotId);
        //Gionee guoyx 20130328 added for CR00787716 begin
        boolean dataEnabled = getMobileDataEnabledGemini(ConnMgr, 0L);
        if (!dataEnabled) {
            Log.d("Mms/Txn", "startUsingNetworkFeatureGemini: data is closed! will open it.");
            backupDataEnabled = 0;
            setMobileDataEnabledGemini(ConnMgr, true);
        }
		//Gionee guoyx 20130328 added for CR00787716 end
        return ConnMgr.startUsingNetworkFeature(
                ConnectivityManager.TYPE_MOBILE, Phone.FEATURE_ENABLE_MMS);
    }
    
    public static int stopUsingNetworkFeatureGemini(ConnectivityManager ConnMgr, int slotId) {
//        ConnMgr.stopUsingNetworkFeatureGemini(
//                ConnectivityManager.TYPE_MOBILE, GnPhone.FEATURE_ENABLE_MMS, slotId);
        //Gionee guoyx 20130328 modified for CR00787716 begin
        int result = -1;
        result = ConnMgr.stopUsingNetworkFeature(
                ConnectivityManager.TYPE_MOBILE, Phone.FEATURE_ENABLE_MMS);
        
        if (backupDataEnabled == 0) {
            Log.d("Mms/Txn", "stopUsingNetworkFeatureGemini: backup data is closed! will close it.");
            backupDataEnabled = -1;
            setMobileDataEnabledGemini(ConnMgr, false);
        }
      return result;
	  //Gionee guoyx 20130328 modified for CR00787716 end
  }

    public static boolean getMobileDataEnabledGemini(
            ConnectivityManager ConnMgr, long simId) {
        //return ConnMgr.getMobileDataEnabledGemini(simId);
		//Gionee guoyx 20130328 added for CR00787716 begin
        return ConnMgr.getMobileDataEnabled();
		//Gionee guoyx 20130328 added for CR00787716 end
    }
    
	//Gionee guoyx 20130328 added for CR00787716 begin
    public static void setMobileDataEnabledGemini(
            ConnectivityManager ConnMgr, boolean enabled) {
        //return ConnMgr.getMobileDataEnabledGemini(simId);
//        ConnMgr.setMobileDataEnabled(enabled);
	telManager.setDataEnabled(enabled);
    }
	//Gionee guoyx 20130328 added for CR00787716 end

    private static TelephonyManager telManager = TelephonyManager.getDefault();
}
