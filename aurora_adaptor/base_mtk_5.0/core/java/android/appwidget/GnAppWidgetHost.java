package android.appwidget;
import java.lang.reflect.Method;
import android.app.ActivityManager;
import android.os.Build;
public class GnAppWidgetHost{

public static int allocateAppWidgetIdForSystem(int hostId)
{
      if(Build.VERSION.SDK_INT<18)
    {
           try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.appwidget.AppWidgetHost");
                  Method method=sPolicy.getMethod("allocateAppWidgetIdForSystem", int.class);
                  return ((Integer)method.invoke(null,hostId)).intValue();
          }catch(Exception e){}
    }
  else
  {
    try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.appwidget.AppWidgetHost");
                  Method method=sPolicy.getMethod("allocateAppWidgetIdForSystem", int.class,int.class);
                  return ((Integer)method.invoke(null,hostId,ActivityManager.getCurrentUser())).intValue();
      }catch(Exception e){}
  } 
  return 0;
}

public static int deleteAppWidgetIdForSystem(int hostId)
{
      if(Build.VERSION.SDK_INT<18)
    {
           try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.appwidget.AppWidgetHost");
                  Method method=sPolicy.getMethod("deleteAppWidgetIdForSystem", int.class);
                  return ((Integer)method.invoke(null,hostId)).intValue();
          }catch(Exception e){}
    }
  else
  {
    try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.appwidget.AppWidgetHost");
                  Method method=sPolicy.getMethod("deleteAppWidgetIdForSystem", int.class,int.class);
                  return ((Integer)method.invoke(null,hostId,ActivityManager.getCurrentUser())).intValue();
      }catch(Exception e){}
  } 
  return 0;
}

}
