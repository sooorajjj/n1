package gionee.net;

import com.gionee.internal.telephony.GnPhone;

import android.net.ConnectivityManager;

public class GnConnectivityManager {

    public static int startUsingNetworkFeatureGemini(
            ConnectivityManager ConnMgr, int slotId) {
        return ConnMgr.startUsingNetworkFeatureGemini(
                ConnectivityManager.TYPE_MOBILE, GnPhone.FEATURE_ENABLE_MMS,
                slotId);
    }

    public static int stopUsingNetworkFeatureGemini(
            ConnectivityManager ConnMgr, int slotId) {
        return ConnMgr.stopUsingNetworkFeatureGemini(
                ConnectivityManager.TYPE_MOBILE, GnPhone.FEATURE_ENABLE_MMS,
                slotId);
    }

    public static boolean getMobileDataEnabledGemini(
            ConnectivityManager ConnMgr, long simId) {
        int mSimId = (int)simId;
        return ConnMgr.getMobileDataEnabledGemini(mSimId);
    }
    
}
