package android.content;
import java.lang.reflect.Method;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.UserHandle;
import android.os.Build;
public class GnContext{

public  static boolean bindService(Intent service, ServiceConnection conn, int flags, int userHandle,Context context)
{
    if(Build.VERSION.SDK_INT<18)
    {
           try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.Context");
                  Method method=sPolicy.getMethod("bindService", Intent.class,ServiceConnection.class,int.class,int.class);
                  return ((Boolean)method.invoke(context,service,conn,flags,userHandle)).booleanValue();
          }catch(Exception e){}
    }
  else
  {
           try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.Context");
                  Method method=sPolicy.getMethod("bindService", Intent.class,ServiceConnection.class,int.class,UserHandle.class);
                  return ((Boolean)method.invoke(context,service,conn,flags,new UserHandle(userHandle))).booleanValue();
          }catch(Exception e){}
  } 
  return false;
}

}
