#ifndef SHF_EXTRACTOR_H_
#define SHF_EXTRACTOR_H_

#include "shf_define.h"

void shf_extractor_run();

/******************************************************************************
 * Unit Test Function
******************************************************************************/
#ifdef SHF_UNIT_TEST_ENABLE
void shf_extractor_unit_test();
#endif

#endif /* SHF_EXTRACTOR_H_ */
